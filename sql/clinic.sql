drop database if exists clinic;
create database clinic;
use clinic;

create table user(
id INT(16) not null auto_increment,
login VARCHAR(60) not null,
password VARCHAR(60) not null,
firstName VARCHAR(60) not null,
lastName VARCHAR(60) not null,
patronymic VARCHAR(60) default null,
userType ENUM('ADMIN','DOCTOR','PATIENT') not null,
primary key(id),
unique(login)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

create table admin(
id INT(16) not null auto_increment,
userId INT(16) not null,
position VARCHAR(150),
primary key(id),
foreign key(userId) references user(id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

create table patient(
id INT(16) not null auto_increment,
userId INT(16) not null,
email VARCHAR(150) not null,
address VARCHAR(300) not null,
phone VARCHAR(20) not null,
primary key(id),
unique(email),
foreign key(userId) references user(id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

create table speciality(
id INT(16) not null auto_increment,
name VARCHAR (180) not null,
primary key(id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

create table room(
id INT(16) not null auto_increment,
name VARCHAR (180) not null,
primary key(id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

create table doctor(
id INT(16) not null auto_increment,
userId INT(16) not null,
specialityId INT(9) not null,
roomId INT(9) not null,
primary key(id),
foreign key(userId) references user(id),
foreign key(specialityId) references speciality(id),
foreign key(roomId) references room(id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

create table ticket(
id INT(16) not null auto_increment,
patientId INT(16) not null,
ticketNumber VARCHAR(300) not null, 
ticketType ENUM('RECEPTION','COMMISSION') not null,
primary key(id),
foreign key(patientId) references patient(id) on delete cascade 
)ENGINE=INNODB DEFAULT CHARSET=utf8;

create table day_schedule(
id INT(16) not null auto_increment,
doctorId INT(16) not null,
date DATE not null,
primary key(id),
foreign key(doctorId) references doctor(id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

create table reception(
id INT(16) not null auto_increment,
ticketId INT(16) default null,
day_scheduleId INT(16) not null,
time TIME not null,
duration INT default null,
status ENUM('TICKET','FREE','BUSY'),
primary key(id),
foreign key(ticketId) references ticket(id),
foreign key(day_scheduleId) references day_schedule(id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

create table commission(
id INT(16) not null auto_increment,
ticketId INT(16) default null,
date DATETIME not null,
duration INT default null,
roomName VARCHAR(180) not null,
primary key(id),
foreign key(ticketId) references ticket(id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

create table commission_doctors(
id INT(16) not null auto_increment,
commissionId INT(16) not null,
doctorId INT(16) not null,
primary key(id),
foreign key(commissionId) references commission(id),
foreign key(doctorId) references doctor(id)  
)ENGINE=INNODB DEFAULT CHARSET=utf8;

create table session(
id INT not null auto_increment,
cookie VARCHAR(50),
userId INT not null,
primary key(id),
unique(userId),
key(userId),
foreign key(userId) references user(id) on delete cascade
) ENGINE=INNODB DEFAULT CHARSET=utf8;

insert into user value(1,'admin','admin!!!','Ivan','Ivanov','Ivanovich','ADMIN');
insert into admin value(1,1,"admin");
insert into speciality values(1, 'therapist'),(2,'neurologist');
insert into room values(1,'112A'),(2,'113B');	

select * from doctor;
select * from day_schedule;
select * from reception;
select * from user;
select * from room;
select * from speciality;
select * from session;
select * from commission;
select * from commission_doctors;
select * from ticket;
select * from patient;


