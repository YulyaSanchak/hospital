package net.thumbtack.school.endpoints;

import net.thumbtack.school.dto.requests.*;
import net.thumbtack.school.dto.requests.register_update_admin.AdminRegisterRequest;
import net.thumbtack.school.dto.requests.register_update_doctor.DoctorRegisterRequest;
import net.thumbtack.school.dto.requests.register_update_doctor.WeekSchedule;
import net.thumbtack.school.dto.requests.register_update_patient.PatientRegisterRequest;
import net.thumbtack.school.dto.responses.*;
import net.thumbtack.school.dto.responses.tickets.CommissionResponse;
import net.thumbtack.school.exceptions.BaseException;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class CommissionEndpointsTest extends BaseTest {
    @Test
    //10:10 dur 30
    public void testCreateCommission() {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        registerAdmin(defaultAdminCookie,dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        ImmutablePair<String,PatientRegisterUpdateResponse> patientResponse = registerPatient(dtoPatient);
        String cookiePatient = patientResponse.getLeft();
        PatientRegisterUpdateResponse patientDtoResponse = patientResponse.getRight();

        LocalDate startDate = LocalDate.of(2020,2,2);
        LocalDate endDate = LocalDate.of(2020,2,4);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor1 = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor1 = registerDoctor(adminCookie,dtoCreateDoctor1);
        DoctorRegisterRequest dtoCreateDoctor2 = new DoctorRegisterRequest("Иван","Иванов","Петрович","neurologist","113B"
                ,"neurologist_ivanov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor2 = registerDoctor(adminCookie,dtoCreateDoctor2);

        String doctorCookie = login(new LoginDtoRequest(dtoCreateDoctor1.getLogin(),dtoCreateDoctor1.getPassword())).getLeft();

        List<Integer> doctorIds = new ArrayList<>();
        Collections.addAll(doctorIds,dtoResponseCreateDoctor1.getId(),dtoResponseCreateDoctor2.getId());
        CommissionRequest commissionDtoRequest =new CommissionRequest(patientDtoResponse.getId(),doctorIds,
                dtoCreateDoctor1.getRoom(),LocalDate.of(2020,2,3).toString(),
                LocalTime.of(10,10).toString(),30);
        CommissionResponse commissionDtoResponse =  createCommission(doctorCookie,commissionDtoRequest);

        assertEquals(commissionDtoRequest.getPatientId(),commissionDtoResponse.getPatientId());
        assertTrue(commissionDtoRequest.getDoctorIds().containsAll(commissionDtoResponse.getDoctorIds()));
        assertEquals(commissionDtoRequest.getRoom(),commissionDtoResponse.getRoom());
        assertEquals(commissionDtoRequest.getDate(),commissionDtoResponse.getDate());
        assertEquals(commissionDtoRequest.getTime(),commissionDtoResponse.getTime());
        assertEquals(commissionDtoRequest.getDuration(),commissionDtoResponse.getDuration());

    }

    @Test
    //10:00 dur 30
    public void testCreateCommission2() {
        String defaultAdminCookie = loginDefaultAdmin();

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        ImmutablePair<String,PatientRegisterUpdateResponse> patientResponse = registerPatient(dtoPatient);
        String cookiePatient = patientResponse.getLeft();
        PatientRegisterUpdateResponse patientDtoResponse = patientResponse.getRight();

        LocalDate startDate = LocalDate.of(2020,2,2);
        LocalDate endDate = LocalDate.of(2020,2,4);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor1 = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor1 = registerDoctor(defaultAdminCookie,dtoCreateDoctor1);
        DoctorRegisterRequest dtoCreateDoctor2 = new DoctorRegisterRequest("Иван","Иванов","Петрович","neurologist","113B"
                ,"neurologist_ivanov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor2 = registerDoctor(defaultAdminCookie,dtoCreateDoctor2);

        String doctorCookie = login(new LoginDtoRequest(dtoCreateDoctor1.getLogin(),dtoCreateDoctor1.getPassword())).getLeft();

        List<Integer> doctorIds = new ArrayList<>();
        Collections.addAll(doctorIds,dtoResponseCreateDoctor1.getId(),dtoResponseCreateDoctor2.getId());
        CommissionRequest commissionDtoRequest =new CommissionRequest(patientDtoResponse.getId(),doctorIds,
                dtoCreateDoctor1.getRoom(),LocalDate.of(2020,2,3).toString(),
                LocalTime.of(10,0).toString(),30);
        CommissionResponse commissionDtoResponse =  createCommission(doctorCookie,commissionDtoRequest);

        assertNotNull(commissionDtoResponse.getTicket());
        assertEquals(commissionDtoRequest.getPatientId(),commissionDtoResponse.getPatientId());
        assertTrue(commissionDtoRequest.getDoctorIds().containsAll(commissionDtoResponse.getDoctorIds()));
        assertEquals(commissionDtoRequest.getRoom(),commissionDtoResponse.getRoom());
        assertEquals(commissionDtoRequest.getDate(),commissionDtoResponse.getDate());
        assertEquals(commissionDtoRequest.getTime(),commissionDtoResponse.getTime());
        assertEquals(commissionDtoRequest.getDuration(),commissionDtoResponse.getDuration());

    }

    @Test
    //10:10 dur 15
    public void testCreateCommission3() {
        String defaultAdminCookie = loginDefaultAdmin();

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        ImmutablePair<String,PatientRegisterUpdateResponse> patientResponse = registerPatient(dtoPatient);
        String cookiePatient = patientResponse.getLeft();
        PatientRegisterUpdateResponse patientDtoResponse = patientResponse.getRight();

        LocalDate startDate = LocalDate.of(2020,2,2);
        LocalDate endDate = LocalDate.of(2020,2,4);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor1 = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor1 = registerDoctor(defaultAdminCookie,dtoCreateDoctor1);
        DoctorRegisterRequest dtoCreateDoctor2 = new DoctorRegisterRequest("Иван","Иванов","Петрович","neurologist","113B"
                ,"neurologist_ivanov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor2 = registerDoctor(defaultAdminCookie,dtoCreateDoctor2);

        String doctorCookie = login(new LoginDtoRequest(dtoCreateDoctor1.getLogin(),dtoCreateDoctor1.getPassword())).getLeft();

        List<Integer> doctorIds = new ArrayList<>();
        Collections.addAll(doctorIds,dtoResponseCreateDoctor1.getId(),dtoResponseCreateDoctor2.getId());
        CommissionRequest commissionDtoRequest =new CommissionRequest(patientDtoResponse.getId(),doctorIds,
                dtoCreateDoctor1.getRoom(),LocalDate.of(2020,2,3).toString(),
                LocalTime.of(10,10).toString(),15);
        CommissionResponse commissionDtoResponse =  createCommission(doctorCookie,commissionDtoRequest);

        assertNotNull(commissionDtoResponse.getTicket());
        assertEquals(commissionDtoRequest.getPatientId(),commissionDtoResponse.getPatientId());
        assertTrue(commissionDtoRequest.getDoctorIds().containsAll(commissionDtoResponse.getDoctorIds()));
        assertEquals(commissionDtoRequest.getRoom(),commissionDtoResponse.getRoom());
        assertEquals(commissionDtoRequest.getDate(),commissionDtoResponse.getDate());
        assertEquals(commissionDtoRequest.getTime(),commissionDtoResponse.getTime());
        assertEquals(commissionDtoRequest.getDuration(),commissionDtoResponse.getDuration());
    }

    @Test
    //10:00 dur 45
    public void testCreateCommission4() {
        String defaultAdminCookie = loginDefaultAdmin();

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        ImmutablePair<String,PatientRegisterUpdateResponse> patientResponse = registerPatient(dtoPatient);
        String cookiePatient = patientResponse.getLeft();
        PatientRegisterUpdateResponse patientDtoResponse = patientResponse.getRight();

        LocalDate startDate = LocalDate.of(2020,2,2);
        LocalDate endDate = LocalDate.of(2020,2,4);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor1 = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor1 = registerDoctor(defaultAdminCookie,dtoCreateDoctor1);
        DoctorRegisterRequest dtoCreateDoctor2 = new DoctorRegisterRequest("Иван","Иванов","Петрович","neurologist","113B"
                ,"neurologist_ivanov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor2 = registerDoctor(defaultAdminCookie,dtoCreateDoctor2);

        String doctorCookie = login(new LoginDtoRequest(dtoCreateDoctor1.getLogin(),dtoCreateDoctor1.getPassword())).getLeft();

        List<Integer> doctorIds = new ArrayList<>();
        Collections.addAll(doctorIds,dtoResponseCreateDoctor1.getId(),dtoResponseCreateDoctor2.getId());
        CommissionRequest commissionDtoRequest =new CommissionRequest(patientDtoResponse.getId(),doctorIds,
                dtoCreateDoctor1.getRoom(),LocalDate.of(2020,2,3).toString(),
                LocalTime.of(10,0).toString(),45);
        CommissionResponse commissionDtoResponse =  createCommission(doctorCookie,commissionDtoRequest);

        assertNotNull(commissionDtoResponse.getTicket());
        assertEquals(commissionDtoRequest.getPatientId(),commissionDtoResponse.getPatientId());
        assertTrue(commissionDtoRequest.getDoctorIds().containsAll(commissionDtoResponse.getDoctorIds()));
        assertEquals(commissionDtoRequest.getRoom(),commissionDtoResponse.getRoom());
        assertEquals(commissionDtoRequest.getDate(),commissionDtoResponse.getDate());
        assertEquals(commissionDtoRequest.getTime(),commissionDtoResponse.getTime());
        assertEquals(commissionDtoRequest.getDuration(),commissionDtoResponse.getDuration());
    }

    @Test
    //10:10 dur 70
    public void testCreateCommission5() {
        String defaultAdminCookie = loginDefaultAdmin();

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        ImmutablePair<String,PatientRegisterUpdateResponse> patientResponse = registerPatient(dtoPatient);
        String cookiePatient = patientResponse.getLeft();
        PatientRegisterUpdateResponse patientDtoResponse = patientResponse.getRight();

        LocalDate startDate = LocalDate.of(2020,2,2);
        LocalDate endDate = LocalDate.of(2020,2,4);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor1 = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor1 = registerDoctor(defaultAdminCookie,dtoCreateDoctor1);
        DoctorRegisterRequest dtoCreateDoctor2 = new DoctorRegisterRequest("Иван","Иванов","Петрович","neurologist","113B"
                ,"neurologist_ivanov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor2 = registerDoctor(defaultAdminCookie,dtoCreateDoctor2);

        String doctorCookie = login(new LoginDtoRequest(dtoCreateDoctor1.getLogin(),dtoCreateDoctor1.getPassword())).getLeft();

        List<Integer> doctorIds = new ArrayList<>();
        Collections.addAll(doctorIds,dtoResponseCreateDoctor1.getId(),dtoResponseCreateDoctor2.getId());
        CommissionRequest commissionDtoRequest =new CommissionRequest(patientDtoResponse.getId(),doctorIds,
                dtoCreateDoctor1.getRoom(),LocalDate.of(2020,2,3).toString(),
                LocalTime.of(10,10).toString(),70);
        CommissionResponse commissionDtoResponse =  createCommission(doctorCookie,commissionDtoRequest);

        assertNotNull(commissionDtoResponse.getTicket());
        assertEquals(commissionDtoRequest.getPatientId(),commissionDtoResponse.getPatientId());
        assertTrue(commissionDtoRequest.getDoctorIds().containsAll(commissionDtoResponse.getDoctorIds()));
        assertEquals(commissionDtoRequest.getRoom(),commissionDtoResponse.getRoom());
        assertEquals(commissionDtoRequest.getDate(),commissionDtoResponse.getDate());
        assertEquals(commissionDtoRequest.getTime(),commissionDtoResponse.getTime());
        assertEquals(commissionDtoRequest.getDuration(),commissionDtoResponse.getDuration());
    }

    @Test
    //10:00 dur 90
    public void testCreateCommission6() {
        String defaultAdminCookie = loginDefaultAdmin();

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        ImmutablePair<String,PatientRegisterUpdateResponse> patientResponse = registerPatient(dtoPatient);
        String cookiePatient = patientResponse.getLeft();
        PatientRegisterUpdateResponse patientDtoResponse = patientResponse.getRight();

        LocalDate startDate = LocalDate.of(2020,2,2);
        LocalDate endDate = LocalDate.of(2020,2,4);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor1 = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor1 = registerDoctor(defaultAdminCookie,dtoCreateDoctor1);
        DoctorRegisterRequest dtoCreateDoctor2 = new DoctorRegisterRequest("Иван","Иванов","Петрович","neurologist","113B"
                ,"neurologist_ivanov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor2 = registerDoctor(defaultAdminCookie,dtoCreateDoctor2);

        String doctorCookie = login(new LoginDtoRequest(dtoCreateDoctor1.getLogin(),dtoCreateDoctor1.getPassword())).getLeft();

        List<Integer> doctorIds = new ArrayList<>();
        Collections.addAll(doctorIds,dtoResponseCreateDoctor1.getId(),dtoResponseCreateDoctor2.getId());
        CommissionRequest commissionDtoRequest =new CommissionRequest(patientDtoResponse.getId(),doctorIds,
                dtoCreateDoctor1.getRoom(),LocalDate.of(2020,2,3).toString(),
                LocalTime.of(10,0).toString(),90);
        CommissionResponse commissionDtoResponse =  createCommission(doctorCookie,commissionDtoRequest);

        assertNotNull(commissionDtoResponse.getTicket());
        assertEquals(commissionDtoRequest.getPatientId(),commissionDtoResponse.getPatientId());
        assertTrue(commissionDtoRequest.getDoctorIds().containsAll(commissionDtoResponse.getDoctorIds()));
        assertEquals(commissionDtoRequest.getRoom(),commissionDtoResponse.getRoom());
        assertEquals(commissionDtoRequest.getDate(),commissionDtoResponse.getDate());
        assertEquals(commissionDtoRequest.getTime(),commissionDtoResponse.getTime());
        assertEquals(commissionDtoRequest.getDuration(),commissionDtoResponse.getDuration());
    }

    @Test
    //10:00 dur 60
    public void testCreateCommissionWithDoctorWithoutReceptions() {
        String defaultAdminCookie = loginDefaultAdmin();

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        ImmutablePair<String,PatientRegisterUpdateResponse> patientResponse = registerPatient(dtoPatient);
        String cookiePatient = patientResponse.getLeft();
        PatientRegisterUpdateResponse patientDtoResponse = patientResponse.getRight();

        LocalDate startDate1 = LocalDate.of(2020,2,2);
        LocalDate endDate1 = LocalDate.of(2020,2,4);

        LocalDate startDate2 = LocalDate.of(2020,2,3);
        LocalDate endDate2 = LocalDate.of(2020,2,5);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays1 = new ArrayList<>();
        Collections.addAll(weekdays1,"Mon");
        WeekSchedule weekSchedule1 = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays1);

        List<String> weekdays2 = new ArrayList<>();
        Collections.addAll(weekdays2,"Tue");
        WeekSchedule weekSchedule2 = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays2);

        DoctorRegisterRequest dtoCreateDoctor1 = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate1.toString(), endDate1.toString(),null , weekSchedule1,30);
        DoctorRegisterResponse dtoResponseCreateDoctor1 = registerDoctor(defaultAdminCookie,dtoCreateDoctor1);

        DoctorRegisterRequest dtoCreateDoctor2 = new DoctorRegisterRequest("Иван","Иванов","Петрович","neurologist","113B"
                ,"neurologist_ivanov","122A001", startDate2.toString(), endDate2.toString(),null , weekSchedule2,30);
        DoctorRegisterResponse dtoResponseCreateDoctor2 = registerDoctor(defaultAdminCookie,dtoCreateDoctor2);

        String doctorCookie = login(new LoginDtoRequest(dtoCreateDoctor1.getLogin(),dtoCreateDoctor1.getPassword())).getLeft();

        List<Integer> doctorIds = new ArrayList<>();
        Collections.addAll(doctorIds,dtoResponseCreateDoctor1.getId(),dtoResponseCreateDoctor2.getId());
        CommissionRequest commissionDtoRequest = new CommissionRequest(patientDtoResponse.getId(),doctorIds,
                dtoCreateDoctor1.getRoom(),LocalDate.of(2020,2,3).toString(),
                LocalTime.of(10,0).toString(),60);
        CommissionResponse commissionDtoResponse =  createCommission(doctorCookie,commissionDtoRequest);

        assertNotNull(commissionDtoResponse.getTicket());
        assertEquals(commissionDtoRequest.getPatientId(),commissionDtoResponse.getPatientId());
        assertTrue(commissionDtoRequest.getDoctorIds().containsAll(commissionDtoResponse.getDoctorIds()));
        assertEquals(commissionDtoRequest.getRoom(),commissionDtoResponse.getRoom());
        assertEquals(commissionDtoRequest.getDate(),commissionDtoResponse.getDate());
        assertEquals(commissionDtoRequest.getTime(),commissionDtoResponse.getTime());
        assertEquals(commissionDtoRequest.getDuration(),commissionDtoResponse.getDuration());
    }


    @Test
    //10:10 dur 30
    public void testDeleteCommission() {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        registerAdmin(defaultAdminCookie,dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        ImmutablePair<String,PatientRegisterUpdateResponse> patientResponse = registerPatient(dtoPatient);
        String cookiePatient = patientResponse.getLeft();
        PatientRegisterUpdateResponse patientDtoResponse = patientResponse.getRight();

        LocalDate startDate = LocalDate.of(2020,2,2);
        LocalDate endDate = LocalDate.of(2020,2,4);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor1 = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor1 = registerDoctor(adminCookie,dtoCreateDoctor1);
        DoctorRegisterRequest dtoCreateDoctor2 = new DoctorRegisterRequest("Иван","Иванов","Петрович","neurologist","113B"
                ,"neurologist_ivanov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor2 = registerDoctor(adminCookie,dtoCreateDoctor2);

        String doctorCookie = login(new LoginDtoRequest(dtoCreateDoctor1.getLogin(),dtoCreateDoctor1.getPassword())).getLeft();

        List<Integer> doctorIds = new ArrayList<>();
        Collections.addAll(doctorIds,dtoResponseCreateDoctor1.getId(),dtoResponseCreateDoctor2.getId());
        CommissionRequest commissionDtoRequest =new CommissionRequest(patientDtoResponse.getId(),doctorIds,
                dtoCreateDoctor1.getRoom(),LocalDate.of(2020,2,3).toString(),
                LocalTime.of(10,10).toString(),30);
        CommissionResponse commissionDtoResponse =  createCommission(doctorCookie,commissionDtoRequest);

        String responseDeleteCommission = deleteCommission(cookiePatient,commissionDtoResponse.getTicket());

        assertEquals("{}",responseDeleteCommission);
    }

    @Test
    //10:00 dur 30
    public void testDeleteCommission2() {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        registerAdmin(defaultAdminCookie,dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        ImmutablePair<String,PatientRegisterUpdateResponse> patientResponse = registerPatient(dtoPatient);
        String cookiePatient = patientResponse.getLeft();
        PatientRegisterUpdateResponse patientDtoResponse = patientResponse.getRight();

        LocalDate startDate = LocalDate.of(2020,2,2);
        LocalDate endDate = LocalDate.of(2020,2,4);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor1 = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor1 = registerDoctor(adminCookie,dtoCreateDoctor1);
        DoctorRegisterRequest dtoCreateDoctor2 = new DoctorRegisterRequest("Иван","Иванов","Петрович","neurologist","113B"
                ,"neurologist_ivanov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor2 = registerDoctor(adminCookie,dtoCreateDoctor2);

        String doctorCookie = login(new LoginDtoRequest(dtoCreateDoctor1.getLogin(),dtoCreateDoctor1.getPassword())).getLeft();

        List<Integer> doctorIds = new ArrayList<>();
        Collections.addAll(doctorIds,dtoResponseCreateDoctor1.getId(),dtoResponseCreateDoctor2.getId());
        CommissionRequest commissionDtoRequest =new CommissionRequest(patientDtoResponse.getId(),doctorIds,
                dtoCreateDoctor1.getRoom(),LocalDate.of(2020,2,3).toString(),
                LocalTime.of(10,0).toString(),30);
        CommissionResponse commissionDtoResponse =  createCommission(doctorCookie,commissionDtoRequest);

        String responseDeleteCommission = deleteCommission(cookiePatient,commissionDtoResponse.getTicket());

        assertEquals("{}",responseDeleteCommission);
    }

    @Test
    //10:10 dur 15
    public void testDeleteCommission3() {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        registerAdmin(defaultAdminCookie,dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        ImmutablePair<String,PatientRegisterUpdateResponse> patientResponse = registerPatient(dtoPatient);
        String cookiePatient = patientResponse.getLeft();
        PatientRegisterUpdateResponse patientDtoResponse = patientResponse.getRight();

        LocalDate startDate = LocalDate.of(2020,2,2);
        LocalDate endDate = LocalDate.of(2020,2,4);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor1 = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor1 = registerDoctor(adminCookie,dtoCreateDoctor1);
        DoctorRegisterRequest dtoCreateDoctor2 = new DoctorRegisterRequest("Иван","Иванов","Петрович","neurologist","113B"
                ,"neurologist_ivanov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor2 = registerDoctor(adminCookie,dtoCreateDoctor2);

        String doctorCookie = login(new LoginDtoRequest(dtoCreateDoctor1.getLogin(),dtoCreateDoctor1.getPassword())).getLeft();

        List<Integer> doctorIds = new ArrayList<>();
        Collections.addAll(doctorIds,dtoResponseCreateDoctor1.getId(),dtoResponseCreateDoctor2.getId());
        CommissionRequest commissionDtoRequest =new CommissionRequest(patientDtoResponse.getId(),doctorIds,
                dtoCreateDoctor1.getRoom(),LocalDate.of(2020,2,3).toString(),
                LocalTime.of(10,10).toString(),15);
        CommissionResponse commissionDtoResponse =  createCommission(doctorCookie,commissionDtoRequest);

        String responseDeleteCommission = deleteCommission(cookiePatient,commissionDtoResponse.getTicket());

        assertEquals("{}",responseDeleteCommission);
    }

    @Test
    //10:00 dur 45
    public void testDeleteCommission4() {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        registerAdmin(defaultAdminCookie,dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        ImmutablePair<String,PatientRegisterUpdateResponse> patientResponse = registerPatient(dtoPatient);
        String cookiePatient = patientResponse.getLeft();
        PatientRegisterUpdateResponse patientDtoResponse = patientResponse.getRight();

        LocalDate startDate = LocalDate.of(2020,2,2);
        LocalDate endDate = LocalDate.of(2020,2,4);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor1 = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor1 = registerDoctor(adminCookie,dtoCreateDoctor1);
        DoctorRegisterRequest dtoCreateDoctor2 = new DoctorRegisterRequest("Иван","Иванов","Петрович","neurologist","113B"
                ,"neurologist_ivanov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor2 = registerDoctor(adminCookie,dtoCreateDoctor2);

        String doctorCookie = login(new LoginDtoRequest(dtoCreateDoctor1.getLogin(),dtoCreateDoctor1.getPassword())).getLeft();

        List<Integer> doctorIds = new ArrayList<>();
        Collections.addAll(doctorIds,dtoResponseCreateDoctor1.getId(),dtoResponseCreateDoctor2.getId());
        CommissionRequest commissionDtoRequest =new CommissionRequest(patientDtoResponse.getId(),doctorIds,
                dtoCreateDoctor1.getRoom(),LocalDate.of(2020,2,3).toString(),
                LocalTime.of(10,0).toString(),45);
        CommissionResponse commissionDtoResponse =  createCommission(doctorCookie,commissionDtoRequest);

        String responseDeleteCommission = deleteCommission(cookiePatient,commissionDtoResponse.getTicket());

        assertEquals("{}",responseDeleteCommission);
    }

    @Test
    //10:10 dur 70
    public void testDeleteCommission5() {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        registerAdmin(defaultAdminCookie,dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        ImmutablePair<String,PatientRegisterUpdateResponse> patientResponse = registerPatient(dtoPatient);
        String cookiePatient = patientResponse.getLeft();
        PatientRegisterUpdateResponse patientDtoResponse = patientResponse.getRight();

        LocalDate startDate = LocalDate.of(2020,2,2);
        LocalDate endDate = LocalDate.of(2020,2,4);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor1 = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor1 = registerDoctor(adminCookie,dtoCreateDoctor1);
        DoctorRegisterRequest dtoCreateDoctor2 = new DoctorRegisterRequest("Иван","Иванов","Петрович","neurologist","113B"
                ,"neurologist_ivanov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor2 = registerDoctor(adminCookie,dtoCreateDoctor2);

        String doctorCookie = login(new LoginDtoRequest(dtoCreateDoctor1.getLogin(),dtoCreateDoctor1.getPassword())).getLeft();

        List<Integer> doctorIds = new ArrayList<>();
        Collections.addAll(doctorIds,dtoResponseCreateDoctor1.getId(),dtoResponseCreateDoctor2.getId());
        CommissionRequest commissionDtoRequest =new CommissionRequest(patientDtoResponse.getId(),doctorIds,
                dtoCreateDoctor1.getRoom(),LocalDate.of(2020,2,3).toString(),
                LocalTime.of(10,10).toString(),70);
        CommissionResponse commissionDtoResponse =  createCommission(doctorCookie,commissionDtoRequest);

        String responseDeleteCommission = deleteCommission(cookiePatient,commissionDtoResponse.getTicket());

        assertEquals("{}",responseDeleteCommission);
    }

    @Test
    //10:00 dur 90
    public void testDeleteCommission6() {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        registerAdmin(defaultAdminCookie,dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        ImmutablePair<String,PatientRegisterUpdateResponse> patientResponse = registerPatient(dtoPatient);
        String cookiePatient = patientResponse.getLeft();
        PatientRegisterUpdateResponse patientDtoResponse = patientResponse.getRight();

        LocalDate startDate = LocalDate.of(2020,2,2);
        LocalDate endDate = LocalDate.of(2020,2,4);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor1 = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor1 = registerDoctor(adminCookie,dtoCreateDoctor1);
        DoctorRegisterRequest dtoCreateDoctor2 = new DoctorRegisterRequest("Иван","Иванов","Петрович","neurologist","113B"
                ,"neurologist_ivanov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor2 = registerDoctor(adminCookie,dtoCreateDoctor2);

        String doctorCookie = login(new LoginDtoRequest(dtoCreateDoctor1.getLogin(),dtoCreateDoctor1.getPassword())).getLeft();

        List<Integer> doctorIds = new ArrayList<>();
        Collections.addAll(doctorIds,dtoResponseCreateDoctor1.getId(),dtoResponseCreateDoctor2.getId());
        CommissionRequest commissionDtoRequest =new CommissionRequest(patientDtoResponse.getId(),doctorIds,
                dtoCreateDoctor1.getRoom(),LocalDate.of(2020,2,3).toString(),
                LocalTime.of(10,0).toString(),90);
        CommissionResponse commissionDtoResponse =  createCommission(doctorCookie,commissionDtoRequest);

        String responseDeleteCommission = deleteCommission(cookiePatient,commissionDtoResponse.getTicket());

        assertEquals("{}",responseDeleteCommission);
    }

    @Test
    public void testDeleteCommissionWithDoctorWithoutReceptions() {
        String defaultAdminCookie = loginDefaultAdmin();

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        ImmutablePair<String,PatientRegisterUpdateResponse> patientResponse = registerPatient(dtoPatient);
        String cookiePatient = patientResponse.getLeft();
        PatientRegisterUpdateResponse patientDtoResponse = patientResponse.getRight();

        LocalDate startDate1 = LocalDate.of(2020,2,2);
        LocalDate endDate1 = LocalDate.of(2020,2,4);

        LocalDate startDate2 = LocalDate.of(2020,2,3);
        LocalDate endDate2 = LocalDate.of(2020,2,5);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays1 = new ArrayList<>();
        Collections.addAll(weekdays1,"Mon");
        WeekSchedule weekSchedule1 = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays1);

        List<String> weekdays2 = new ArrayList<>();
        Collections.addAll(weekdays2,"Tue");
        WeekSchedule weekSchedule2 = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays2);

        DoctorRegisterRequest dtoCreateDoctor1 = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate1.toString(), endDate1.toString(),null , weekSchedule1,30);
        DoctorRegisterResponse dtoResponseCreateDoctor1 = registerDoctor(defaultAdminCookie,dtoCreateDoctor1);

        DoctorRegisterRequest dtoCreateDoctor2 = new DoctorRegisterRequest("Иван","Иванов","Петрович","neurologist","113B"
                ,"neurologist_ivanov","122A001", startDate2.toString(), endDate2.toString(),null , weekSchedule2,30);
        DoctorRegisterResponse dtoResponseCreateDoctor2 = registerDoctor(defaultAdminCookie,dtoCreateDoctor2);

        String doctorCookie = login(new LoginDtoRequest(dtoCreateDoctor1.getLogin(),dtoCreateDoctor1.getPassword())).getLeft();

        List<Integer> doctorIds = new ArrayList<>();
        Collections.addAll(doctorIds,dtoResponseCreateDoctor1.getId(),dtoResponseCreateDoctor2.getId());
        CommissionRequest commissionDtoRequest = new CommissionRequest(patientDtoResponse.getId(),doctorIds,
                dtoCreateDoctor1.getRoom(),LocalDate.of(2020,2,3).toString(),
                LocalTime.of(10,0).toString(),45);
        CommissionResponse commissionDtoResponse =  createCommission(doctorCookie,commissionDtoRequest);
        assertNotNull(commissionDtoResponse.getTicket());

        String responseDelete = deleteCommission(cookiePatient,commissionDtoResponse.getTicket());
        assertEquals("{}",responseDelete);

    }

    @Test
    //Нарушение прав доступа
    public void testCreateCommissionFail() {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        registerAdmin(defaultAdminCookie,dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        ImmutablePair<String,PatientRegisterUpdateResponse> patientResponse = registerPatient(dtoPatient);
        String cookiePatient = patientResponse.getLeft();
        PatientRegisterUpdateResponse patientDtoResponse = patientResponse.getRight();

        LocalDate startDate = LocalDate.of(2020,2,2);
        LocalDate endDate = LocalDate.of(2020,2,4);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor1 = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor1 = registerDoctor(adminCookie,dtoCreateDoctor1);
        DoctorRegisterRequest dtoCreateDoctor2 = new DoctorRegisterRequest("Иван","Иванов","Петрович","neurologist","113B"
                ,"neurologist_ivanov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor2 = registerDoctor(adminCookie,dtoCreateDoctor2);

        String doctorCookie = login(new LoginDtoRequest(dtoCreateDoctor1.getLogin(),dtoCreateDoctor1.getPassword())).getLeft();

        List<Integer> doctorIds = new ArrayList<>();
        Collections.addAll(doctorIds,dtoResponseCreateDoctor1.getId(),dtoResponseCreateDoctor2.getId());
        CommissionRequest commissionDtoRequest =new CommissionRequest(patientDtoResponse.getId(),doctorIds,
                dtoCreateDoctor1.getRoom(),LocalDate.of(2020,2,3).toString(),
                LocalTime.of(10,10).toString(),30);
        try {
            CommissionResponse commissionDtoResponse =  createCommission(cookiePatient,commissionDtoRequest);
        }catch (HttpClientErrorException e){
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
        }
    }

    @Test
    //Запись в воскресенье
    public void testCreateCommissionFail2() {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        registerAdmin(defaultAdminCookie,dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        ImmutablePair<String,PatientRegisterUpdateResponse> patientResponse = registerPatient(dtoPatient);
        String cookiePatient = patientResponse.getLeft();
        PatientRegisterUpdateResponse patientDtoResponse = patientResponse.getRight();

        LocalDate startDate = LocalDate.of(2020,2,2);
        LocalDate endDate = LocalDate.of(2020,2,4);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor1 = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor1 = registerDoctor(adminCookie,dtoCreateDoctor1);
        DoctorRegisterRequest dtoCreateDoctor2 = new DoctorRegisterRequest("Иван","Иванов","Петрович","neurologist","113B"
                ,"neurologist_ivanov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor2 = registerDoctor(adminCookie,dtoCreateDoctor2);

        String doctorCookie = login(new LoginDtoRequest(dtoCreateDoctor1.getLogin(),dtoCreateDoctor1.getPassword())).getLeft();

        List<Integer> doctorIds = new ArrayList<>();
        Collections.addAll(doctorIds,dtoResponseCreateDoctor1.getId(),dtoResponseCreateDoctor2.getId());
        CommissionRequest commissionDtoRequest =new CommissionRequest(patientDtoResponse.getId(),doctorIds,
                dtoCreateDoctor1.getRoom(),LocalDate.of(2020,2,2).toString(),
                LocalTime.of(10,10).toString(),30);
        try {
            CommissionResponse commissionDtoResponse =  createCommission(doctorCookie,commissionDtoRequest);
        }catch (HttpClientErrorException e){
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
        }
    }

   

}
