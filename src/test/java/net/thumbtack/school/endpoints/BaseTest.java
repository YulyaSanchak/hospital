package net.thumbtack.school.endpoints;




import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import net.thumbtack.school.dto.requests.*;
import net.thumbtack.school.dto.requests.register_update_admin.AdminRegisterRequest;
import net.thumbtack.school.dto.requests.register_update_admin.AdminUpdateRequest;
import net.thumbtack.school.dto.requests.register_update_doctor.DoctorRegisterRequest;
import net.thumbtack.school.dto.requests.register_update_doctor.DoctorScheduleUpdateRequest;
import net.thumbtack.school.dto.requests.register_update_patient.PatientRegisterRequest;
import net.thumbtack.school.dto.requests.register_update_patient.PatientUpdateRequest;
import net.thumbtack.school.dto.responses.*;
import net.thumbtack.school.dto.responses.failure.JsonErrorResponse;
import net.thumbtack.school.dto.responses.failure.JsonSimpleErrorResponse;
import net.thumbtack.school.dto.responses.settings.GetSettingsResponse;
import net.thumbtack.school.dto.responses.statistics.StatisticResponse;
import net.thumbtack.school.dto.responses.tickets.AllTicketsResponse;
import net.thumbtack.school.dto.responses.tickets.CommissionResponse;
import net.thumbtack.school.dto.responses.tickets.ReceptionTicketResponse;
import net.thumbtack.school.utils.ConfigUtils;
import net.thumbtack.school.utils.MyBatisUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class BaseTest {
    protected RestTemplate template = new RestTemplate();
    protected static String urlPrefix;
    protected Gson json = new Gson();
    private static boolean setUpIsDone = false;

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @BeforeClass
    public static void before() {
        new ConfigUtils("config.properties");
        urlPrefix = String.format("http://localhost:%d", ConfigUtils.getRestHttpPort());
    }

    @Before
    public void deleteAll(){
        template.exchange(urlPrefix + "/api/debug/clear", HttpMethod.POST, null, String.class);
    }



    protected AdminRegisterResponse registerAdmin(String adminCookie, AdminRegisterRequest dto) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cookie", adminCookie);
        HttpEntity<String> response = template.exchange(urlPrefix + "/api/admin", HttpMethod.POST, new HttpEntity<>(dto,headers), String.class);
        AdminRegisterResponse dtoResponse = json.fromJson(response.getBody(),AdminRegisterResponse.class);
        assert  dtoResponse != null;
        return dtoResponse;
    }

    protected AdminRegisterResponse updateAdmin(String adminCookie, AdminUpdateRequest dto){
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", adminCookie);
        HttpEntity<String> response = template.exchange(urlPrefix + "/api/admin",
                HttpMethod.PUT, new HttpEntity<>(dto,requestHeaders), String.class);
        AdminRegisterResponse dtoResponse = json.fromJson(response.getBody(),AdminRegisterResponse.class);
        assert dtoResponse != null;
        return dtoResponse;
    }

    protected String loginDefaultAdmin(){
        ImmutablePair<String,LoginDtoResponse> response = login(new LoginDtoRequest("admin","admin!!!"));
        return response.getLeft();
    }

    protected DoctorRegisterResponse registerDoctor(String cookie,DoctorRegisterRequest dto) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cookie", cookie);
        HttpEntity<String> response = template.exchange(urlPrefix + "/api/doctors", HttpMethod.POST,
                new HttpEntity<>(dto,headers), String.class);
        DoctorRegisterResponse dtoResponse = json.fromJson(response.getBody(),DoctorRegisterResponse.class);
        assert  dtoResponse != null;
        return dtoResponse;
    }

    protected ImmutablePair<String, PatientRegisterUpdateResponse> registerPatient(PatientRegisterRequest dto) {
        HttpEntity<PatientRegisterRequest> request = new HttpEntity<>(dto);
        HttpEntity<String> response = template.exchange(urlPrefix + "/api/patients", HttpMethod.POST, request, String.class);
        HttpHeaders headersResponse = response.getHeaders();
        String cookie = headersResponse.getFirst(HttpHeaders.SET_COOKIE);
        assertNotNull(cookie);
        PatientRegisterUpdateResponse dtoResponse = json.fromJson(response.getBody(), PatientRegisterUpdateResponse.class);
        assert  dtoResponse != null;
        return new ImmutablePair<>(cookie,dtoResponse);
    }

    protected PatientRegisterUpdateResponse getPatientInfoById(String cookieAdminDoctor, int patientId){
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", cookieAdminDoctor);
        HttpEntity<String> response = template.exchange(urlPrefix + "/api/patients/" + patientId,
                HttpMethod.GET, new HttpEntity<>(requestHeaders), String.class);
        PatientRegisterUpdateResponse dtoResponse = json.fromJson(response.getBody()
                , PatientRegisterUpdateResponse.class);
        assert dtoResponse != null;
        return dtoResponse;
    }

    protected PatientRegisterUpdateResponse updatePatient(String cookiePatient, PatientUpdateRequest dto){
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", cookiePatient);
        HttpEntity<String> response = template.exchange(urlPrefix + "/api/client", HttpMethod.PUT,
                new HttpEntity<>(dto, requestHeaders), String.class);

        PatientRegisterUpdateResponse dtoResponse = json.fromJson(response.getBody(), PatientRegisterUpdateResponse.class);
        assert dtoResponse != null;
        return  dtoResponse;
    }

    protected ImmutablePair<String,LoginDtoResponse> login(LoginDtoRequest dto){
        HttpEntity<LoginDtoRequest> request = new HttpEntity<>(dto);
        HttpEntity<String> response = template.exchange(urlPrefix + "/api/session", HttpMethod.POST, request, String.class);
        HttpHeaders headersResponse = response.getHeaders();
        String cookie = headersResponse.getFirst(HttpHeaders.SET_COOKIE);
        assertNotNull(cookie);
        LoginDtoResponse dtoResponse = json.fromJson(response.getBody(), LoginDtoResponse.class);
        assert  dtoResponse != null;
        return new ImmutablePair<>(cookie,dtoResponse);
    }

    protected ImmutablePair<String,LoginDtoResponse> getAccountInfo(String cookie){
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", cookie);
        HttpEntity<String> accountResponse = template.exchange(urlPrefix + "/api/account", HttpMethod.GET,
                new HttpEntity<>(requestHeaders), String.class);
        HttpHeaders responseHeaders = accountResponse.getHeaders();
        String accountCookie = responseHeaders.getFirst(HttpHeaders.SET_COOKIE);
        assertNotNull(accountCookie);
        LoginDtoResponse accountDtoResponse = json.fromJson(accountResponse.getBody(), LoginDtoResponse.class);
        assert accountDtoResponse != null;
        return new ImmutablePair<>(cookie,accountDtoResponse);
    }

    protected String logout(String cookie){
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie",cookie);
        HttpEntity<String> response = template.exchange(urlPrefix + "/api/session", HttpMethod.DELETE,
                new HttpEntity<>(requestHeaders), String.class);
        return response.getBody();
    }

    protected DoctorRegisterResponse getDoctorInfo(String cookie, int doctorId, String filter,
                                                   LocalDate startDate, LocalDate endDate) throws UnsupportedEncodingException {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cookie",cookie);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlPrefix + "/api/doctors/"+doctorId)
                .queryParam("filter", URLEncoder.encode(filter,"UTF-8"))
                .queryParam("startDate",startDate)
                .queryParam("endDate",endDate);
        HttpEntity<String> getResponse = template.exchange(builder.toUriString(),
                HttpMethod.GET, new HttpEntity<>(headers), String.class);
        DoctorRegisterResponse doctorInfoResponse = json.fromJson(getResponse.getBody(),DoctorRegisterResponse.class);
        assert doctorInfoResponse != null;
        return doctorInfoResponse;
    }

    protected List<DoctorRegisterResponse> getDoctorsInfo(String cookie, String specialityName, String filter,
                                                          LocalDate startDate, LocalDate endDate) throws UnsupportedEncodingException {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cookie",cookie);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlPrefix + "/api/doctors")
                .queryParam("speciality", URLEncoder.encode(specialityName,"UTF-8"))
                .queryParam("filter", URLEncoder.encode(filter,"UTF-8"))
                .queryParam("startDate",startDate)
                .queryParam("endDate",endDate);
        HttpEntity<String> getResponse = template.exchange(builder.toUriString(),
                HttpMethod.GET, new HttpEntity<>(headers), String.class);
        Type listType = new TypeToken<List<DoctorRegisterResponse>>() {}.getType();
        List<DoctorRegisterResponse> doctorInfoResponse = json.fromJson(getResponse.getBody(),listType);
        assert doctorInfoResponse != null;
        return doctorInfoResponse;
    }

    protected DoctorRegisterResponse updateDoctorSchedule(String adminCookie, int doctorId, DoctorScheduleUpdateRequest dto){
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", adminCookie);
        HttpEntity<String> response = template.exchange(urlPrefix + "/api/doctor/"+doctorId,
                HttpMethod.PUT, new HttpEntity<>(dto,requestHeaders), String.class);
        DoctorRegisterResponse dtoResponse = json.fromJson(response.getBody(),DoctorRegisterResponse.class);
        assert dtoResponse != null;
        return dtoResponse;
    }

    protected String deleteDoctor(String adminCookie, int doctorId){
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", adminCookie);
        HttpEntity<String> response = template.exchange(urlPrefix + "/api/doctor/"+doctorId,
                HttpMethod.DELETE, new HttpEntity<>(requestHeaders), String.class);
        return response.getBody();
    }
    protected CommissionResponse createCommission(String doctorCookie, CommissionRequest dto){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cookie", doctorCookie);
        HttpEntity<String> response = template.exchange(urlPrefix + "/api/comissions", HttpMethod.POST,
                new HttpEntity<>(dto,headers), String.class);
        CommissionResponse dtoResponse = json.fromJson(response.getBody(), CommissionResponse.class);
        assert dtoResponse != null;
        return dtoResponse;
    }
    protected String deleteCommission(String patientCookie,String ticketNumber){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cookie", patientCookie);
        HttpEntity<String> response = template.exchange(urlPrefix + "/api/comissions/"+ticketNumber, HttpMethod.DELETE,
                new HttpEntity<>(headers), String.class);
        return response.getBody();
    }

    protected GetSettingsResponse getSettings(String cookie){
        HttpHeaders headers = new HttpHeaders();
        headers .add("Cookie", cookie);
        HttpEntity<String> response = template.exchange(urlPrefix + "/api/settings", HttpMethod.GET,
                new HttpEntity<>(headers), String.class);
        GetSettingsResponse dtoResponse = json.fromJson(response.getBody(), GetSettingsResponse.class);
        assert dtoResponse != null;
        return dtoResponse;
    }

    protected StatisticResponse getStatistic(String cookie, String filter, String specialityName, LocalDate startDate, LocalDate endDate) throws UnsupportedEncodingException {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cookie", cookie);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlPrefix + "/api/statistics")
                .queryParam("filter", URLEncoder.encode(filter,"UTF-8"))
                .queryParam("specialityName", specialityName)
                .queryParam("startDate",startDate.toString())
                .queryParam("endDate",endDate.toString());
        HttpEntity<String> response = template.exchange(builder.toUriString(),
                HttpMethod.GET, new HttpEntity<>(headers), String.class);
        StatisticResponse dtoResponse = json.fromJson(response.getBody(),StatisticResponse.class);
        assert dtoResponse != null;
        return dtoResponse;
    }

    protected ReceptionTicketResponse createReceptions(String patientCookie, CreateTicketRequest dto){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cookie", patientCookie);
        HttpEntity<String> ticketResp = template.exchange(urlPrefix + "/api/tickets", HttpMethod.POST,
                new HttpEntity<>(dto,headers), String.class);
        ReceptionTicketResponse ticketResponse = json.fromJson(ticketResp.getBody(), ReceptionTicketResponse.class);
        assert ticketResponse != null;
        return ticketResponse;
    }

    protected String deleteReception(String cookie, String numberTicket){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cookie",cookie);
        HttpEntity<String> ticketDelete = template.exchange(urlPrefix + "/api/tickets/"+numberTicket, HttpMethod.DELETE,
                new HttpEntity<>(headers), String.class);
        return ticketDelete.getBody();
    }

    protected List<AllTicketsResponse> getPatientsTickets(String patientCookie){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cookie",patientCookie);
        HttpEntity<String> response = template.exchange(urlPrefix+"/api/tickets", HttpMethod.GET,
                new HttpEntity<>(headers),String.class);
        Type listType = new TypeToken<List<AllTicketsResponse>>() {}.getType();
        List<AllTicketsResponse> dtoResponses = json.fromJson(response.getBody(),listType);
        assert dtoResponses != null;
        return dtoResponses;
    }

    protected void allExpectedErrorsContains(JsonErrorResponse errors, String ... messages) {
        assertEquals(messages.length, errors.getJsons().size());
        List<String> messagesList = Arrays.asList(messages);
        for (JsonSimpleErrorResponse error: errors.getJsons()) {
            assertTrue(messagesList.contains(error.getMessage()));
        }
    }
}
