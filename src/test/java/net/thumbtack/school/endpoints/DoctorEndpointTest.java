package net.thumbtack.school.endpoints;


import net.thumbtack.school.dto.requests.register_update_admin.AdminRegisterRequest;
import net.thumbtack.school.dto.requests.register_update_patient.PatientRegisterRequest;
import net.thumbtack.school.dto.requests.register_update_doctor.DoctorScheduleUpdateRequest;
import net.thumbtack.school.dto.requests.LoginDtoRequest;
import net.thumbtack.school.dto.requests.register_update_doctor.DayScheduleRequest;
import net.thumbtack.school.dto.requests.register_update_doctor.DoctorRegisterRequest;
import net.thumbtack.school.dto.requests.register_update_doctor.WeekSchedule;
import net.thumbtack.school.dto.responses.DoctorRegisterResponse;
import net.thumbtack.school.dto.responses.LoginDtoResponse;
import net.thumbtack.school.dto.responses.failure.JsonErrorResponse;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import sun.net.www.http.HttpClient;

import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class DoctorEndpointTest extends BaseTest {
    @Test
    public void testCreateDoctorSameReceptionTimes() {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        registerAdmin(defaultAdminCookie,dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        LocalDate startDate = LocalDate.of(2020,2,1);
        LocalDate endDate = LocalDate.of(2020,3,15);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon","Wed","Fri");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dto = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,20);

        DoctorRegisterResponse dtoResponse = registerDoctor(adminCookie,dto);
        assertEquals(dto.getFirstName(), dtoResponse.getFirstName());
        assertEquals(dto.getLastName(), dtoResponse.getLastName());
        assertEquals(dto.getPatronymic(), dtoResponse.getPatronymic());
        assertEquals(dto.getSpeciality(), dtoResponse.getSpeciality());
        assertEquals(dto.getRoom(),dtoResponse.getRoom());

        LoginDtoRequest loginDtoRequest = new LoginDtoRequest(dto.getLogin(), dto.getPassword());
        ImmutablePair<String,LoginDtoResponse> loginDoctorResponse = login(loginDtoRequest);
        LoginDtoResponse loginDoctorDtoResponse = loginDoctorResponse.getRight();
        assertEquals(dto.getFirstName(), loginDoctorDtoResponse.getFirstName());
        assertEquals(dto.getLastName(), loginDoctorDtoResponse.getLastName());
        assertEquals(dto.getPatronymic(), loginDoctorDtoResponse.getPatronymic());
        assertEquals(dto.getSpeciality(), loginDoctorDtoResponse.getSpeciality());
        assertEquals(dto.getRoom(), loginDoctorDtoResponse.getRoom());

        String responseLogout = logout(loginDoctorResponse.getLeft());
        assertEquals("{}",responseLogout);
    }

    @Test
    public void testCreateDoctorWithEmptyWeekdaysList() {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        registerAdmin(defaultAdminCookie,dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        LocalDate startDate = LocalDate.of(2020,2,1);
        LocalDate endDate = LocalDate.of(2020,2,15);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dto = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,20);

        DoctorRegisterResponse dtoResponse = registerDoctor(adminCookie,dto);
        assertEquals(dto.getFirstName(), dtoResponse.getFirstName());
        assertEquals(dto.getLastName(), dtoResponse.getLastName());
        assertEquals(dto.getPatronymic(), dtoResponse.getPatronymic());
        assertEquals(dto.getSpeciality(), dtoResponse.getSpeciality());
        assertEquals(dto.getRoom(),dtoResponse.getRoom());

    }

    @Test
    public void testCreateDoctorWithNullWeekdaysList() {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        registerAdmin(defaultAdminCookie,dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        LocalDate startDate = LocalDate.of(2020,2,1);
        LocalDate endDate = LocalDate.of(2020,2,15);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);

        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),null);

        DoctorRegisterRequest dto = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,20);

        DoctorRegisterResponse dtoResponse = registerDoctor(adminCookie,dto);
        assertEquals(dto.getFirstName(), dtoResponse.getFirstName());
        assertEquals(dto.getLastName(), dtoResponse.getLastName());
        assertEquals(dto.getPatronymic(), dtoResponse.getPatronymic());
        assertEquals(dto.getSpeciality(), dtoResponse.getSpeciality());
        assertEquals(dto.getRoom(),dtoResponse.getRoom());
    }

    @Test
    public void testCreateDoctorWithDifferentReceptionTimes () {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        registerAdmin(defaultAdminCookie,dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        LocalDate startDate = LocalDate.of(2020,2,1);
        LocalDate endDate = LocalDate.of(2020,3,15);

        List<DayScheduleRequest> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,
                new DayScheduleRequest("Mon",LocalTime.of(14,0).toString(),
                        LocalTime.of(18,0).toString()),
                new DayScheduleRequest("Fri",LocalTime.of(9,0).toString(),
                        LocalTime.of(14,0).toString())
        );
        DoctorRegisterRequest dto = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),weekdays , null ,20);

        DoctorRegisterResponse dtoResponse = registerDoctor(adminCookie,dto);
        assertEquals(dto.getFirstName(), dtoResponse.getFirstName());
        assertEquals(dto.getLastName(), dtoResponse.getLastName());
        assertEquals(dto.getPatronymic(), dtoResponse.getPatronymic());
        assertEquals(dto.getSpeciality(), dtoResponse.getSpeciality());
        assertEquals(dto.getRoom(),dtoResponse.getRoom());
    }

    @Test
    public void testCreateDoctorSameReceptionTimesAndGetInfo() throws UnsupportedEncodingException {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        registerAdmin(defaultAdminCookie,dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        LocalDate startDate = LocalDate.of(2020,2,1);
        LocalDate endDate = LocalDate.of(2020,3,15);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon","Wed","Fri");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dto = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,20);
        DoctorRegisterResponse dtoDoctorResponse = registerDoctor(adminCookie,dto);

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        String cookiePatient  = registerPatient(dtoPatient).getLeft();

        DoctorRegisterResponse doctorInfoResponse;
        doctorInfoResponse = getDoctorInfo(cookiePatient,dtoDoctorResponse.getId(),"yes",
                LocalDate.of(2020,2,5),LocalDate.of(2020,2,25));
        assertEquals(17,doctorInfoResponse.getScheduleResponse().size());

        doctorInfoResponse = getDoctorInfo(cookiePatient,dtoDoctorResponse.getId(),"yes",
                null,LocalDate.of(2020,2,25));
        assertTrue(doctorInfoResponse.getScheduleResponse().isEmpty());

        doctorInfoResponse = getDoctorInfo(cookiePatient,dtoDoctorResponse.getId(),"yes",
                LocalDate.of(2020,2,25),null);
        assertEquals(8, doctorInfoResponse.getScheduleResponse().size());

        doctorInfoResponse = getDoctorInfo(cookiePatient,dtoDoctorResponse.getId(),"no",
                LocalDate.of(2020,2,5),LocalDate.of(2020,2,25));
        assertNull(doctorInfoResponse.getScheduleResponse());
    }

    @Test
    public void testGetInfoAboutDoctors() throws UnsupportedEncodingException {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        registerAdmin(defaultAdminCookie,dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        LocalDate startDate = LocalDate.of(2020,2,1);
        LocalDate endDate = LocalDate.of(2020,3,15);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon","Wed","Fri");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dto1 = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,20);
        registerDoctor(adminCookie,dto1);

        DoctorRegisterRequest dto2 = new DoctorRegisterRequest("Петр","Иванов","Петрович","therapist","113B"
                ,"therapist_ivanov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,20);
        registerDoctor(adminCookie,dto2);

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        String cookiePatient = registerPatient(dtoPatient).getLeft();

        List<DoctorRegisterResponse> doctorInfoResponse = getDoctorsInfo(cookiePatient,"therapist","yes",
                LocalDate.of(2020,2,5),LocalDate.of(2020,2,25));
        assertEquals(2,doctorInfoResponse.size());
        assertTrue(doctorInfoResponse.get(0).getScheduleResponse().containsAll(doctorInfoResponse.get(1).getScheduleResponse()));
    }

    @Test
    public void testUpdateDoctorScheduleWithSameReceptionTimes() {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        registerAdmin(defaultAdminCookie,dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        LocalDate startDate = LocalDate.of(2020,2,1);
        LocalDate endDate = LocalDate.of(2020,3,15);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon","Wed","Fri");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,20);
        DoctorRegisterResponse dtoDoctorResponse = registerDoctor(adminCookie,dtoCreateDoctor);
        assertEquals(18,dtoDoctorResponse.getScheduleResponse().size());

        LocalDate newStartDate = LocalDate.of(2020,3,16);
        LocalDate newEndDate = LocalDate.of(2020,3,25);
        LocalTime newStartTime = LocalTime.of(10,0);
        LocalTime newEndTime = LocalTime.of(15,0);
        List<String> newWeekdays = new ArrayList<>();
        Collections.addAll(newWeekdays,"Tue","Thu","Fri");
        WeekSchedule newWeekSchedule = new WeekSchedule(newStartTime.toString(),newEndTime.toString(),newWeekdays);

        DoctorScheduleUpdateRequest dto = new DoctorScheduleUpdateRequest(newStartDate.toString(),
                newEndDate.toString(),null, newWeekSchedule,30);
        DoctorRegisterResponse dtoResponse = updateDoctorSchedule(adminCookie,dtoDoctorResponse.getId(),dto);
        assertEquals(22,dtoResponse.getScheduleResponse().size());
    }


    @Test
    public void testUpdateDoctorSchedule() {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        registerAdmin(defaultAdminCookie,dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        LocalDate startDate = LocalDate.of(2020,2,1);
        LocalDate endDate = LocalDate.of(2020,3,15);
        List<DayScheduleRequest> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,
                new DayScheduleRequest("Mon",LocalTime.of(14,0).toString(),
                        LocalTime.of(18,0).toString()),
                new DayScheduleRequest("Fri",LocalTime.of(9,0).toString(),
                        LocalTime.of(14,0).toString())
        );

        DoctorRegisterRequest dtoCreateDoctor = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),weekdays , null,20);
        DoctorRegisterResponse dtoDoctorResponse = registerDoctor(adminCookie,dtoCreateDoctor);
        assertEquals(12,dtoDoctorResponse.getScheduleResponse().size());

        LocalDate newStartDate = LocalDate.of(2020,2,1);
        LocalDate newEndDate = LocalDate.of(2020,3,15);
        LocalTime newStartTime = LocalTime.of(10,0);
        LocalTime newEndTime = LocalTime.of(15,0);
        List<String> newWeekdays = new ArrayList<>();
        Collections.addAll(newWeekdays,"Tue","Wed","Thu");
        WeekSchedule newWeekSchedule = new WeekSchedule(newStartTime.toString(),newEndTime.toString(),newWeekdays);

        DoctorScheduleUpdateRequest dto = new DoctorScheduleUpdateRequest(newStartDate.toString(),
                newEndDate.toString(),null, newWeekSchedule,30);

        DoctorRegisterResponse dtoResponse = updateDoctorSchedule(adminCookie,dtoDoctorResponse.getId(),dto);
        assertEquals(18,dtoResponse.getScheduleResponse().size());

    }

    @Test
    public void testDeleteDoctorScheduleWithSameReceptionTimes() {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        registerAdmin(defaultAdminCookie,dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        LocalDate startDate = LocalDate.of(2020,2,1);
        LocalDate endDate = LocalDate.of(2020,3,15);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon","Wed","Fri");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor = new DoctorRegisterRequest("Петр","Петров",
                "Петрович","therapist","112A","therapist_petrov","122A001",
                startDate.toString(), endDate.toString(),null , weekSchedule,20);
        DoctorRegisterResponse dtoResponseCreateDoctor = registerDoctor(adminCookie,dtoCreateDoctor);
        assertEquals(18,dtoResponseCreateDoctor.getScheduleResponse().size());

        String response = deleteDoctor(adminCookie,dtoResponseCreateDoctor.getId());
        assertEquals("{}",response);
    }

    @Test
    public void testCreateDoctorFail1() {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван", "Иванов", "Иванович",
                "position", "login", "password");
        registerAdmin(defaultAdminCookie, dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        LocalTime startTime = LocalTime.of(8, 0);
        LocalTime endTime = LocalTime.of(14, 0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays, "Mon", "Wed", "Fri");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(), endTime.toString(), weekdays);

        DoctorRegisterRequest dto = new DoctorRegisterRequest("Петр", "Петрович", "Петрович", "therapist", "112A"
                , "therapist_petrov", "122A001", null, null, null, weekSchedule, 20);


        HttpHeaders headers = new HttpHeaders();
        headers.add("Cookie", adminCookie);
        try {
            HttpEntity<String> response = template.exchange(urlPrefix + "/api/doctors", HttpMethod.POST, new HttpEntity<>(dto, headers), String.class);
        } catch (HttpClientErrorException e) {
            JsonErrorResponse error = json.fromJson(e.getResponseBodyAsString(), JsonErrorResponse.class);
            allExpectedErrorsContains(error, "dateStart can't be null", "dateEnd can't be null");
        }
    }

    @Test
    //Нет запрашиваемой специальности
    public void testCreateDoctorFail12() {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван", "Иванов", "Иванович",
                "position", "login", "password");
        registerAdmin(defaultAdminCookie, dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        LocalDate startDate = LocalDate.of(2020,2,1);
        LocalDate endDate = LocalDate.of(2020,3,15);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon","Wed","Fri");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dto = new DoctorRegisterRequest("Петр","Петров","Петрович","pediatrician","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,20);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Cookie", adminCookie);
        try {
            HttpEntity<String> response = template.exchange(urlPrefix + "/api/doctors", HttpMethod.POST, new HttpEntity<>(dto, headers), String.class);
        } catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
        }
    }
}
