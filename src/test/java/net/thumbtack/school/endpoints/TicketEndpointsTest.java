package net.thumbtack.school.endpoints;

import net.thumbtack.school.dto.requests.CommissionRequest;
import net.thumbtack.school.dto.requests.CreateTicketRequest;
import net.thumbtack.school.dto.requests.LoginDtoRequest;
import net.thumbtack.school.dto.requests.register_update_patient.PatientRegisterRequest;
import net.thumbtack.school.dto.requests.register_update_doctor.DoctorRegisterRequest;
import net.thumbtack.school.dto.requests.register_update_doctor.WeekSchedule;
import net.thumbtack.school.dto.responses.*;
import net.thumbtack.school.dto.responses.tickets.AllTicketsResponse;
import net.thumbtack.school.dto.responses.tickets.CommissionResponse;
import net.thumbtack.school.dto.responses.tickets.ReceptionTicketResponse;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class TicketEndpointsTest extends BaseTest {
    @Test
    public void testCreateTicketByDoctorId() {
        String cookieDefaultAdmin = loginDefaultAdmin();
        PatientRegisterRequest dto = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        String cookiePatient = registerPatient(dto).getLeft();

        LocalDate startDate = LocalDate.of(2020,2,1);
        LocalDate endDate = LocalDate.of(2020,3,15);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon","Wed","Fri");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,20);
        DoctorRegisterResponse dtoResponseCreateDoctor = registerDoctor(cookieDefaultAdmin,dtoCreateDoctor);
        CreateTicketRequest ticketDto = new CreateTicketRequest(dtoResponseCreateDoctor.getId(),
                LocalDate.of(2020,2,7).toString(),
                        LocalTime.of(9,0).toString());
        ReceptionTicketResponse ticketResponse = createReceptions(cookiePatient,ticketDto);
        assertEquals(ticketDto.getDate(),ticketResponse.getDate());
        assertEquals(ticketDto.getTime(),ticketResponse.getTime());
        assertEquals(dtoCreateDoctor.getSpeciality(),ticketResponse.getSpeciality());
        assertEquals(dtoCreateDoctor.getRoom(),ticketResponse.getRoom());
    }

    @Test
    public void testCreateTicketBySpeciality() {
        String cookieDefaultAdmin = loginDefaultAdmin();
        PatientRegisterRequest dto = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        String cookiePatient = registerPatient(dto).getLeft();

        LocalDate startDate = LocalDate.of(2020,2,1);
        LocalDate endDate = LocalDate.of(2020,3,15);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon","Wed","Fri");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor1 = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,20);
        registerDoctor(cookieDefaultAdmin,dtoCreateDoctor1);
        DoctorRegisterRequest dtoCreateDoctor2 = new DoctorRegisterRequest("Иван","Иванов","Петрович","neurologist","113B"
                ,"neurologist_ivanov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,20);
        registerDoctor(cookieDefaultAdmin,dtoCreateDoctor2);

        CreateTicketRequest ticketDto = new CreateTicketRequest("neurologist",
                LocalDate.of(2020,2,7).toString(),
                LocalTime.of(9,0).toString());
        ReceptionTicketResponse ticketResponse = createReceptions(cookiePatient,ticketDto);
        assertEquals(ticketDto.getDate(),ticketResponse.getDate());
        assertEquals(ticketDto.getTime(),ticketResponse.getTime());
        assertEquals(dtoCreateDoctor2.getSpeciality(),ticketResponse.getSpeciality());
        assertEquals(dtoCreateDoctor2.getRoom(),ticketResponse.getRoom());
    }


    @Test
    public void testDeleteTicket() {
        String cookieDefaultAdmin = loginDefaultAdmin();
        PatientRegisterRequest dto = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        String cookiePatient = registerPatient(dto).getLeft();

        LocalDate startDate = LocalDate.of(2020,2,1);
        LocalDate endDate = LocalDate.of(2020,3,15);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon","Wed","Fri");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,20);
        DoctorRegisterResponse dtoResponseCreateDoctor = registerDoctor(cookieDefaultAdmin,dtoCreateDoctor);

        CreateTicketRequest ticketDto = new CreateTicketRequest(dtoResponseCreateDoctor.getId(),
                LocalDate.of(2020,2,7).toString(),
                LocalTime.of(9,0).toString());

        ReceptionTicketResponse ticketResponse = createReceptions(cookiePatient,ticketDto);
        String responseDeleteReception = deleteReception(cookiePatient,ticketResponse.getTicket());
        assertEquals("{}",responseDeleteReception);

    }

    @Test
    public void testGetAllTickets() {
        String cookieDefaultAdmin = loginDefaultAdmin();
        PatientRegisterRequest dto = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        String cookiePatient = registerPatient(dto).getLeft();

        LocalDate startDate = LocalDate.of(2020,2,1);
        LocalDate endDate = LocalDate.of(2020,3,15);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon","Wed","Fri");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,20);
        DoctorRegisterResponse dtoResponseCreateDoctor = registerDoctor(cookieDefaultAdmin,dtoCreateDoctor);

        CreateTicketRequest ticketDto = new CreateTicketRequest(dtoResponseCreateDoctor.getId(),
                LocalDate.of(2020,2,7).toString(),
                LocalTime.of(9,0).toString());
        ReceptionTicketResponse ticketResponse = createReceptions(cookiePatient,ticketDto);

        List<AllTicketsResponse> responses = getPatientsTickets(cookiePatient);
        assertEquals(ticketResponse.getTicket(),responses.get(0).getTicket());
        assertEquals(ticketResponse.getRoom(),responses.get(0).getRoom());
        assertEquals(ticketResponse.getDate(),responses.get(0).getDate());
        assertEquals(ticketResponse.getTime(),responses.get(0).getTime());
        assertEquals(ticketResponse.getDoctorId(),responses.get(0).getDoctorId());

    }

    @Test
    public void testGetAllTicketsCommissions() {
        String defaultAdminCookie = loginDefaultAdmin();

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        ImmutablePair<String,PatientRegisterUpdateResponse> patientResponse = registerPatient(dtoPatient);
        String cookiePatient = patientResponse.getLeft();
        PatientRegisterUpdateResponse patientDtoResponse = patientResponse.getRight();

        LocalDate startDate = LocalDate.of(2020,2,2);
        LocalDate endDate = LocalDate.of(2020,2,4);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor1 = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor1 = registerDoctor(defaultAdminCookie,dtoCreateDoctor1);
        DoctorRegisterRequest dtoCreateDoctor2 = new DoctorRegisterRequest("Иван","Иванов","Петрович","neurologist","113B"
                ,"neurologist_ivanov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,30);
        DoctorRegisterResponse dtoResponseCreateDoctor2 = registerDoctor(defaultAdminCookie,dtoCreateDoctor2);

        String doctorCookie = login(new LoginDtoRequest(dtoCreateDoctor1.getLogin(),dtoCreateDoctor1.getPassword())).getLeft();

        List<Integer> doctorIds = new ArrayList<>();
        Collections.addAll(doctorIds,dtoResponseCreateDoctor1.getId(),dtoResponseCreateDoctor2.getId());
        CommissionRequest commissionDtoRequest =new CommissionRequest(patientDtoResponse.getId(),doctorIds,
                dtoCreateDoctor1.getRoom(),LocalDate.of(2020,2,3).toString(),
                LocalTime.of(10,0).toString(),90);
        CommissionResponse commissionDtoResponse =  createCommission(doctorCookie,commissionDtoRequest);
        assertNotNull(commissionDtoResponse.getTicket());

        List<AllTicketsResponse> responses = getPatientsTickets(cookiePatient);

       assertEquals(commissionDtoResponse.getTicket(),responses.get(0).getTicket());
       assertEquals(commissionDtoResponse.getRoom(), responses.get(0).getRoom());
       assertEquals(commissionDtoResponse.getDate(),responses.get(0).getDate());
       assertEquals(commissionDtoResponse.getTime(),responses.get(0).getTime());
    }

    @Test
    //Запись к одному и тому же врачу в один и тот же день
    public void testCreateTicketByDoctorIdFail() {
        String cookieDefaultAdmin = loginDefaultAdmin();
        PatientRegisterRequest dto = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        String cookiePatient = registerPatient(dto).getLeft();

        LocalDate startDate = LocalDate.of(2020,2,1);
        LocalDate endDate = LocalDate.of(2020,3,15);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon","Wed","Fri");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dtoCreateDoctor = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,20);
        DoctorRegisterResponse dtoResponseCreateDoctor = registerDoctor(cookieDefaultAdmin,dtoCreateDoctor);
        CreateTicketRequest ticketDto = new CreateTicketRequest(dtoResponseCreateDoctor.getId(),
                LocalDate.of(2020,2,7).toString(),
                LocalTime.of(9,0).toString());
        ReceptionTicketResponse ticketResponse = createReceptions(cookiePatient,ticketDto);
        assertEquals(ticketDto.getDate(),ticketResponse.getDate());
        assertEquals(ticketDto.getTime(),ticketResponse.getTime());
        assertEquals(dtoCreateDoctor.getSpeciality(),ticketResponse.getSpeciality());
        assertEquals(dtoCreateDoctor.getRoom(),ticketResponse.getRoom());

        CreateTicketRequest ticketDto2 = new CreateTicketRequest(dtoResponseCreateDoctor.getId(),
                LocalDate.of(2020,2,7).toString(),
                LocalTime.of(11,0).toString());
        try{
            ReceptionTicketResponse ticketResponse2 = createReceptions(cookiePatient,ticketDto2);
        }catch (HttpClientErrorException e){
            assertEquals(HttpStatus.BAD_REQUEST,e.getStatusCode());
        }
    }
}
