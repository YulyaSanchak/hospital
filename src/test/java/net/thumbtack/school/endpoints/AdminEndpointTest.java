package net.thumbtack.school.endpoints;


import net.thumbtack.school.dto.requests.StringConstants;
import net.thumbtack.school.dto.requests.register_update_admin.AdminRegisterRequest;
import net.thumbtack.school.dto.requests.register_update_admin.AdminUpdateRequest;
import net.thumbtack.school.dto.requests.LoginDtoRequest;
import net.thumbtack.school.dto.responses.AdminRegisterResponse;
import net.thumbtack.school.dto.responses.LoginDtoResponse;
import net.thumbtack.school.dto.responses.failure.JsonErrorResponse;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;


import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class AdminEndpointTest extends BaseTest {
    @Test
    public void testCreateAdmin() {
        String cookieDefaultAdmin = loginDefaultAdmin();
        AdminRegisterRequest dto = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                 "position","login", "password");
        AdminRegisterResponse dtoRegisterAdminResponse = registerAdmin(cookieDefaultAdmin,dto);
        assertEquals(dto.getFirstName(), dtoRegisterAdminResponse.getFirstName());
        assertEquals(dto.getLastName(), dtoRegisterAdminResponse.getLastName());
        assertEquals(dto.getPatronymic(),dtoRegisterAdminResponse.getPatronymic());
        assertEquals(dto.getPosition(), dtoRegisterAdminResponse.getPosition());

        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dto.getLogin(), dto.getPassword());
        ImmutablePair<String,LoginDtoResponse> loginAdminResponse = login(loginAdminDtoRequest);
        String adminCookie = loginAdminResponse.getLeft();
        LoginDtoResponse loginDtoResponse = loginAdminResponse.getRight();
        assertEquals(dto.getFirstName(), loginDtoResponse.getFirstName());
        assertEquals(dto.getLastName(), loginDtoResponse.getLastName());
        assertEquals(dto.getPatronymic(), loginDtoResponse.getPatronymic());
        assertEquals(dto.getPosition(), loginDtoResponse.getPosition());

        String logoutResponse = logout(adminCookie);
        assertEquals("{}",logoutResponse);
    }


    @Test
    public void testUpdateAdmin() {
        String cookieDefaultAdmin = loginDefaultAdmin();
        AdminRegisterRequest dto = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        AdminRegisterResponse dtoRegisterAdminResponse = registerAdmin(cookieDefaultAdmin,dto);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dto.getLogin(), dto.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        AdminUpdateRequest dtoUpdate = new AdminUpdateRequest( dtoRegisterAdminResponse.getFirstName(),
                dtoRegisterAdminResponse.getLastName(), dtoRegisterAdminResponse.getPatronymic(),"position_admin",
                dto.getPassword(),"passWORD");
        AdminRegisterResponse dtoResponseUpdate = updateAdmin(adminCookie,dtoUpdate);

        assertEquals(dtoRegisterAdminResponse.getFirstName(),dtoResponseUpdate.getFirstName());
        assertEquals(dtoRegisterAdminResponse.getLastName(),dtoResponseUpdate.getLastName());
        assertEquals(dtoRegisterAdminResponse.getPatronymic(),dtoResponseUpdate.getPatronymic());
        assertEquals(dtoUpdate.getPosition(),dtoResponseUpdate.getPosition());
    }

    @Test
    public void testCreateAdminFail1() {
        String cookieDefaultAdmin = loginDefaultAdmin();
        AdminRegisterRequest dto = new AdminRegisterRequest(null,null , "Иванович",
                null,null,null);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Cookie", cookieDefaultAdmin);
        try {
            HttpEntity<String> response = template.exchange(urlPrefix + "/api/admin", HttpMethod.POST, new HttpEntity<>(dto, headers), String.class);
        }catch (HttpClientErrorException e){
            JsonErrorResponse error = json.fromJson(e.getResponseBodyAsString(), JsonErrorResponse.class);
            allExpectedErrorsContains(error, StringConstants.NULL_FIRSTNAME,
                    StringConstants.NULL_LASTNAME, StringConstants.NULL_POSITION,
                    StringConstants.NULL_LOGIN, StringConstants.NULL_PASSWORD);
        }
    }

    @Test
    public void testCreateAdminFail2() {
        String cookieDefaultAdmin = loginDefaultAdmin();
        AdminRegisterRequest dto = new AdminRegisterRequest("Иван1","Ivanov" , "Иванович",
                "pos","login","12");

        HttpHeaders headers = new HttpHeaders();
        headers.add("Cookie", cookieDefaultAdmin);
        try {
            HttpEntity<String> response = template.exchange(urlPrefix + "/api/admin", HttpMethod.POST, new HttpEntity<>(dto, headers), String.class);
        }catch (HttpClientErrorException e){
            JsonErrorResponse error = json.fromJson(e.getResponseBodyAsString(), JsonErrorResponse.class);

            allExpectedErrorsContains(error, StringConstants.INVALID_FIRSTNAME,
                    StringConstants.INVALID_LASTNAME, StringConstants.INVALID_PASSWORD);
        }
    }

    @Test
    public void testCreateAdminFail3() {
        String cookieDefaultAdmin = loginDefaultAdmin();
        AdminRegisterRequest dto = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        AdminRegisterResponse dtoRegisterAdminResponse = registerAdmin(cookieDefaultAdmin,dto);
        assertEquals(dto.getFirstName(), dtoRegisterAdminResponse.getFirstName());
        assertEquals(dto.getLastName(), dtoRegisterAdminResponse.getLastName());
        assertEquals(dto.getPatronymic(),dtoRegisterAdminResponse.getPatronymic());
        assertEquals(dto.getPosition(), dtoRegisterAdminResponse.getPosition());

        AdminRegisterRequest dto2 = new AdminRegisterRequest("Петр","Петров" , "Иванович",
                "admin","login", "pass15663");
        try{
            AdminRegisterResponse dtoRegisterAdminResponse2 = registerAdmin(cookieDefaultAdmin,dto2);
        }catch (HttpServerErrorException e){
            assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,e.getStatusCode());
        }

    }
}
