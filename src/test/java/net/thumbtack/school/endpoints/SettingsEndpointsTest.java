package net.thumbtack.school.endpoints;

import net.thumbtack.school.dto.requests.register_update_admin.AdminRegisterRequest;
import net.thumbtack.school.dto.requests.LoginDtoRequest;
import net.thumbtack.school.dto.requests.register_update_patient.PatientRegisterRequest;
import net.thumbtack.school.dto.requests.register_update_doctor.DoctorRegisterRequest;
import net.thumbtack.school.dto.requests.register_update_doctor.WeekSchedule;
import net.thumbtack.school.dto.responses.*;
import net.thumbtack.school.dto.responses.settings.GetSettingsResponse;
import net.thumbtack.school.utils.ConfigUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class SettingsEndpointsTest extends BaseTest {
    @Test
    public void testAdminGetSettings() {
        String cookieDefaultAdmin = loginDefaultAdmin();
        AdminRegisterRequest dto = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        AdminRegisterResponse dtoRegisterAdminResponse = registerAdmin(cookieDefaultAdmin,dto);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dto.getLogin(), dto.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        GetSettingsResponse dtoSettingsResponse = getSettings(adminCookie);
        assertEquals(ConfigUtils.getMaxNameLength(),dtoSettingsResponse.getMaxNameLength());
        assertEquals(ConfigUtils.getMinPasswordLength(), dtoSettingsResponse.getMinPasswordLength());

    }

    @Test
    public void testDoctorGetSettings() {
        String defaultAdminCookie = loginDefaultAdmin();

        LocalDate startDate = LocalDate.of(2020,2,1);
        LocalDate endDate = LocalDate.of(2020,3,15);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon","Wed","Fri");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);

        DoctorRegisterRequest dto = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,20);
        registerDoctor(defaultAdminCookie,dto);
        String doctorCookie = login(new LoginDtoRequest(dto.getLogin(),dto.getPassword())).getLeft();

        GetSettingsResponse dtoGetSettingsResponse = getSettings(doctorCookie);
        assertEquals(ConfigUtils.getMaxNameLength(),dtoGetSettingsResponse.getMaxNameLength());
        assertEquals(ConfigUtils.getMinPasswordLength(), dtoGetSettingsResponse.getMinPasswordLength());

    }

    @Test
    public void testPatientGetSettings() {
        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        ImmutablePair<String,PatientRegisterUpdateResponse> patientResponse = registerPatient(dtoPatient);
        String cookiePatient = patientResponse.getLeft();
        GetSettingsResponse dtoGetSettingsResponse = getSettings(cookiePatient);
        assertEquals(ConfigUtils.getMaxNameLength(),dtoGetSettingsResponse.getMaxNameLength());
        assertEquals(ConfigUtils.getMinPasswordLength(), dtoGetSettingsResponse.getMinPasswordLength());

    }

    @Test
    public void testGetSettingsWithoutCookie() {
        GetSettingsResponse dtoGetSettingsResponse = getSettings(null);
        assertEquals(ConfigUtils.getMaxNameLength(),dtoGetSettingsResponse.getMaxNameLength());
        assertEquals(ConfigUtils.getMinPasswordLength(), dtoGetSettingsResponse.getMinPasswordLength());

    }

}
