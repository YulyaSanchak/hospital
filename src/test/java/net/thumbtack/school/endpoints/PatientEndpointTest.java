package net.thumbtack.school.endpoints;

import net.thumbtack.school.dto.requests.register_update_admin.AdminRegisterRequest;
import net.thumbtack.school.dto.requests.LoginDtoRequest;
import net.thumbtack.school.dto.requests.register_update_patient.PatientRegisterRequest;
import net.thumbtack.school.dto.requests.register_update_patient.PatientUpdateRequest;
import net.thumbtack.school.dto.requests.register_update_doctor.DoctorRegisterRequest;
import net.thumbtack.school.dto.requests.register_update_doctor.WeekSchedule;
import net.thumbtack.school.dto.responses.DoctorRegisterResponse;
import net.thumbtack.school.dto.responses.LoginDtoResponse;
import net.thumbtack.school.dto.responses.PatientRegisterUpdateResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class PatientEndpointTest extends BaseTest {

    @Test
    public void testCreatePatient() {
        PatientRegisterRequest dto = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        ImmutablePair<String,PatientRegisterUpdateResponse> patientResponse = registerPatient(dto);
        String cookiePatient = patientResponse.getLeft();
        PatientRegisterUpdateResponse dtoPatientResponse = patientResponse.getRight();
        assertEquals(dto.getFirstName(), dtoPatientResponse.getFirstName());
        assertEquals(dto.getLastName(), dtoPatientResponse.getLastName());
        assertEquals(dto.getPatronymic(),dtoPatientResponse.getPatronymic());
        assertEquals(dto.getEmail(), dtoPatientResponse.getEmail());
        assertEquals(dto.getAddress(), dtoPatientResponse.getAddress());
        assertEquals(dto.getPhone(), dtoPatientResponse.getPhone());

        ImmutablePair<String,LoginDtoResponse> accountResponse = getAccountInfo(cookiePatient);
        String cookieAccountInfo = accountResponse.getLeft();
        LoginDtoResponse accountDtoResponse = accountResponse.getRight();
        assertEquals(cookiePatient,cookieAccountInfo);
        assertEquals(dto.getFirstName(), accountDtoResponse.getFirstName());
        assertEquals(dto.getLastName(), accountDtoResponse.getLastName());
        assertEquals(dto.getPatronymic(), accountDtoResponse.getPatronymic());
        assertEquals(dto.getEmail(), accountDtoResponse.getEmail());
        assertEquals(dto.getAddress(), accountDtoResponse.getAddress());
        assertEquals(dto.getPhone(),accountDtoResponse.getPhone());

        String logoutResponse = logout(cookiePatient);
        assertEquals("{}",logoutResponse);

    }

    @Test
    public void testAdminGetPatient() {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        registerAdmin(defaultAdminCookie,dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        PatientRegisterUpdateResponse patientResponse = registerPatient(dtoPatient).getRight();

        PatientRegisterUpdateResponse patientGetInfoResponse = getPatientInfoById(adminCookie,patientResponse.getId());

        assertEquals(dtoPatient.getFirstName(), patientGetInfoResponse.getFirstName());
        assertEquals(dtoPatient.getLastName(), patientGetInfoResponse.getLastName());
        assertEquals(dtoPatient.getPatronymic(), patientGetInfoResponse.getPatronymic());
        assertEquals(dtoPatient.getEmail(), patientGetInfoResponse.getEmail());
        assertEquals(dtoPatient.getAddress(), patientGetInfoResponse.getAddress());
        assertEquals(dtoPatient.getPhone(), patientGetInfoResponse.getPhone());
    }

    @Test
    public void testDoctorGetPatient() {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        registerAdmin(defaultAdminCookie,dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        String adminCookie = login(loginAdminDtoRequest).getLeft();

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        PatientRegisterUpdateResponse patientDtoResponse = registerPatient(dtoPatient).getRight();

        LocalDate startDate = LocalDate.of(2020,2,1);
        LocalDate endDate = LocalDate.of(2020,3,15);
        LocalTime startTime = LocalTime.of(8,0);
        LocalTime endTime = LocalTime.of(14,0);
        List<String> weekdays = new ArrayList<>();
        Collections.addAll(weekdays,"Mon","Wed","Fri");
        WeekSchedule weekSchedule = new WeekSchedule(startTime.toString(),endTime.toString(),weekdays);
        DoctorRegisterRequest dtoDoctor = new DoctorRegisterRequest("Петр","Петров","Петрович","therapist","112A"
                ,"therapist_petrov","122A001", startDate.toString(), endDate.toString(),null , weekSchedule,20);
        DoctorRegisterResponse dtoResponse = registerDoctor(adminCookie,dtoDoctor);
        LoginDtoRequest loginDtoRequest = new LoginDtoRequest(dtoDoctor.getLogin(), dtoDoctor.getPassword());
        String cookieDoctor = login(loginDtoRequest).getLeft();

        PatientRegisterUpdateResponse patientGetInfoResponse = getPatientInfoById(cookieDoctor,patientDtoResponse.getId());

        assertEquals(dtoPatient.getFirstName(), patientGetInfoResponse.getFirstName());
        assertEquals(dtoPatient.getLastName(), patientGetInfoResponse.getLastName());
        assertEquals(dtoPatient.getPatronymic(), patientGetInfoResponse.getPatronymic());
        assertEquals(dtoPatient.getEmail(), patientGetInfoResponse.getEmail());
        assertEquals(dtoPatient.getAddress(), patientGetInfoResponse.getAddress());
        assertEquals(dtoPatient.getPhone(), patientGetInfoResponse.getPhone());
    }

    @Test
    public void testPatientGetPatientFail() {
        String defaultAdminCookie = loginDefaultAdmin();
        AdminRegisterRequest dtoAdmin = new AdminRegisterRequest("Иван","Иванов" , "Иванович",
                "position","login", "password");
        registerAdmin(defaultAdminCookie,dtoAdmin);
        LoginDtoRequest loginAdminDtoRequest = new LoginDtoRequest(dtoAdmin.getLogin(), dtoAdmin.getPassword());
        login(loginAdminDtoRequest).getLeft();

        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        ImmutablePair<String,PatientRegisterUpdateResponse> patientResponse = registerPatient(dtoPatient);
        String cookiePatient = patientResponse.getLeft();
        PatientRegisterUpdateResponse patientDtoResponse = patientResponse.getRight();
        try {
            getPatientInfoById(cookiePatient,patientDtoResponse.getId());
        } catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
        }
    }

    @Test
    public void testUpdatePatient() {
        PatientRegisterRequest dtoPatient = new PatientRegisterRequest("Иван","Иванов", "Иванович"
                ,"ivanov@gmail.com", "г. Омск", "+79999999999","loginИванИванов","password");
        ImmutablePair<String,PatientRegisterUpdateResponse> patientResponse = registerPatient(dtoPatient);
        String cookiePatient = patientResponse.getLeft();
        patientResponse.getRight();

        PatientUpdateRequest dtoUpdate = new PatientUpdateRequest(dtoPatient.getFirstName(),dtoPatient.getLastName(),
                dtoPatient.getPatronymic(),dtoPatient.getEmail(),dtoPatient.getAddress(),dtoPatient.getPhone(),
                dtoPatient.getPassword(),"passWORD");
        PatientRegisterUpdateResponse dtoResponseUpdate = updatePatient(cookiePatient,dtoUpdate);

        assertEquals(dtoUpdate.getFirstName(),dtoResponseUpdate.getFirstName());
        assertEquals(dtoUpdate.getLastName(),dtoResponseUpdate.getLastName());
        assertEquals(dtoUpdate.getPatronymic(),dtoResponseUpdate.getPatronymic());
        assertEquals(dtoUpdate.getEmail(),dtoResponseUpdate.getEmail());
        assertEquals(dtoUpdate.getAddress(),dtoResponseUpdate.getAddress());
        assertEquals(dtoUpdate.getPhone(),dtoResponseUpdate.getPhone());
    }
}
