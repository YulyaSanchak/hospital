package net.thumbtack.school.validators;



import net.thumbtack.school.utils.ConfigUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class EmailValid implements ConstraintValidator<EmailConstraint, String> {
    private static final String EMAIL_PATTERN =
            "^[A-Za-z0-9\\-]+(\\.[A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    @Override
    public boolean isValid(String field, ConstraintValidatorContext constraintValidatorContext) {
        Pattern p = Pattern.compile(EMAIL_PATTERN);

        return  field == null || ((field.length()<= ConfigUtils.getMaxNameLength()*ConfigUtils.getMaxNameLength()) && p.matcher(field).matches());
    }
}
