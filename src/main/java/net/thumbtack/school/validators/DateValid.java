package net.thumbtack.school.validators;



import net.thumbtack.school.utils.ConfigUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class DateValid implements ConstraintValidator<DateConstraint, String> {
    @Override
    public boolean isValid(String field, ConstraintValidatorContext constraintValidatorContext) {
        Pattern p = Pattern.compile("^\\d{4}-\\d{2}-\\d{2}$");

        return  field == null || p.matcher(field).matches();
    }
}
