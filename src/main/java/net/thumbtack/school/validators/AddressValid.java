package net.thumbtack.school.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AddressValid implements ConstraintValidator<AddressConstraint, String> {

    @Override
    public boolean isValid(String field, ConstraintValidatorContext constraintValidatorContext) {
        return  field == null || (!field.isEmpty());
    }
}
