package net.thumbtack.school.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class PhoneValid implements ConstraintValidator<PhoneConstraint, String> {
    private static final String PHONE_PATTERN_V1 =
            "^(\\+7)\\d{10}+$";
    private static final String PHONE_PATTERN_V2 =
            "^8\\d{10}+$";

    @Override
    public boolean isValid(String field, ConstraintValidatorContext constraintValidatorContext) {
        Pattern p1 = Pattern.compile(PHONE_PATTERN_V1);
        Pattern p2 = Pattern.compile(PHONE_PATTERN_V2);
        return  field == null || p1.matcher(field).matches() || p2.matcher(field).matches();
    }
}
