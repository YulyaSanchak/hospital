package net.thumbtack.school.validators;


import net.thumbtack.school.utils.ConfigUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValid implements ConstraintValidator<PasswordConstraint,String> {
    @Override
    public boolean isValid(String field, ConstraintValidatorContext constraintValidatorContext) {
        return field == null || ((field.length() >= ConfigUtils.getMinPasswordLength())&&
                (field.length() <= ConfigUtils.getMaxNameLength()));
    }
}