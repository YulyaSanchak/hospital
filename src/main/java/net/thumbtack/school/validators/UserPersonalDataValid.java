package net.thumbtack.school.validators;



import net.thumbtack.school.utils.ConfigUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class UserPersonalDataValid implements ConstraintValidator<UserPersonalDataValidConstraint, String> {
    @Override
    public boolean isValid(String field, ConstraintValidatorContext constraintValidatorContext) {
        Pattern p = Pattern.compile("^[А-Яа-я\\-]+$");

        return  field == null || ((field.length()<= ConfigUtils.getMaxNameLength()*ConfigUtils.getMaxNameLength()) && p.matcher(field).matches());
    }
}
