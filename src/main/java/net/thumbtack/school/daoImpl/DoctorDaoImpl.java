package net.thumbtack.school.daoImpl;


import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import net.thumbtack.school.dao.DoctorDao;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.exceptions.ErrorCode;
import net.thumbtack.school.exceptions.ErrorItem;
import net.thumbtack.school.model.DaySchedule;
import net.thumbtack.school.model.Speciality;
import net.thumbtack.school.model.UserType;
import net.thumbtack.school.model.users.Doctor;
import net.thumbtack.school.model.users.User;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DoctorDaoImpl extends  DaoImplBase implements DoctorDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(DoctorDaoImpl.class);

    public Doctor insert(Doctor doctor) throws BaseException {
        LOGGER.debug("DAO insert Doctor {} ",doctor);
        try (SqlSession sqlSession = getSession()) {
            try {
                getUserMapper(sqlSession).insert(doctor,UserType.DOCTOR);
                getDoctorMapper(sqlSession).insert(doctor);
                for (DaySchedule day : doctor.getDaySchedules()) {
                    getDayScheduleMapper(sqlSession).insert(day);
                    for(int i = 0; i < day.getReceptions().size(); i++){
                        day.getReceptions().get(i).getDaySchedule().setId(day.getId());
                    }
                    getReceptionMapper(sqlSession).batchInsert(day.getReceptions());
                }
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert Doctor {}, {}", doctor, ex);
                sqlSession.rollback();
                throw ex;
            } catch (MySQLIntegrityConstraintViolationException ex){
                throw new BaseException(new ErrorItem(ErrorCode.LOGIN_IS_EXIST,doctor.getLogin()));
            }
            sqlSession.commit();
        }
        return doctor;
    }

    public Doctor getById(int id) throws BaseException {
        LOGGER.debug("DAO get Doctor by Id {}", id);
        try (SqlSession sqlSession = getSession()) {
            return getDoctorMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Doctor by Id {} {}", id, ex);
            throw new BaseException(new ErrorItem(ErrorCode.USER_IS_NOT_FOUND,String.valueOf(id)));
        }
    }

    public Doctor getByUserId(int userId) throws BaseException {
        LOGGER.debug("DAO get Doctor by userId {}", userId);
        try (SqlSession sqlSession = getSession()) {
            return getDoctorMapper(sqlSession).getByUserId(userId);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Doctor by userId {} {}", userId, ex);
            throw new BaseException(new ErrorItem(ErrorCode.USER_IS_NOT_FOUND,String.valueOf(userId)));
        }
    }

    public Doctor getInfo(int id, LocalDate startDate, LocalDate endDate) throws BaseException {
        LOGGER.debug("DAO get Doctor info by id {}", id);
        try (SqlSession sqlSession = getSession()) {
            Map<String, Object> map = new HashMap<>();
            map.put("doctorId",id);
            map.put("specialityId",null);
            map.put("startDate", startDate);
            map.put("endDate", endDate);
            return sqlSession.selectOne("DoctorMapper.getDoctorsInfo",map);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Doctor info by id {} {}", id, ex);
            throw new BaseException(new ErrorItem(ErrorCode.USER_IS_NOT_FOUND,String.valueOf(id)));
        }
    }

    public List<Doctor> getDoctorsInfo(Speciality speciality, LocalDate startDate, LocalDate endDate) throws BaseException {
        LOGGER.debug("DAO get Doctor info by Speciality {}", speciality);
        try (SqlSession sqlSession = getSession()) {
            Map<String, Object> map = new HashMap<>();
            if (speciality==null){
                map.put("specialityId",null);
            }else{
                map.put("specialityId",speciality.getId());
            }
            map.put("doctorId", null);
            map.put("startDate", startDate);
            map.put("endDate", endDate);
            return sqlSession.selectList("DoctorMapper.getDoctorsInfo",map);

        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Doctor info by Speciality {} {}", speciality, ex);
            throw new BaseException(new ErrorItem(ErrorCode.DOCTORS_NOT_FOUND,speciality.getName()));
        }
    }

    public void delete(Doctor doctor) {
        LOGGER.debug("DAO delete Doctor {} ", doctor);
        try (SqlSession sqlSession = getSession()) {
            try {
                getDoctorMapper(sqlSession).delete(doctor);
                getUserMapper(sqlSession).delete(new User(doctor.getUserId(),doctor.getFirstName()
                        ,doctor.getLastName(),doctor.getPatronymic(),doctor.getLogin(),doctor.getPassword(),UserType.DOCTOR));
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete Doctor {},{} ", doctor, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    public void deleteAll() {
        LOGGER.debug("DAO delete all Doctors ");
        try (SqlSession sqlSession = getSession()) {
            try {
                getDoctorMapper(sqlSession).deleteAll();
                getUserMapper(sqlSession).deleteAllByType(UserType.DOCTOR);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete all Doctors {} ", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    public Doctor update(Doctor doctor) throws BaseException {
        LOGGER.debug("DAO update Doctor {}", doctor);
        try (SqlSession sqlSession = getSession()) {
            try {
                getUserMapper(sqlSession).update(new User(doctor.getUserId(),doctor.getFirstName()
                        ,doctor.getLastName(),doctor.getPatronymic(),doctor.getLogin(),doctor.getPassword()
                        ,UserType.DOCTOR));
                getDoctorMapper(sqlSession).update(doctor);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't update Doctor{} {} ", doctor, ex);
                sqlSession.rollback();
                throw new BaseException(new ErrorItem(ErrorCode.USER_IS_NOT_FOUND,String.valueOf(doctor.getUserId())));
            }
            sqlSession.commit();
        }
        return doctor;
    }

    public List<Doctor> getBySpeciality(Speciality speciality) {
        LOGGER.debug("DAO get doctors by speciality {}", speciality);
        try (SqlSession sqlSession = getSession()) {
            return getDoctorMapper(sqlSession).getBySpeciality(speciality);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get doctors by speciality {} {} ", speciality, ex);
            throw ex;
        }
    }

    public List<Doctor> getAll() {
        LOGGER.debug("DAO get all doctors ");
        try (SqlSession sqlSession = getSession()) {
            return getDoctorMapper(sqlSession).getAll();
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get all doctors {} ", ex);
            throw ex;
        }
    }

    private Doctor insertSchedule(Doctor doctor) throws BaseException {
        LOGGER.debug("DAO insert Doctor's schedule {} ",doctor.getDaySchedules());
        try (SqlSession sqlSession = getSession()) {
            try {
                for (DaySchedule day : doctor.getDaySchedules()) {
                    getDayScheduleMapper(sqlSession).insert(day);
                    for(int i = 0; i < day.getReceptions().size(); i++){
                        day.getReceptions().get(i).getDaySchedule().setId(day.getId());
                    }
                    getReceptionMapper(sqlSession).batchInsert(day.getReceptions());
                }
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert Doctor's schedule {}, {}", doctor.getDaySchedules(), ex);
                sqlSession.rollback();
                throw new BaseException(new ErrorItem(ErrorCode.LOGIN_IS_EXIST,doctor.getLogin()));
            }
            sqlSession.commit();
        }
        return doctor;
    }

    public Doctor update(Doctor doctor, LocalDate startDate ,LocalDate endDate, boolean isCreateSchedule) throws BaseException {
        LOGGER.debug("DAO update Doctor's Schedule {}", doctor);

        try (SqlSession sqlSession = getSession()) {
            try {
                if(isCreateSchedule){
                    doctor.setDaySchedules(insertSchedule(doctor).getDaySchedules());
                }else{
                    deleteReception(doctor.getId(),startDate,endDate);
                    deleteSchedule(doctor.getId(),startDate,endDate);
                    doctor.setDaySchedules(insertSchedule(doctor).getDaySchedules());
                }
            } catch (RuntimeException ex) {
                LOGGER.info("Can't update Doctor's Schedule {} {} ", doctor, ex);
                sqlSession.rollback();
                throw new BaseException(new ErrorItem(ErrorCode.USER_IS_NOT_FOUND,String.valueOf(doctor.getUserId())));
            }
            sqlSession.commit();
        }
        return doctor;
    }

    private void deleteSchedule(int doctorId,LocalDate startDate, LocalDate endDate) {
        LOGGER.debug("DAO delete Schedule");
        try (SqlSession sqlSession = getSession()) {
            try {
                getDayScheduleMapper(sqlSession).deleteByDoctorAndDate(doctorId, startDate, endDate);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete ScheduleRequest {} ", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    private void deleteReception(int doctorId,LocalDate startDate, LocalDate endDate) {
        LOGGER.debug("DAO delete Receptions");
        try (SqlSession sqlSession = getSession()) {
            try {
                List<Integer> ids = getReceptionMapper(sqlSession).getIdByDoctorAndDate(doctorId,startDate,endDate);
                getReceptionMapper(sqlSession).deleteByIds(ids);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete Receptions{} ", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    public Doctor getDoctorForTicket(int id, LocalDate date, LocalTime time) throws BaseException {
        LOGGER.debug("DAO get Doctor for ticket {}", id);
        try (SqlSession sqlSession = getSession()) {
            Map<String, Object> map = new HashMap<>();
            map.put("doctorId",id);
            map.put("specialityId", null);
            map.put("date", date);
            map.put("time", time);
            return sqlSession.selectOne("DoctorMapper.getDoctorForReceptions",map);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Doctor  for ticket {} {}", id, ex);
            throw new BaseException(new ErrorItem(ErrorCode.USER_IS_NOT_FOUND,String.valueOf(id)));
        }
    }

    public List<Doctor> getDoctorsForTicket(Speciality speciality, LocalDate date, LocalTime time) throws BaseException {
        LOGGER.debug("DAO get Doctors by Speciality for ticket Reception {}", speciality);
        try (SqlSession sqlSession = getSession()) {
            Map<String, Object> map = new HashMap<>();
            map.put("doctorId",null);
            map.put("specialityId",speciality.getId());
            map.put("date", date);
            map.put("time", time);
            return sqlSession.selectList("DoctorMapper.getDoctorForReceptions",map);

        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Doctor info by Speciality for ticket Reception {} {}", speciality, ex);
            throw new BaseException(new ErrorItem(ErrorCode.DOCTORS_NOT_FOUND,speciality.getName()));
        }
    }

    public List<Doctor> getDoctorsForCommission(List<Integer> ids, LocalDate date) throws BaseException {
        LOGGER.debug("DAO get Doctors for ticket Commission {}", ids);
        try (SqlSession sqlSession = getSession()) {
            Map<String, Object> map = new HashMap<>();
            map.put("list",ids);
            map.put("date", date);
            return sqlSession.selectList("DoctorMapper.getDoctorForCommission",map);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Doctors  for ticket Commission {} {}", ids, ex);
            throw new BaseException(new ErrorItem(ErrorCode.DOCTORS_NOT_FOUND));
        }
    }

    public List<Doctor> getDoctorsForDeleteCommission(LocalDate date, Integer ticketId) throws BaseException {
        LOGGER.debug("DAO get Doctor for delete ticket Commission");
        try (SqlSession sqlSession = getSession()) {
            Map<String, Object> map = new HashMap<>();
            map.put("date", date);
            map.put("ticketId", ticketId);
            return sqlSession.selectList("DoctorMapper.getCommissionReceptions",map);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Doctor for delete ticket Commission  {}", ex);
            throw new BaseException(new ErrorItem(ErrorCode.DOCTORS_NOT_FOUND));
        }
    }


}
