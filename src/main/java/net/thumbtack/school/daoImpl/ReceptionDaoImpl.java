package net.thumbtack.school.daoImpl;

import net.thumbtack.school.containers.DurationReception;
import net.thumbtack.school.dao.ReceptionDao;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.exceptions.ErrorCode;
import net.thumbtack.school.exceptions.ErrorItem;
import net.thumbtack.school.model.Reception;
import net.thumbtack.school.model.Status;
import net.thumbtack.school.model.Ticket;
import net.thumbtack.school.model.users.Doctor;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReceptionDaoImpl extends DaoImplBase implements ReceptionDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReceptionDaoImpl.class);

    public Reception insert(Reception reception) {
        LOGGER.debug("DAO insert Reception {} ", reception);
        try (SqlSession sqlSession = getSession()) {
            try {
                getReceptionMapper(sqlSession).insert(reception);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert Reception {}, {}", reception, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return reception;
    }

    public void batchInsert(List<Reception> receptions) {
        LOGGER.debug("DAO batch insert Receptions {} ", receptions);
        try (SqlSession sqlSession = getSession()) {
            try {
                getReceptionMapper(sqlSession).batchInsert(receptions);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't batch insert Receptions {}, {} ", receptions, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    public Reception getById(int id) {
        LOGGER.debug("DAO get Reception by Id {}", id);
        try (SqlSession sqlSession = getSession()) {
            return getReceptionMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Reception by Id {} {}", id, ex);
            throw ex;
        }
    }

    public Reception getByTicket(Ticket ticket) {
        LOGGER.debug("DAO get Reception by ticket {}", ticket);
        try (SqlSession sqlSession = getSession()) {
            return getReceptionMapper(sqlSession).getByTicket(ticket.getId());
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Reception by ticket {} {}", ticket, ex);
            throw ex;
        }
    }

    public List<Reception> getByDoctor(Doctor doctor) {
        LOGGER.debug("DAO get Reception by Doctor {}", doctor);
        try (SqlSession sqlSession = getSession()) {
            return getReceptionMapper(sqlSession).getByDoctor(doctor);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Reception by Doctor {} {}", doctor, ex);
            throw ex;
        }
    }

    public void delete(Reception reception) {
        LOGGER.debug("DAO delete Reception {} ", reception);
        try (SqlSession sqlSession = getSession()) {
            try {
                getReceptionMapper(sqlSession).delete(reception);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete Reception {},{} ", reception, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    public void deleteAll() {
        LOGGER.debug("DAO delete all Receptions");
        try (SqlSession sqlSession = getSession()) {
            try {
                getReceptionMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete all Receptions");
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    public void deleteByIds(List<Integer> ids) {
        LOGGER.debug("DAO delete Receptions with ids {}",ids);
        try (SqlSession sqlSession = getSession()) {
            try {
                getReceptionMapper(sqlSession).deleteByIds(ids);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete Receptions with ids {}", ids);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    public void update(int receptionId, Integer ticketId, Status status){
        LOGGER.debug("DAO update Reception with id {}", receptionId);
        try (SqlSession sqlSession = getSession()) {
            try {
                getReceptionMapper(sqlSession).update(receptionId,ticketId, status);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't update Reception with id{} {} ", receptionId, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    public List<Reception> updateReceptions(List<Reception> receptions, Integer ticketId, Status status){
        LOGGER.debug("DAO update Receptions {}", receptions);
        try (SqlSession sqlSession = getSession()) {
            try {
                for (Reception reception: receptions){
                    getReceptionMapper(sqlSession).update(reception.getId(),ticketId, status);
                    if(ticketId == null){
                        reception.setTicket(null);
                    }
                }
            } catch (RuntimeException ex) {
                LOGGER.info("Can't update Receptions{} {} ", receptions, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return receptions;
    }

    public void updateForCommission(List<Integer> ids) throws BaseException {
        LOGGER.debug("DAO update Receptions with ids for Commission{}", ids);
        try (SqlSession sqlSession = getSession()) {
            try {
                Integer countUpdatedReceptions = getReceptionMapper(sqlSession).updateForCommission(ids);
                if (countUpdatedReceptions.compareTo(ids.size())!=0){
                    sqlSession.rollback();
                    throw new BaseException(new ErrorItem(ErrorCode.RECEPTION_IS_BUSY));
                }
            } catch (RuntimeException ex) {
                LOGGER.info("Can't update Receptions with ids for Commission{} {} ",ids, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    public void updateState(Integer ticketId, Status status){
        LOGGER.debug("DAO update Receptions swith ticketId {}", ticketId);
        try (SqlSession sqlSession = getSession()) {
            try {
                getReceptionMapper(sqlSession).updateState(ticketId, status);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't update Receptions  with ticketId{} {} ", ticketId, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    public void updateForTicketReception(Integer id,Integer ticketId, Status status) throws BaseException {
        LOGGER.debug("DAO update Receptions with id {}", id);
        try (SqlSession sqlSession = getSession()) {
            try {
                Integer update = getReceptionMapper(sqlSession).updateForTicket(id,ticketId, status);
                if(update!=1){
                    sqlSession.rollback();
                    throw new BaseException(new ErrorItem(ErrorCode.IMPOSSIBLE_CREATE_TICKET));
                }
            } catch (RuntimeException ex) {
                LOGGER.info("Can't update Receptions with id{} {} ", ticketId, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    public List<DurationReception> getDurationReceptions(List<Integer> ids, LocalDate date){
        LOGGER.debug("DAO get duration Receptions ");
        try (SqlSession sqlSession = getSession()) {
            Map<String, Object> map = new HashMap<>();
            map.put("list",ids);
            map.put("date", date);
            return sqlSession.selectList("ReceptionMapper.getDurations",map);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't duration Receptions {}", ex);
            throw ex;
        }
    }

    public List<Reception> getReceptions(List<Integer> ticketIds){
        LOGGER.debug("DAO get Receptions by ticketIds ");
        try (SqlSession sqlSession = getSession()) {
            Map<String, Object> map = new HashMap<>();
            map.put("list", ticketIds);
            return sqlSession.selectList("PatientMapper.getReceptions",map);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Receptions by ticketIds {}", ex);
            throw ex;
        }
    }

    public List<Reception> getTicketReceptionsPatientByDate(int patientId, LocalDate date, Integer doctorId){
        LOGGER.debug("DAO get BUSY or TICKET receptions by date ");
        try (SqlSession sqlSession = getSession()) {
            Map<String, Object> map = new HashMap<>();
            map.put("date", date);
            map.put("patientId",patientId);
            map.put("doctorId",doctorId);
            return sqlSession.selectList("ReceptionMapper.getTicketReceptionsPatientByDate",map);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get BUSY or TICKET receptions by date {}", ex);
            throw ex;
        }
    }

    public int getDurationReceptionsByDate(int doctorId, LocalDate date){
        LOGGER.debug("DAO get duration receptions by date ");
        try (SqlSession sqlSession = getSession()) {
            return getReceptionMapper(sqlSession).getDurationReception(doctorId,date);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get durations receptions by date {}", ex);
            throw ex;
        }
    }
}
