package net.thumbtack.school.daoImpl;

import net.thumbtack.school.containers.statistics.DoctorStatistic;
import net.thumbtack.school.containers.statistics.PatientStatistic;
import net.thumbtack.school.containers.statistics.Statistic;
import net.thumbtack.school.dao.StatisticsDao;
import net.thumbtack.school.model.Status;
import net.thumbtack.school.model.TicketType;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StatisticsDaoImpl extends DaoImplBase implements StatisticsDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(StatisticsDaoImpl.class);

    //Суммарная статистика
    public int getAllPatientsQuantity(LocalDate startDate, LocalDate endDate) {
        LOGGER.debug("DAO get all patients quantity ");
        try (SqlSession sqlSession = getSession()) {
            return getStatisticsMapper(sqlSession).getAllPatientsQuantity(startDate, endDate);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get all patients quantity {}", ex);
            throw ex;
        }
    }

    public int getAllReceptionsQuantity(LocalDate startDate, LocalDate endDate) {
        LOGGER.debug("DAO get all receptions quantity ");
        try (SqlSession sqlSession = getSession()) {
            return getStatisticsMapper(sqlSession).getAllReceptionsQuantity(startDate, endDate);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get all receptions quantity {}", ex);
            throw ex;
        }
    }

    public List<Statistic> getAllReceptionsWithStatusQuantity(LocalDate startDate, LocalDate endDate) {
        LOGGER.debug("DAO get all receptions with status quantity ");
        try (SqlSession sqlSession = getSession()) {
            return getStatisticsMapper(sqlSession).getAllReceptionsWithStatusQuantity(startDate, endDate);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get all receptions with status quantity {}", ex);
            throw ex;
        }
    }

    public Integer getAllCommissionsQuantity(LocalDate startDate, LocalDate endDate) {
        LOGGER.debug("DAO get all commissions quantity ");
        try (SqlSession sqlSession = getSession()) {
            return getStatisticsMapper(sqlSession).getAllCommissionsQuantity(startDate, endDate);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get all commissions quantity {}", ex);
            throw ex;
        }
    }

    public Integer  getDoctorsInCommissionsQuantity(LocalDate startDate, LocalDate endDate) {
        LOGGER.debug("DAO get doctors in commissions quantity ");
        try (SqlSession sqlSession = getSession()) {
            return getStatisticsMapper(sqlSession).getDoctorsInCommissionsQuantity(startDate, endDate);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get doctors in commissions quantity  {}", ex);
            throw ex;
        }
    }

    public Integer  getDoctorsInReceptions(LocalDate startDate, LocalDate endDate) {
        LOGGER.debug("DAO get doctors in receptions quantity ");
        try (SqlSession sqlSession = getSession()) {
            return getStatisticsMapper(sqlSession).getDoctorsInReceptions(startDate, endDate);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get doctors in receptions quantity  {}", ex);
            throw ex;
        }
    }

    //Статистика по врачам
    public List<DoctorStatistic> getWorkingDaysQuantity(Integer specialityId, LocalDate startDate, LocalDate endDate)  {
        LOGGER.debug("DAO get working days quantity ");
        try (SqlSession sqlSession = getSession()) {
            Map<String, Object> map = new HashMap<>();
            map.put("specialityId",specialityId);
            map.put("dateStart", startDate);
            map.put("dateEnd", endDate);
            return sqlSession.selectList("StatisticsMapper.getWorkingDaysQuantity",map);

        } catch (RuntimeException ex) {
            LOGGER.info("Can't get working days quantity {}", ex);
            throw ex;
        }
    }

    public List<DoctorStatistic> getReceptionsQuantity(Integer specialityId, LocalDate startDate, LocalDate endDate)  {
        LOGGER.debug("DAO get receptions quantity ");
        try (SqlSession sqlSession = getSession()) {
            Map<String, Object> map = new HashMap<>();
            map.put("specialityId",specialityId);
            map.put("dateStart", startDate);
            map.put("dateEnd", endDate);
            return sqlSession.selectList("StatisticsMapper.getReceptionsQuantity",map);

        } catch (RuntimeException ex) {
            LOGGER.info("Can't get receptions quantity {}", ex);
            throw ex;
        }
    }

    public List<DoctorStatistic> getReceptionsQuantityByStatus(Integer specialityId, LocalDate startDate, LocalDate endDate, Status status)  {
        LOGGER.debug("DAO get receptions quantity ");
        try (SqlSession sqlSession = getSession()) {
            Map<String, Object> map = new HashMap<>();
            map.put("specialityId",specialityId);
            map.put("dateStart", startDate);
            map.put("dateEnd", endDate);
            map.put("status", status);
            return sqlSession.selectList("StatisticsMapper.getReceptionsQuantityByStatus",map);

        } catch (RuntimeException ex) {
            LOGGER.info("Can't get receptions quantity {}", ex);
            throw ex;
        }
    }

    public List<DoctorStatistic> getCommissionsQuantity(Integer specialityId, LocalDate startDate, LocalDate endDate)  {
        LOGGER.debug("DAO get commissions quantity ");
        try (SqlSession sqlSession = getSession()) {
            Map<String, Object> map = new HashMap<>();
            map.put("specialityId",specialityId);
            map.put("dateStart", startDate);
            map.put("dateEnd", endDate);
            return sqlSession.selectList("StatisticsMapper.getCommissionsQuantity",map);

        } catch (RuntimeException ex) {
            LOGGER.info("Can't get commissions quantity {}", ex);
            throw ex;
        }
    }

    public List<DoctorStatistic> getPatientsQuantity(Integer specialityId, LocalDate startDate, LocalDate endDate)  {
        LOGGER.debug("DAO get patients quantity ");
        try (SqlSession sqlSession = getSession()) {
            Map<String, Object> map = new HashMap<>();
            map.put("specialityId",specialityId);
            map.put("dateStart", startDate);
            map.put("dateEnd", endDate);
            return sqlSession.selectList("StatisticsMapper.getPatientsQuantity",map);

        } catch (RuntimeException ex) {
            LOGGER.info("Can't get patients quantity {}", ex);
            throw ex;
        }
    }

    //Статистика по пациентам
    public List<PatientStatistic> getTicketPatientsQuantity(LocalDate startDate, LocalDate endDate)  {
        LOGGER.debug("DAO get ticket patients quantity ");
        try (SqlSession sqlSession = getSession()) {
            Map<String, Object> map = new HashMap<>();
            map.put("dateStart", startDate);
            map.put("dateEnd", endDate);
            return sqlSession.selectList("StatisticsMapper.getTicketPatientsQuantity",map);

        } catch (RuntimeException ex) {
            LOGGER.info("Can't get ticket patients quantity {}", ex);
            throw ex;
        }
    }

    public List<PatientStatistic> getTicketPatientsQuantityByType(LocalDate startDate, LocalDate endDate, TicketType ticketType)  {
        LOGGER.debug("DAO get ticket patients with type quantity ");
        try (SqlSession sqlSession = getSession()) {
            Map<String, Object> map = new HashMap<>();
            map.put("dateStart", startDate);
            map.put("dateEnd", endDate);
            map.put("ticketType", ticketType);
            return sqlSession.selectList("StatisticsMapper.getTicketPatientsQuantityByType",map);

        } catch (RuntimeException ex) {
            LOGGER.info("Can't get ticket patients with type quantity {}", ex);
            throw ex;
        }
    }
}
