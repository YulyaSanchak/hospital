package net.thumbtack.school.daoImpl;

import net.thumbtack.school.dao.SessionDao;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.exceptions.ErrorCode;
import net.thumbtack.school.exceptions.ErrorItem;
import net.thumbtack.school.model.Session;
import net.thumbtack.school.model.users.User;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

public class SessionDaoImpl extends DaoImplBase implements SessionDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(SessionDaoImpl.class);

    public Session insert(User user) throws BaseException {
        LOGGER.debug("DAO insert Session for user {}", user);
        Session session = new Session(user, UUID.randomUUID().toString());
        try (SqlSession sqlSession = getSession()) {
            try {
                getSessionMapper(sqlSession).insert(session, user);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert Session for user {}, {}", user, ex);
                sqlSession.rollback();
                throw new BaseException(new ErrorItem(ErrorCode.USER_IS_NOT_FOUND, String.valueOf(user.getUserId())));
            }
            sqlSession.commit();
        }
        return session;
    }

    public void delete(String cookie) throws BaseException {
        LOGGER.debug("DAO delete Session {} ", cookie);
        try (SqlSession sqlSession = getSession()) {
            try {
                getSessionMapper(sqlSession).delete(cookie);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete Session {} {}", cookie, ex);
                sqlSession.rollback();
                throw new BaseException(new ErrorItem(ErrorCode.COOKIE_IS_NOT_FOUND, cookie));
            }
            sqlSession.commit();
        }
    }

    public void deleteAll(){
        LOGGER.debug("DAO delete all Sessions");
        try (SqlSession sqlSession = getSession()) {
            try {
                getSessionMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete all Sessions {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    public Session getByUserId(int userId) throws BaseException {
        LOGGER.debug("DAO get Session by userId {}", userId);
        try (SqlSession sqlSession = getSession()) {
            return getSessionMapper(sqlSession).getByUserId(userId);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Session by UserId {} {}",userId, ex);
            throw new BaseException(new ErrorItem(ErrorCode.SESSION_IS_NOT_FOUND, String.valueOf(userId)));
        }
    }

    public Session getByCookie(String cookie) throws BaseException {
        LOGGER.debug("DAO get Session by cookie {}", cookie);
        try (SqlSession sqlSession = getSession()) {
            return getSessionMapper(sqlSession).getByCookie(cookie);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Session by cookie {} {}",cookie, ex);
            throw new BaseException(new ErrorItem(ErrorCode.SESSION_IS_NOT_FOUND, String.valueOf(cookie)));
        }
    }
}
