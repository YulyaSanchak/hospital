package net.thumbtack.school.daoImpl;


import net.thumbtack.school.dao.SpecialityDao;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.exceptions.ErrorCode;
import net.thumbtack.school.exceptions.ErrorItem;
import net.thumbtack.school.model.Speciality;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SpecialityDaoImpl extends DaoImplBase implements SpecialityDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpecialityDaoImpl.class);

    public Speciality insert(Speciality speciality) {
        LOGGER.debug("DAO insert Speciality {} ", speciality);
        try (SqlSession sqlSession = getSession()) {
            try {
                getSpecialityMapper(sqlSession).insert(speciality);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert Speciality {}, {}", speciality, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return speciality;
    }


    public Speciality getById(int id) {
        LOGGER.debug("DAO get Speciality by Id {}", id);
        try (SqlSession sqlSession = getSession()) {
            return getSpecialityMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Speciality by Id {} {}", id, ex);
            throw ex;
        }
    }

    public Speciality getByName(String name) throws BaseException {
        LOGGER.debug("DAO get Speciality by name {}", name);
        try (SqlSession sqlSession = getSession()) {
            return getSpecialityMapper(sqlSession).getByName(name);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Speciality by name {} {}", name, ex);
            throw new BaseException(new ErrorItem(ErrorCode.SPECIALITY_IS_NOT_FOUND,name));
        }
    }

    public void delete(Speciality speciality) {
        LOGGER.debug("DAO delete Speciality {} ", speciality);
        try (SqlSession sqlSession = getSession()) {
            try {
                getSpecialityMapper(sqlSession).delete(speciality);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete Speciality {},{} ", speciality, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    public void deleteAll() {
        LOGGER.debug("DAO delete all Specialities ");
        try (SqlSession sqlSession = getSession()) {
            try {
                getSpecialityMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete all Specialities {} ", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }


}
