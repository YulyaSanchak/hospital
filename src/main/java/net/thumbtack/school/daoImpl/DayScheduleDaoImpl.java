package net.thumbtack.school.daoImpl;

import net.thumbtack.school.dao.DayScheduleDao;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.exceptions.ErrorCode;
import net.thumbtack.school.exceptions.ErrorItem;
import net.thumbtack.school.model.DaySchedule;
import net.thumbtack.school.model.users.Doctor;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.List;

public class DayScheduleDaoImpl extends DaoImplBase implements DayScheduleDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(DayScheduleDaoImpl.class);

    public DaySchedule insert(DaySchedule daySchedule) {
        LOGGER.debug("DAO insert DaySchedule {} ", daySchedule);
        try (SqlSession sqlSession = getSession()) {
            try {
                getDayScheduleMapper(sqlSession).insert(daySchedule);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert DaySchedule {}, {}", daySchedule, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return daySchedule;
    }

    public DaySchedule getById(int id) {
        LOGGER.debug("DAO get DaySchedule by Id {}", id);
        try (SqlSession sqlSession = getSession()) {
            return getDayScheduleMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get DaySchedule by Id {} {}", id, ex);
            throw ex;
        }
    }

    public List<DaySchedule> getByDoctor(Doctor doctor) {
        LOGGER.debug("DAO get DaySchedule by Doctor {}", doctor);
        try (SqlSession sqlSession = getSession()) {
            return getDayScheduleMapper(sqlSession).getByDoctor(doctor);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get DaySchedule by Doctor {} {}", doctor, ex);
            throw ex;
        }
    }

    public void delete(DaySchedule daySchedule) {
        LOGGER.debug("DAO delete DaySchedule {} ", daySchedule);
        try (SqlSession sqlSession = getSession()) {
            try {
                getDayScheduleMapper(sqlSession).delete(daySchedule);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete DaySchedule {},{} ", daySchedule, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    public void deleteAll() {
        LOGGER.debug("DAO delete all DaySchedules");
        try (SqlSession sqlSession = getSession()) {
            try {
                getDayScheduleMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete all DaySchedules");
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }
}
