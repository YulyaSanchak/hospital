package net.thumbtack.school.daoImpl;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import net.thumbtack.school.dao.CommonDao;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.exceptions.ErrorCode;
import net.thumbtack.school.exceptions.ErrorItem;
import net.thumbtack.school.model.UserType;
import net.thumbtack.school.model.users.Admin;
import net.thumbtack.school.model.users.User;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommonDaoImpl extends DaoImplBase implements CommonDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommonDaoImpl.class);
    @Override
    public void clear(){
        LOGGER.debug("Clear Database");
        try (SqlSession sqlSession = getSession()) {
            try {
                getCommissionMapper(sqlSession).deleteAllDoctorsFromCommissions();
                getCommissionMapper(sqlSession).deleteAll();
                getReceptionMapper(sqlSession).deleteAll();
                getDayScheduleMapper(sqlSession).deleteAll();
                getTicketMapper(sqlSession).deleteAll();
                getPatientMapper(sqlSession).deleteAll();
                getDoctorMapper(sqlSession).deleteAll();
                getAdminMapper(sqlSession).deleteAll();
                getUserMapper(sqlSession).deleteAll();

            } catch (RuntimeException ex) {
                LOGGER.info("Can't clear database");
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    public void addDefaultAdmin() throws BaseException {
        LOGGER.debug("add default admin");
        try (SqlSession sqlSession = getSession()) {
            try {
                User user = new User(0,"Ivan","Ivanov","Ivanovich","admin"
                        ,"admin!!!",UserType.ADMIN);
                getUserMapper(sqlSession).insert(user,UserType.ADMIN);
                getAdminMapper(sqlSession).insert(new Admin(0,user.getUserId(),"Ivan","Ivanov"
                        ,"Ivanovich","admin","admin!!!","admin"));

            } catch (RuntimeException ex) {
                LOGGER.info("Can't add default admin");
                sqlSession.rollback();
                throw ex;
            } catch (MySQLIntegrityConstraintViolationException e) {
                throw new BaseException(new ErrorItem(ErrorCode.LOGIN_IS_EXIST));
            }
            sqlSession.commit();
        }
    }
}
