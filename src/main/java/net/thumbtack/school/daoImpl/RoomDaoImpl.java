package net.thumbtack.school.daoImpl;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import net.thumbtack.school.dao.RoomDao;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.exceptions.ErrorCode;
import net.thumbtack.school.exceptions.ErrorItem;
import net.thumbtack.school.model.Room;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoomDaoImpl extends DaoImplBase implements RoomDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(RoomDaoImpl.class);

    public Room insert(Room room){
        LOGGER.debug("DAO insert Room {} ",room);
        try (SqlSession sqlSession = getSession()) {
            try {
               getRoomMapper(sqlSession).insert(room);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert Room {}, {}", room, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return room;
    }


    public Room getById(int id) {
        LOGGER.debug("DAO get Room by Id {}", id);
        try (SqlSession sqlSession = getSession()) {
            return getRoomMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Room by Id {} {}", id, ex);
            throw ex;
        }
    }

    public Room getByName(String name) throws BaseException {
        LOGGER.debug("DAO get Room by name {}", name);
        try (SqlSession sqlSession = getSession()) {
            return getRoomMapper(sqlSession).getByName(name);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Room by name {} {}", name, ex);
            throw new BaseException(new ErrorItem(ErrorCode.ROOM_IS_NOT_FOUND,name));
        }
    }

    public Room getBusyRoomByName(String name) throws BaseException {
        LOGGER.debug("DAO get busy Room by name {}", name);
        try (SqlSession sqlSession = getSession()) {
            return getRoomMapper(sqlSession).getBusyRoomByName(name);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get busy Room by name {} {}", name, ex);
            throw new BaseException(new ErrorItem(ErrorCode.ROOM_IS_NOT_FOUND,name));
        }
    }

    public void delete(Room room) {
        LOGGER.debug("DAO delete Room {} ", room);
        try (SqlSession sqlSession = getSession()) {
            try {
                getRoomMapper(sqlSession).delete(room);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete Room {},{} ", room, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    public void deleteAll() {
        LOGGER.debug("DAO delete all Rooms ");
        try (SqlSession sqlSession = getSession()) {
            try {
                getRoomMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete all Rooms {} ", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }


    public Room update(Room Room) {
        LOGGER.debug("DAO update Room {}", Room);
        try (SqlSession sqlSession = getSession()) {
            try {
                getRoomMapper(sqlSession).update(Room);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't update Room{} {} ", Room, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return Room;
    }
}
