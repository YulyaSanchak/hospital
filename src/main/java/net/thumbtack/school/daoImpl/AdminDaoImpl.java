package net.thumbtack.school.daoImpl;

import net.thumbtack.school.dao.AdminDao;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.exceptions.ErrorCode;
import net.thumbtack.school.exceptions.ErrorItem;
import net.thumbtack.school.model.UserType;
import net.thumbtack.school.model.users.Admin;
import net.thumbtack.school.model.users.User;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class AdminDaoImpl extends DaoImplBase implements AdminDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdminDaoImpl.class);

    public Admin insert(Admin admin) throws BaseException {
        LOGGER.debug("DAO insert Admin {} ",admin);
        try (SqlSession sqlSession = getSession()) {
            try {
                getUserMapper(sqlSession).insert(admin,UserType.ADMIN);
                getAdminMapper(sqlSession).insert(admin);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert Admin {}, {}", admin, ex);
                sqlSession.rollback();
                throw ex;
            }catch (MySQLIntegrityConstraintViolationException ex){
                throw new BaseException(new ErrorItem(ErrorCode.LOGIN_IS_EXIST,admin.getLogin()));
            }
            sqlSession.commit();
        }
        return admin;
    }

    public Admin getById(int id) throws BaseException {
        LOGGER.debug("DAO get Admin by Id {}", id);
        try (SqlSession sqlSession = getSession()) {
            return getAdminMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Admin by Id {} {}", id, ex);
            throw new BaseException(new ErrorItem(ErrorCode.USER_IS_NOT_FOUND,String.valueOf(id)));
        }
    }

    public Admin getByUserId(int userId) throws BaseException {
        LOGGER.debug("DAO get Admin by userId {}", userId);
        try (SqlSession sqlSession = getSession()) {
            return getAdminMapper(sqlSession).getByUserId(userId);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Admin by userId {} {}", userId, ex);
            throw new BaseException(new ErrorItem(ErrorCode.USER_IS_NOT_FOUND,String.valueOf(userId)));
        }
    }

    public Admin getByCookie(String cookie) throws BaseException {
        LOGGER.debug("DAO get Admin by cookie {}", cookie);
        try (SqlSession sqlSession = getSession()) {
            return getAdminMapper(sqlSession).getByCookie(cookie);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Admin by cookie {} {}", cookie, ex);
            throw new BaseException(new ErrorItem(ErrorCode.USER_IS_NOT_FOUND,cookie));
        }
    }

    public void delete(Admin admin) throws BaseException {
        LOGGER.debug("DAO delete Admin {} ", admin);
        try (SqlSession sqlSession = getSession()) {
            try {
                getAdminMapper(sqlSession).delete(admin);
                getUserMapper(sqlSession).delete(new User(admin.getUserId(),admin.getFirstName()
                        ,admin.getLastName(),admin.getPatronymic(),admin.getLogin(),admin.getPassword(),UserType.ADMIN));
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete Admin {},{} ", admin, ex);
                sqlSession.rollback();
                throw new BaseException(new ErrorItem(ErrorCode.USER_IS_NOT_FOUND,String.valueOf(admin.getUserId())));
            }
            sqlSession.commit();
        }
    }

    public Admin update(Admin admin) throws BaseException {
        LOGGER.debug("DAO update Admin {}", admin);
        try (SqlSession sqlSession = getSession()) {
            try {
                getUserMapper(sqlSession).update(new User(admin.getUserId(),admin.getFirstName()
                        ,admin.getLastName(),admin.getPatronymic(),admin.getLogin(),admin.getPassword()
                        ,UserType.ADMIN));
                getAdminMapper(sqlSession).update(admin);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't update Admin{} {} ", admin, ex);
                sqlSession.rollback();
                throw new BaseException(new ErrorItem(ErrorCode.USER_IS_NOT_FOUND,String.valueOf(admin.getUserId())));
            }
            sqlSession.commit();
        }
        return admin;
    }

    public void deleteAll() {
        LOGGER.debug("DAO delete all Admins ");
        try (SqlSession sqlSession = getSession()) {
            try {
                getAdminMapper(sqlSession).deleteAll();
                getUserMapper(sqlSession).deleteAllByType(UserType.ADMIN);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete all Admins {} ", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }
}
