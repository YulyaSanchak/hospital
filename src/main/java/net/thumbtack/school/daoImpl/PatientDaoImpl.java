package net.thumbtack.school.daoImpl;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import net.thumbtack.school.dao.PatientDao;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.exceptions.ErrorCode;
import net.thumbtack.school.exceptions.ErrorItem;
import net.thumbtack.school.model.Session;
import net.thumbtack.school.model.UserType;
import net.thumbtack.school.model.users.Patient;
import net.thumbtack.school.model.users.User;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

public class PatientDaoImpl extends DaoImplBase implements PatientDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(PatientDaoImpl.class);

    public ImmutablePair<Patient, Session> insert(Patient patient) throws BaseException {
        LOGGER.debug("DAO insert Patient {} ",patient);
        Session session = new Session(patient, UUID.randomUUID().toString());
        try (SqlSession sqlSession = getSession()) {
            try {
                getUserMapper(sqlSession).insert(patient, UserType.PATIENT);
                getPatientMapper(sqlSession).insert(patient);
                getSessionMapper(sqlSession).insert(session,patient);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert Patient {}, {}", patient, ex);
                sqlSession.rollback();
                throw ex;
            }catch (MySQLIntegrityConstraintViolationException ex){
                throw new BaseException(new ErrorItem(ErrorCode.LOGIN_IS_EXIST,patient.getLogin()));
            }
            sqlSession.commit();
        }
        return new ImmutablePair<>(patient,session);
    }


    public Patient getById(int id) throws BaseException {
        LOGGER.debug("DAO get Patient by Id {}", id);
        try (SqlSession sqlSession = getSession()) {
            return getPatientMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Patient by Id {} {}", id, ex);
            throw new BaseException(new ErrorItem(ErrorCode.USER_IS_NOT_FOUND,String.valueOf(id)));
        }
    }

    public Patient getByUserId(int userId) throws BaseException {
        LOGGER.debug("DAO get Patient by userId {}", userId);
        try (SqlSession sqlSession = getSession()) {
            return getPatientMapper(sqlSession).getByUserId(userId);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Patient by userId {} {}", userId, ex);
            throw new BaseException(new ErrorItem(ErrorCode.USER_IS_NOT_FOUND,String.valueOf(userId)));
        }
    }

    public void delete(Patient patient) {
        LOGGER.debug("DAO delete Patient {} ", patient);
        try (SqlSession sqlSession = getSession()) {
            try {
                getPatientMapper(sqlSession).delete(patient);
                getUserMapper(sqlSession).delete(new User(patient.getUserId(),patient.getFirstName()
                        ,patient.getLastName(),patient.getPatronymic(),patient.getLogin(),patient.getPassword(),UserType.PATIENT));
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete Patient {},{} ", patient, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    public void deleteAll() {
        LOGGER.debug("DAO delete all Patients ");
        try (SqlSession sqlSession = getSession()) {
            try {
                getPatientMapper(sqlSession).deleteAll();
                getUserMapper(sqlSession).deleteAllByType(UserType.PATIENT);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete all Patients {} ", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }


    public Patient update(Patient patient) throws BaseException {
        LOGGER.debug("DAO update Patient {}", patient);
        try (SqlSession sqlSession = getSession()) {
            try {
                getUserMapper(sqlSession).update(new User(patient.getUserId(),patient.getFirstName()
                        ,patient.getLastName(),patient.getPatronymic(),patient.getLogin(),patient.getPassword()
                        ,UserType.PATIENT));
                getPatientMapper(sqlSession).update(patient);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't update Patient{} {} ", patient, ex);
                sqlSession.rollback();
                throw new BaseException(new ErrorItem(ErrorCode.USER_IS_NOT_FOUND,String.valueOf(patient.getUserId())));
            }
            sqlSession.commit();
        }
        return patient;
    }
}
