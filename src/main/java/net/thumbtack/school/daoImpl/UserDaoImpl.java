package net.thumbtack.school.daoImpl;


import net.thumbtack.school.dao.UserDao;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.exceptions.ErrorCode;
import net.thumbtack.school.exceptions.ErrorItem;
import net.thumbtack.school.model.Session;
import net.thumbtack.school.model.users.User;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserDaoImpl extends DaoImplBase implements UserDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);

    @Override
    public User update(User user) throws BaseException {
        LOGGER.debug("DAO update User {}", user);
        try (SqlSession sqlSession = getSession()) {
            try {
                getUserMapper(sqlSession).update(user);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't update User {}, {}", user, ex);
                sqlSession.rollback();
                throw new BaseException(new ErrorItem(ErrorCode.USER_IS_NOT_FOUND, String.valueOf(user.getUserId())));
            }
            sqlSession.commit();
        }
        return user;
    }

    @Override
    public void delete(Session session) throws BaseException {
        LOGGER.debug("DAO delete User by cookie {}", session.getCookie());
        try (SqlSession sqlSession = getSession()) {
            try {
                session =  getSessionMapper(sqlSession).getByCookie(session.getCookie());
                getUserMapper(sqlSession).deleteById(session.getUser().getUserId());
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete User by cookie {}", ex);
                sqlSession.rollback();
                throw new BaseException(new ErrorItem(ErrorCode.USER_IS_NOT_FOUND, String.valueOf(session.getUser().getUserId())));
            }
            sqlSession.commit();
        }
    }

    @Override
    public User getByLogin(String login) throws BaseException {
        LOGGER.debug("DAO get User by Login {}", login);
        try (SqlSession sqlSession = getSession()) {
            return getUserMapper(sqlSession).getByLogin(login);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get user {}", ex);
            throw new BaseException(new ErrorItem(ErrorCode.LOGIN_DOES_NOT_EXIST, login));
        }
    }

    @Override
    public User getByCookie(String cookie) throws BaseException {
        LOGGER.debug("DAO get User by Cookie {}", cookie);
        try (SqlSession sqlSession = getSession()) {
            return getUserMapper(sqlSession).getByCookie(cookie);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get User by cookie {}", ex);
            throw new BaseException(new ErrorItem(ErrorCode.COOKIE_IS_NOT_FOUND, cookie));
        }
    }
}
