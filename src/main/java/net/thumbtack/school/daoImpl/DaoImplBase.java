package net.thumbtack.school.daoImpl;

import net.thumbtack.school.mappers.*;
import net.thumbtack.school.model.DaySchedule;
import net.thumbtack.school.model.Session;
import net.thumbtack.school.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;

public class DaoImplBase {

    protected SqlSession getSession() {
        return MyBatisUtils.getSqlSessionFactory().openSession();
    }

    protected PatientMapper getPatientMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(PatientMapper.class);
    }

    protected AdminMapper getAdminMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(AdminMapper.class);
    }

    protected DoctorMapper getDoctorMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(DoctorMapper.class);
    }

    protected UserMapper getUserMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(UserMapper.class);
    }

    protected RoomMapper getRoomMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(RoomMapper.class);
    }

    protected SpecialityMapper getSpecialityMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(SpecialityMapper.class);
    }

    protected ReceptionMapper getReceptionMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(ReceptionMapper.class);
    }

    protected DayScheduleMapper getDayScheduleMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(DayScheduleMapper.class);
    }

    protected TicketMapper getTicketMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(TicketMapper.class);
    }

    protected CommissionMapper getCommissionMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(CommissionMapper.class);
    }

    protected SessionMapper getSessionMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(SessionMapper.class);
    }

    protected StatisticsMapper getStatisticsMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(StatisticsMapper.class);
    }
}