package net.thumbtack.school.daoImpl;

import net.thumbtack.school.dao.CommissionDao;
import net.thumbtack.school.model.Commission;
import net.thumbtack.school.model.users.Doctor;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommissionDaoImpl extends DaoImplBase implements CommissionDao{
    private static final Logger LOGGER = LoggerFactory.getLogger(CommissionDaoImpl.class);

    public Commission insert(Commission commission) {
        LOGGER.debug("DAO insert Commission {} ", commission);
        try (SqlSession sqlSession = getSession()) {
            try {
                getCommissionMapper(sqlSession).insert(commission);
                getCommissionMapper(sqlSession).batchInsert(commission.getId(),commission.getDoctors());
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert Commission {}, {}", commission, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return commission;
    }

    public Commission getById(int id) {
        LOGGER.debug("DAO get Commission by Id {}", id);
        try (SqlSession sqlSession = getSession()) {
            return getCommissionMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Commission by Id {} {}", id, ex);
            throw ex;
        }
    }

    public Commission getByTicketNumber(String ticketNumber) {
        LOGGER.debug("DAO get Commission by ticketNumber {}", ticketNumber);
        try (SqlSession sqlSession = getSession()) {
            return getCommissionMapper(sqlSession).getByTicketNumber(ticketNumber);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Commission by ticketNumber {} {}", ticketNumber, ex);
            throw ex;
        }
    }

    public List<Commission> getByDoctor(Doctor doctor) {
        LOGGER.debug("DAO get Commission by Doctor {}", doctor);
        try (SqlSession sqlSession = getSession()) {
            return getCommissionMapper(sqlSession).getByDoctor(doctor);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Commission by Doctor {} {}", doctor, ex);
            throw ex;
        }
    }

    public void delete(Commission commission) {
        LOGGER.debug("DAO delete Commission {} ", commission);
        try (SqlSession sqlSession = getSession()) {
            try {
                getCommissionMapper(sqlSession).deleteDoctorsFromCommissions(commission);
                getCommissionMapper(sqlSession).delete(commission);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete Commission {},{} ", commission, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    public void deleteAll() {
        LOGGER.debug("DAO delete all Commissions");
        try (SqlSession sqlSession = getSession()) {
            try {
                getCommissionMapper(sqlSession).deleteAllDoctorsFromCommissions();
                getCommissionMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete all Commissions");
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    public List<Commission> getCommissions(List<Integer> ticketIds){
        LOGGER.debug("DAO get Commissions by ticketIds ");
        try (SqlSession sqlSession = getSession()) {
            Map<String, Object> map = new HashMap<>();
            map.put("list", ticketIds);
            return sqlSession.selectList("PatientMapper.getCommissions",map);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Commissions by ticketIds {}", ex);
            throw ex;
        }
    }

}
