package net.thumbtack.school.daoImpl;

import net.thumbtack.school.dao.TicketDao;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.exceptions.ErrorCode;
import net.thumbtack.school.exceptions.ErrorItem;
import net.thumbtack.school.model.Commission;
import net.thumbtack.school.model.Reception;
import net.thumbtack.school.model.Status;
import net.thumbtack.school.model.Ticket;
import net.thumbtack.school.model.users.Patient;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class TicketDaoImpl extends DaoImplBase implements TicketDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(TicketDaoImpl.class);

    public Ticket insert(Ticket ticket, Reception reception){
        LOGGER.debug("DAO insert Ticket {} ", ticket);
        try (SqlSession sqlSession = getSession()) {
            try {
                getTicketMapper(sqlSession).insert(ticket);
                getReceptionMapper(sqlSession).update(reception.getId(),ticket.getId(),Status.TICKET);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert Ticket {}, {}", ticket, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return ticket;
    }

    public Ticket insert(Ticket ticket) {
        LOGGER.debug("DAO insert Ticket {} ", ticket);
        try (SqlSession sqlSession = getSession()) {
            try {
                getTicketMapper(sqlSession).insert(ticket);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert Ticket {}, {}", ticket, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return ticket;
    }

    public Ticket getById(int id) {
        LOGGER.debug("DAO get Ticket by Id {}", id);
        try (SqlSession sqlSession = getSession()) {
            return getTicketMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Ticket by Id {} {}", id, ex);
            throw ex;
        }
    }

    public Ticket getByNumber(String number) {
        LOGGER.debug("DAO get Ticket by Number {}", number);
        try (SqlSession sqlSession = getSession()) {
            return getTicketMapper(sqlSession).getByNumber(number);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Ticket by Number {} {}", number, ex);
            throw ex;
        }
    }

    public List<Ticket> getByPatient(Patient patient) {
        LOGGER.debug("DAO get Ticket by Patient {}", patient);
        try (SqlSession sqlSession = getSession()) {
            return getTicketMapper(sqlSession).getByPatient(patient);
        } catch (RuntimeException ex) {
            LOGGER.info("Can't get Ticket by Patient {} {}", patient, ex);
            throw ex;
        }
    }

    public void delete(Ticket ticket) {
        LOGGER.debug("DAO delete Ticket {} ", ticket);
        try (SqlSession sqlSession = getSession()) {
            try {
                getTicketMapper(sqlSession).delete(ticket);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete Ticket {},{} ", ticket, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    public void deleteAll() {
        LOGGER.debug("DAO delete all Tickets");
        try (SqlSession sqlSession = getSession()) {
            try {
                getTicketMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete all Tickets");
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }
}
