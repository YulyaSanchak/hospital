package net.thumbtack.school.dao;

import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.model.users.Admin;

public interface AdminDao {
    Admin insert(Admin admin) throws BaseException;
    Admin getById(int id) throws BaseException;
    Admin getByUserId(int userId) throws BaseException;
    Admin getByCookie(String cookie) throws BaseException;
    Admin update(Admin admin) throws BaseException;
    void delete(Admin admin) throws BaseException;
}
