package net.thumbtack.school.dao;

import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.model.Speciality;

public interface SpecialityDao {
    Speciality insert(Speciality speciality) throws BaseException;
    Speciality getById(int id);
    Speciality getByName(String name) throws BaseException;
    void delete(Speciality speciality);
    void deleteAll();
}
