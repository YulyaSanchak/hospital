package net.thumbtack.school.dao;

import net.thumbtack.school.containers.statistics.DoctorStatistic;
import net.thumbtack.school.containers.statistics.PatientStatistic;
import net.thumbtack.school.containers.statistics.Statistic;
import net.thumbtack.school.model.Status;
import net.thumbtack.school.model.TicketType;

import java.time.LocalDate;
import java.util.List;

public interface StatisticsDao {

    int getAllPatientsQuantity(LocalDate startDate, LocalDate endDate) ;

    int getAllReceptionsQuantity(LocalDate startDate, LocalDate endDate);

    List<Statistic> getAllReceptionsWithStatusQuantity(LocalDate startDate, LocalDate endDate);

    Integer getAllCommissionsQuantity(LocalDate startDate, LocalDate endDate);

    Integer  getDoctorsInCommissionsQuantity(LocalDate startDate, LocalDate endDate);

    Integer  getDoctorsInReceptions(LocalDate startDate, LocalDate endDate);

    //Статистика по врачам
    List<DoctorStatistic> getWorkingDaysQuantity(Integer specialityId, LocalDate startDate, LocalDate endDate);

    List<DoctorStatistic> getReceptionsQuantity(Integer specialityId, LocalDate startDate, LocalDate endDate);

    List<DoctorStatistic> getReceptionsQuantityByStatus(Integer specialityId, LocalDate startDate, LocalDate endDate, Status status);

    List<DoctorStatistic> getCommissionsQuantity(Integer specialityId, LocalDate startDate, LocalDate endDate);

    List<DoctorStatistic> getPatientsQuantity(Integer specialityId, LocalDate startDate, LocalDate endDate);

    //Статистика по пациентам
    List<PatientStatistic> getTicketPatientsQuantity(LocalDate startDate, LocalDate endDate);

    List<PatientStatistic> getTicketPatientsQuantityByType(LocalDate startDate, LocalDate endDate, TicketType ticketType);
}
