package net.thumbtack.school.dao;

import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.model.Speciality;
import net.thumbtack.school.model.users.Doctor;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface DoctorDao {
    Doctor insert(Doctor doctor) throws BaseException;
    Doctor update(Doctor doctor) throws BaseException;
    Doctor getById(int id) throws BaseException;
    Doctor getByUserId(int userId) throws BaseException;
    Doctor getInfo(int id, LocalDate startDate, LocalDate endDate) throws BaseException;
    List<Doctor> getDoctorsInfo(Speciality speciality, LocalDate startDate, LocalDate endDate) throws BaseException;
    void delete(Doctor doctor);
    void deleteAll();
    List<Doctor> getAll();
    List<Doctor> getBySpeciality(Speciality speciality);
    Doctor update(Doctor doctor, LocalDate startDate , LocalDate endDate, boolean isCreateSchedule) throws BaseException;
    Doctor getDoctorForTicket(int id, LocalDate date, LocalTime time) throws BaseException;
    List<Doctor> getDoctorsForTicket(Speciality speciality, LocalDate date, LocalTime time) throws BaseException;
    List<Doctor> getDoctorsForCommission(List<Integer> ids, LocalDate date) throws BaseException;
    List<Doctor> getDoctorsForDeleteCommission( LocalDate date, Integer ticketId) throws BaseException;
}
