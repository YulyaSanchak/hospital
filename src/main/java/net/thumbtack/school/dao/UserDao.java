package net.thumbtack.school.dao;

import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.model.Session;
import net.thumbtack.school.model.users.User;

public interface UserDao {
    User update(User user) throws BaseException;

    void delete(Session session) throws BaseException;

    User getByLogin(String login) throws BaseException;

    User getByCookie(String cookie) throws BaseException;
}
