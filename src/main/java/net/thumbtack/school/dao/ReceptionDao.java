package net.thumbtack.school.dao;

import net.thumbtack.school.containers.DurationReception;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.model.Reception;
import net.thumbtack.school.model.Status;
import net.thumbtack.school.model.Ticket;
import net.thumbtack.school.model.users.Doctor;

import java.time.LocalDate;
import java.util.List;

public interface ReceptionDao {
    Reception insert(Reception reception);
    void batchInsert(List<Reception> receptions);
    void delete(Reception reception);
    void deleteAll();
    Reception getById(int id);
    Reception getByTicket(Ticket ticket);
    List<Reception> getByDoctor(Doctor doctor);
    void update(int receptionId, Integer ticketId, Status status);
    List<Reception> updateReceptions(List<Reception> receptions,Integer ticketId, Status status);
    void updateForCommission(List<Integer> ids) throws BaseException;
    void deleteByIds(List<Integer> ids);
    void updateState(Integer ticketId, Status status);
    List<DurationReception> getDurationReceptions(List<Integer> ids, LocalDate date);
    List<Reception> getReceptions(List<Integer> ticketIds);
    List<Reception> getTicketReceptionsPatientByDate(int patientId, LocalDate date, Integer doctorId);
    int getDurationReceptionsByDate(int doctorId, LocalDate date);
    void updateForTicketReception(Integer id,Integer ticketId, Status status) throws BaseException;
}
