package net.thumbtack.school.dao;

import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.model.Commission;
import net.thumbtack.school.model.Reception;
import net.thumbtack.school.model.Ticket;
import net.thumbtack.school.model.users.Patient;

import java.util.List;

public interface TicketDao {
    Ticket insert(Ticket ticket, Reception reception) throws BaseException;
    Ticket insert(Ticket ticket);
    void delete(Ticket ticket);
    void deleteAll();
    List<Ticket> getByPatient(Patient patient);
    Ticket getById(int id);
    Ticket getByNumber(String number);
}
