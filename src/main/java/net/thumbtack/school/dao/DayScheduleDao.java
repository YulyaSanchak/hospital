package net.thumbtack.school.dao;

import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.model.DaySchedule;
import net.thumbtack.school.model.users.Doctor;

import java.time.LocalDate;
import java.util.List;

public interface DayScheduleDao {
    DaySchedule insert(DaySchedule daySchedule);
    void delete(DaySchedule daySchedule);
    void deleteAll();
    DaySchedule getById(int id);
    List<DaySchedule> getByDoctor(Doctor doctor);

}
