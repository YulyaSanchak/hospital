package net.thumbtack.school.dao;


import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.model.Session;
import net.thumbtack.school.model.users.User;

public interface SessionDao {

    Session insert(User user) throws BaseException;

    void delete(String value) throws BaseException;

    void deleteAll();

    Session getByUserId(int userId) throws BaseException;
    Session getByCookie(String cookie) throws BaseException;
}
