package net.thumbtack.school.dao;

import net.thumbtack.school.model.Commission;
import net.thumbtack.school.model.Ticket;
import net.thumbtack.school.model.users.Doctor;

import java.util.List;

public interface CommissionDao {
    Commission insert(Commission commission);
    void delete(Commission commission);
    void deleteAll();
    Commission getById(int id);
    List<Commission> getByDoctor(Doctor doctor);
    Commission getByTicketNumber(String ticketNumber);
    List<Commission> getCommissions(List<Integer> ticketIds);
}
