package net.thumbtack.school.dao;

import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.model.Room;

public interface RoomDao {
    Room insert(Room room) throws BaseException;
    Room getById(int id);
    Room getByName(String name) throws BaseException;
    Room getBusyRoomByName(String name) throws BaseException;
    Room update(Room room);
    void delete(Room room);
    void deleteAll();
}
