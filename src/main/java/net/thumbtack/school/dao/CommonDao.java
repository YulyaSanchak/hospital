package net.thumbtack.school.dao;


import net.thumbtack.school.exceptions.BaseException;

public interface CommonDao {
    void clear();
    void addDefaultAdmin() throws BaseException;
}
