package net.thumbtack.school.dao;

import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.model.Session;
import net.thumbtack.school.model.users.Patient;
import org.apache.commons.lang3.tuple.ImmutablePair;

public interface PatientDao {
    ImmutablePair<Patient, Session> insert(Patient patient) throws BaseException;
    void delete(Patient patient);
    Patient getById(int id) throws BaseException;
    Patient getByUserId(int userId) throws BaseException;
    Patient update(Patient patient) throws BaseException;
    void deleteAll();
}
