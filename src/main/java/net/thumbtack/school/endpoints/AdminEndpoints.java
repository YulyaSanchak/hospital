package net.thumbtack.school.endpoints;

import net.thumbtack.school.dto.requests.register_update_admin.AdminRegisterRequest;
import net.thumbtack.school.dto.requests.register_update_admin.AdminUpdateRequest;
import net.thumbtack.school.dto.responses.AdminRegisterResponse;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AdminEndpoints {

    private final AdminService service;

    @Autowired
    public AdminEndpoints(AdminService service) {
        this.service = service;
    }

    @PostMapping(value = "/api/admin", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public AdminRegisterResponse insertAdmin(@CookieValue("JAVASESSIONID") String cookie,
                                             @RequestBody @Valid AdminRegisterRequest request) throws BaseException {
        return service.insert(cookie,request);
    }

    @PutMapping(value = "/api/admin", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public AdminRegisterResponse updateAdmin(@CookieValue("JAVASESSIONID") String cookie,
                                             @RequestBody @Valid AdminUpdateRequest request) throws BaseException {
        return  service.update(cookie,request);
    }
}
