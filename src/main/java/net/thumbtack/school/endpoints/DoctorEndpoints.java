package net.thumbtack.school.endpoints;

import net.thumbtack.school.dto.requests.register_update_doctor.DoctorScheduleUpdateRequest;
import net.thumbtack.school.dto.filters.Filter;
import net.thumbtack.school.dto.requests.register_update_doctor.DoctorRegisterRequest;
import net.thumbtack.school.dto.responses.DoctorRegisterResponse;
import net.thumbtack.school.dto.responses.EmptySuccessResponse;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
public class DoctorEndpoints {

    private final DoctorService service;

    @Autowired
    public DoctorEndpoints(DoctorService service) {
        this.service = service;
    }

    @PostMapping(value = "/api/doctors", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public DoctorRegisterResponse insertDoctor(@CookieValue("JAVASESSIONID") String cookie,
                                               @RequestBody @Valid DoctorRegisterRequest request) throws BaseException {
        DoctorRegisterResponse registerResponse = service.insert(cookie,request);
        return registerResponse;
    }

    @GetMapping(value = "/api/doctors/{doctorId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public DoctorRegisterResponse getInfoDoctor(@CookieValue("JAVASESSIONID") String cookie,
                                                @PathVariable("doctorId") int doctorId,
                                                @RequestParam(value = "filter", required = false) String filter,
                                                @RequestParam(value = "startDate", required = false)
                                                    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                                @RequestParam(value = "endStart", required = false)
                                                    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate)
            throws BaseException {
        return service.getDoctorInfo(cookie, Filter.filterFromString(filter.toUpperCase()),startDate,
                endDate,doctorId);
    }

    @GetMapping(value = "/api/doctors", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DoctorRegisterResponse> getInfoDoctors(@CookieValue("JAVASESSIONID") String cookie,
                                                       @RequestParam(value = "speciality", required = false) String speciality,
                                                       @RequestParam(value = "filter", required = false) String filter,
                                                       @RequestParam(value = "startDate", required = false)
                                                @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                                       @RequestParam(value = "endStart", required = false)
                                                @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate)
            throws BaseException {
        return service.getInfoAboutDoctors(cookie, Filter.filterFromString(filter.toUpperCase()),startDate,
                endDate,speciality);
    }

    @PutMapping(value = "/api/doctor/{doctorId}", produces = MediaType.APPLICATION_JSON_VALUE,
    consumes = MediaType.APPLICATION_JSON_VALUE)
    public DoctorRegisterResponse updateDoctorSchedule(@CookieValue("JAVASESSIONID") String cookie,
                                                       @PathVariable("doctorId") int doctorId,
                                                       @RequestBody @Valid DoctorScheduleUpdateRequest request) throws BaseException {
        return service.updateDoctorSchedule(cookie,doctorId,request);
    }

    @DeleteMapping(value = "/api/doctor/{doctorId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public EmptySuccessResponse deleteDoctor(@CookieValue("JAVASESSIONID") String cookie,
                                             @PathVariable("doctorId") int doctorId) throws BaseException {
        return service.deleteDoctor(cookie,doctorId);
    }
}
