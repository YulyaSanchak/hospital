package net.thumbtack.school.endpoints;

import net.thumbtack.school.dto.responses.settings.GetSettingsResponse;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.service.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SettingEndpoint {

    private final SettingService service;

    @Autowired
    public SettingEndpoint(SettingService service) {
        this.service = service;
    }

    @GetMapping(value = "/api/settings", produces = MediaType.APPLICATION_JSON_VALUE)
    public GetSettingsResponse getSettings(@CookieValue(value = "JAVASESSIONID", required = false) String cookie)
            throws BaseException {
        return service.getSettings(cookie);
    }
}
