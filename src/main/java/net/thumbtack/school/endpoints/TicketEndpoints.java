package net.thumbtack.school.endpoints;


import net.thumbtack.school.dto.requests.CommissionRequest;
import net.thumbtack.school.dto.requests.CreateTicketRequest;
import net.thumbtack.school.dto.responses.tickets.AllTicketsResponse;
import net.thumbtack.school.dto.responses.tickets.CommissionResponse;
import net.thumbtack.school.dto.responses.EmptySuccessResponse;
import net.thumbtack.school.dto.responses.tickets.ReceptionTicketResponse;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class TicketEndpoints {

    private final TicketService service;

    @Autowired
    public TicketEndpoints(TicketService service) {
        this.service = service;
    }

    @PostMapping(value = "/api/tickets", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ReceptionTicketResponse insertTicket(@CookieValue("JAVASESSIONID") String cookie,
                                                @RequestBody @Valid CreateTicketRequest request) throws BaseException {
        return service.createTicketReception(cookie,request);
    }

    @DeleteMapping(value = "/api/tickets/{number}", produces = MediaType.APPLICATION_JSON_VALUE)
    public EmptySuccessResponse deleteTicket(@CookieValue("JAVASESSIONID") String cookie,
                                             @PathVariable("number") String number) throws BaseException {
        return service.cancelTicket(cookie,number);
    }

    @PostMapping(value = "/api/comissions",produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public CommissionResponse insertCommission(@CookieValue("JAVASESSIONID") String cookie,
                                               @RequestBody @Valid CommissionRequest request) throws BaseException {
        return service.createTicketCommission(cookie,request);
    }

    @DeleteMapping(value = "/api/comissions/{numberCommissionTicket}",produces = MediaType.APPLICATION_JSON_VALUE)
    public EmptySuccessResponse deleteCommission(@CookieValue("JAVASESSIONID") String cookie,
                                                 @PathVariable("numberCommissionTicket") String number) throws BaseException {
        return service.cancelCommission(cookie,number);
    }

    @GetMapping(value = "/api/tickets",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AllTicketsResponse> getAllTicket(@CookieValue("JAVASESSIONID") String cookie) throws BaseException {
        return service.getAllTickets(cookie);
    }
}
