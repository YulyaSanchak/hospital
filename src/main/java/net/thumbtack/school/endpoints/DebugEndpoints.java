package net.thumbtack.school.endpoints;


import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.service.DebugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DebugEndpoints {


    private final DebugService service;

    @Autowired
    public DebugEndpoints(DebugService service) {
        this.service = service;
    }

    @PostMapping(value = "/api/debug/clear")
    public void clear() throws BaseException {
        service.clear();
    }
}
