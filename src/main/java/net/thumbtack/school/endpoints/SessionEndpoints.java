package net.thumbtack.school.endpoints;



import net.thumbtack.school.dto.requests.LoginDtoRequest;
import net.thumbtack.school.dto.responses.EmptySuccessResponse;
import net.thumbtack.school.dto.responses.LoginDtoResponse;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.model.Session;
import net.thumbtack.school.service.SessionService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
public class SessionEndpoints {

    private final SessionService service;

    @Autowired
    public SessionEndpoints(SessionService service) {
        this.service = service;
    }

    @PostMapping(value = "/api/session", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public LoginDtoResponse login(@RequestBody @Valid LoginDtoRequest request, HttpServletResponse response)
            throws BaseException {
        ImmutablePair<LoginDtoResponse, Session> pair = service.login(request);
        response.addCookie(new Cookie("JAVASESSIONID", pair.getRight().getCookie()));
        return pair.getLeft();
    }

    @DeleteMapping(value = "/api/session", produces = MediaType.APPLICATION_JSON_VALUE)
    public EmptySuccessResponse logout(@CookieValue("JAVASESSIONID") String cookie) throws BaseException {
        return service.logout(cookie);
    }

    @GetMapping(value = "/api/account", produces = MediaType.APPLICATION_JSON_VALUE)
    public LoginDtoResponse account(@CookieValue("JAVASESSIONID") String cookie, HttpServletResponse response) throws BaseException {
        ImmutablePair<LoginDtoResponse, Session> pair = service.account(cookie);
        response.addCookie(new Cookie("JAVASESSIONID", pair.getRight().getCookie()));
        return pair.getLeft();
    }
}
