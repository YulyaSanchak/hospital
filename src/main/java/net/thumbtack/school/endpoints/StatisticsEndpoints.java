package net.thumbtack.school.endpoints;

import net.thumbtack.school.dto.responses.statistics.StatisticResponse;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class StatisticsEndpoints {

    private final StatisticsService service;

    @Autowired
    public StatisticsEndpoints(StatisticsService service) {
        this.service = service;
    }

    @GetMapping(value = "/api/statistics", produces = MediaType.APPLICATION_JSON_VALUE)
    public StatisticResponse getStatistics(@CookieValue(value = "JAVASESSIONID", required = false) String cookie,
                                           @RequestParam(value = "filter", required = false) String filter,
                                           @RequestParam(value = "specialityName") String specialityName,
                                           @RequestParam(value = "startDate", required = false)String startDate,
                                           @RequestParam(value = "endDate", required = false)String endDate)
            throws BaseException {
        return service.getStatistics(cookie,filter,specialityName,startDate,endDate);
    }
}
