package net.thumbtack.school.endpoints;

import net.thumbtack.school.dto.requests.register_update_patient.PatientRegisterRequest;
import net.thumbtack.school.dto.requests.register_update_patient.PatientUpdateRequest;
import net.thumbtack.school.dto.responses.PatientRegisterUpdateResponse;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.model.Session;
import net.thumbtack.school.service.PatientService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
public class PatientEndpoints {

    private final PatientService service;

    @Autowired
    public PatientEndpoints(PatientService service) {
        this.service = service;
    }

    @PostMapping(value = "/api/patients", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public PatientRegisterUpdateResponse insertPatient(@RequestBody @Valid PatientRegisterRequest request,
                                                       HttpServletResponse response) throws BaseException {
        ImmutablePair<PatientRegisterUpdateResponse, Session> pair = service.insert(request);
        PatientRegisterUpdateResponse dtoResponse = pair.getLeft();
        response.addCookie(new Cookie("JAVASESSIONID", pair.getRight().getCookie()));
        return dtoResponse;
    }

    @GetMapping(value = "/api/patients/{patientId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public PatientRegisterUpdateResponse get(@CookieValue("JAVASESSIONID") String cookie,
                                             @PathVariable("patientId") int patientId,
                                             HttpServletResponse response) throws BaseException {
        ImmutablePair<PatientRegisterUpdateResponse,Session> pair = service.getById(cookie,patientId);
        response.addCookie(new Cookie("JAVASESSIONID", pair.getRight().getCookie()));
        return pair.getLeft();
    }

    @PutMapping(value = "/api/client", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public PatientRegisterUpdateResponse update(@CookieValue("JAVASESSIONID") String cookie,
                                                @RequestBody @Valid PatientUpdateRequest request,
                                                HttpServletResponse response) throws BaseException {
        ImmutablePair<PatientRegisterUpdateResponse, Session> pair = service.update(cookie,request);
        PatientRegisterUpdateResponse dtoResponse = pair.getLeft();
        response.addCookie(new Cookie("JAVASESSIONID", pair.getRight().getCookie()));
        return dtoResponse;
    }
}
