package net.thumbtack.school.model;

import net.thumbtack.school.model.users.Doctor;

public class Room {
    private int id;
    private String name;
    private Doctor doctor;

    public Room() {
    }

    public Room(int id, String name) {
        this(id,name,null);
    }

    public Room(int id, String name, Doctor doctor) {
        this.id = id;
        this.name = name;
        this.doctor = doctor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Room)) return false;

        Room room = (Room) o;

        if (getId() != room.getId()) return false;
        if (getName() != null ? !getName().equals(room.getName()) : room.getName() != null) return false;
        return getDoctor() != null ? getDoctor().equals(room.getDoctor()) : room.getDoctor() == null;
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getDoctor() != null ? getDoctor().hashCode() : 0);
        return result;
    }

}
