package net.thumbtack.school.model;

import net.thumbtack.school.model.users.Doctor;

import java.util.List;

public class Speciality {
    private int id;
    private String name;
    private List<Doctor> doctor;

    public Speciality() {
    }

    public Speciality(int id, String name) {
        this(id,name,null);
    }

    public Speciality(int id, String name, List<Doctor> doctor) {
        this.id = id;
        this.name = name;
        this.doctor = doctor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Doctor> getDoctor() {
        return doctor;
    }

    public void setDoctor(List<Doctor> doctor) {
        this.doctor = doctor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Speciality)) return false;

        Speciality that = (Speciality) o;

        if (getId() != that.getId()) return false;
        if (getName() != null ? !getName().equals(that.getName()) : that.getName() != null) return false;
        return getDoctor() != null ? getDoctor().equals(that.getDoctor()) : that.getDoctor() == null;
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getDoctor() != null ? getDoctor().hashCode() : 0);
        return result;
    }

}
