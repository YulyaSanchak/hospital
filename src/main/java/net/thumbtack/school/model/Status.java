package net.thumbtack.school.model;

public enum Status {
    TICKET,
    FREE,
    BUSY
}
