package net.thumbtack.school.model;

public enum UserType {
    ADMIN,
    DOCTOR,
    PATIENT
}
