package net.thumbtack.school.model;

import net.thumbtack.school.model.users.Patient;


public class Ticket {
    private int id;
    private String ticketNumber;
    private Patient patient;
    private TicketType ticketType;

    public Ticket() {
    }

    public Ticket(int id, String ticketNumber, Patient patient, TicketType ticketType) {
        this.id = id;
        this.ticketNumber = ticketNumber;
        this.patient = patient;
        this.ticketType = ticketType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public TicketType getTicketType() {
        return ticketType;
    }

    public void setTicketType(TicketType ticketType) {
        this.ticketType = ticketType;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ticket)) return false;

        Ticket ticket = (Ticket) o;

        if (getId() != ticket.getId()) return false;
        if (getTicketNumber() != null ? !getTicketNumber().equals(ticket.getTicketNumber()) : ticket.getTicketNumber() != null)
            return false;
        if (getPatient() != null ? !getPatient().equals(ticket.getPatient()) : ticket.getPatient() != null)
            return false;
        return getTicketType() == ticket.getTicketType();
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getTicketNumber() != null ? getTicketNumber().hashCode() : 0);
        result = 31 * result + (getPatient() != null ? getPatient().hashCode() : 0);
        result = 31 * result + (getTicketType() != null ? getTicketType().hashCode() : 0);
        return result;
    }

}
