package net.thumbtack.school.model;

import net.thumbtack.school.model.users.Doctor;
import net.thumbtack.school.model.users.Patient;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Commission {
    private int id;
    private LocalDateTime time;
    private int duration;
    private String roomName;
    private Ticket ticket;
    private List<Doctor> doctors;

    public Commission() {
    }

    public Commission(int id, LocalDateTime time, int duration, String roomName, List<Doctor> doctors) {
        this(id,time,duration,roomName,doctors,null);
    }

    public Commission(int id, LocalDateTime time, int duration,String roomName, List<Doctor> doctors, Ticket ticket) {
        this.id = id;
        this.time = time;
        this.duration = duration;
        this.doctors = doctors;
        this.ticket = ticket;
        this.roomName = roomName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public List<Doctor> getDoctors() {
        return doctors;
    }

    public void setDoctors(List<Doctor> doctors) {
        this.doctors = doctors;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Commission)) return false;

        Commission that = (Commission) o;

        if (getId() != that.getId()) return false;
        if (getDuration() != that.getDuration()) return false;
        if (getTime() != null ? !getTime().equals(that.getTime()) : that.getTime() != null) return false;
        if (getRoomName() != null ? !getRoomName().equals(that.getRoomName()) : that.getRoomName() != null)
            return false;
        if (getTicket() != null ? !getTicket().equals(that.getTicket()) : that.getTicket() != null) return false;
        return getDoctors() != null ? getDoctors().equals(that.getDoctors()) : that.getDoctors() == null;
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getTime() != null ? getTime().hashCode() : 0);
        result = 31 * result + getDuration();
        result = 31 * result + (getRoomName() != null ? getRoomName().hashCode() : 0);
        result = 31 * result + (getTicket() != null ? getTicket().hashCode() : 0);
        result = 31 * result + (getDoctors() != null ? getDoctors().hashCode() : 0);
        return result;
    }

}
