package net.thumbtack.school.model;

import net.thumbtack.school.model.users.Doctor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DaySchedule {
    private int id;
    private Doctor doctor;
    private LocalDate date;
    private List<Reception> receptions;

    public DaySchedule() {
    }

    public DaySchedule(int id, Doctor doctor, LocalDate date, List<Reception> receptions) {
        this.id = id;
        this.doctor = doctor;
        this.date = date;
        this.receptions = receptions;
    }

    public DaySchedule(int id, Doctor doctor, LocalDate date){
        this(id,doctor,date,new ArrayList<>());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public List<Reception> getReceptions() {
        return receptions;
    }

    public void setReceptions(List<Reception> receptions) {
        this.receptions = receptions;
    }

}
