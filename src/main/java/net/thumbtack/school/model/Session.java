package net.thumbtack.school.model;

import net.thumbtack.school.model.users.User;

import java.util.Objects;

public class Session {
    private int id;
    private User user;
    private String cookie;

    public Session() {
    }

    public Session(User user, String cookie) {
        this(0, user, cookie);
    }

    public Session(int id, User user, String cookie) {
        this.id = id;
        this.user = user;
        this.cookie = cookie;
    }

    public Session(String cookie) {
        this.cookie = cookie;
    }

    public int getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Session)) return false;
        Session session = (Session) o;
        return getId() == session.getId() &&
                Objects.equals(getUser(), session.getUser()) &&
                Objects.equals(getCookie(), session.getCookie());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getUser(), getCookie());
    }

}
