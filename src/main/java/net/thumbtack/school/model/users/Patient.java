package net.thumbtack.school.model.users;

import net.thumbtack.school.model.Ticket;
import net.thumbtack.school.model.UserType;

import java.util.ArrayList;
import java.util.List;

public class Patient extends User {
    private int id;
    private String email;
    private String address;
    private String phone;
    private List<Ticket> tickets;

    public Patient() {
    }

    public Patient(String firstName, String lastName, String patronymic, String email, String address
            , String phone, String login, String password) {
        this(0,0,firstName,lastName,patronymic,email,address,phone,login,password,new ArrayList<Ticket>());
    }

    public Patient(int id,int userId, String firstName, String lastName, String patronymic, String email, String address
            , String phone, String login, String password) {
        this(id,userId,firstName,lastName,patronymic,email,address,phone,login,password,new ArrayList<Ticket>());
    }

    public Patient(int id,int userId, String firstName, String lastName, String patronymic, String email, String address
            , String phone, String login, String password,  List<Ticket> tickets) {
        super(userId, firstName, lastName, patronymic, login, password, UserType.PATIENT);
        this.id = id;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.tickets = tickets;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Patient)) return false;
        if (!super.equals(o)) return false;

        Patient patient = (Patient) o;

        if (getId() != patient.getId()) return false;
        if (getEmail() != null ? !getEmail().equals(patient.getEmail()) : patient.getEmail() != null) return false;
        if (getAddress() != null ? !getAddress().equals(patient.getAddress()) : patient.getAddress() != null)
            return false;
        if (getPhone() != null ? !getPhone().equals(patient.getPhone()) : patient.getPhone() != null) return false;
        return getTickets() != null ? getTickets().equals(patient.getTickets()) : patient.getTickets() == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getId();
        result = 31 * result + (getEmail() != null ? getEmail().hashCode() : 0);
        result = 31 * result + (getAddress() != null ? getAddress().hashCode() : 0);
        result = 31 * result + (getPhone() != null ? getPhone().hashCode() : 0);
        result = 31 * result + (getTickets() != null ? getTickets().hashCode() : 0);
        return result;
    }

}
