package net.thumbtack.school.model.users;

import net.thumbtack.school.model.*;

import java.util.ArrayList;
import java.util.List;

public class Doctor extends User {
    private int id;
    private Speciality speciality;
    private Room room;

    private List<DaySchedule> daySchedules;
    private List<Reception> receptions;
    private List<Commission> commissions;

    public Doctor(){
        super();
    }

    public Doctor(int id ,int userId, String firstName, String lastName, String patronymic, String login, String password
            , Speciality speciality, Room room, List<Reception> receptions, List<Commission> commissions
            , List<DaySchedule> daySchedules) {
        super(userId, firstName, lastName, patronymic, login, password, UserType.DOCTOR);
        this.id = id;
        this.speciality = speciality;
        this.room = room;
        this.receptions = receptions;
        this.commissions = commissions;
        this.daySchedules = daySchedules;

    }

    public Doctor(int id ,int userId, String firstName, String lastName, String patronymic, String login, String password
            , Speciality speciality, Room room, List<DaySchedule> daySchedules) {
        this(id,userId, firstName, lastName, patronymic, login, password, speciality,room,null,null,daySchedules);
    }

    public Doctor(int id, int userId, String firstName, String lastName, String patronymic, String login, String password
            , Speciality speciality, Room room) {
        this (id,userId,firstName,lastName,patronymic,login,password,speciality,room,new ArrayList<>(),
                new ArrayList<>(), new ArrayList<>());
    }

    public List<Reception> getReceptions() {
        return receptions;
    }

    public void setReceptions(List<Reception> receptions) {
        this.receptions = receptions;
    }

    public List<Commission> getCommissions() {
        return commissions;
    }

    public void setCommissions(List<Commission> commissions) {
        this.commissions = commissions;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Speciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public List<DaySchedule> getDaySchedules() {
        return daySchedules;
    }

    public void setDaySchedules(List<DaySchedule> daySchedules) {
        this.daySchedules = daySchedules;
    }


}
