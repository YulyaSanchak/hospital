package net.thumbtack.school.model.users;

import net.thumbtack.school.model.UserType;

public class Admin extends User {
    private int id;
    private String position;


    public Admin() {
    }

    public Admin(int id, int userId, String firstName, String lastName, String patronymic, String login
            , String password,String position) {
        super(userId, firstName, lastName, patronymic, login, password, UserType.ADMIN);
        this.position = position;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Admin)) return false;
        if (!super.equals(o)) return false;

        Admin admin = (Admin) o;

        if (getId() != admin.getId()) return false;
        return getPosition() != null ? getPosition().equals(admin.getPosition()) : admin.getPosition() == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getId();
        result = 31 * result + (getPosition() != null ? getPosition().hashCode() : 0);
        return result;
    }

}
