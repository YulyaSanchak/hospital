package net.thumbtack.school.model;

import net.thumbtack.school.model.users.Doctor;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Reception {
    private int id;
    private Ticket ticket;
    private DaySchedule daySchedule;
    private LocalTime time;
    private int duration;
    private Status status;


    public Reception() {
    }

    public Reception(int id, DaySchedule daySchedule, LocalTime time, int duration, Status status) {
       this(id,null, daySchedule, time,duration,status);
    }

    public Reception(int id, LocalTime time, int duration,Status status) {
        this(id, null, null, time,duration,status);

    }

    public Reception(int id, Ticket ticket, LocalTime time, int duration, Status status) {
        this(id,ticket, null, time,duration,status);
    }

    public Reception(int id, Ticket ticket, DaySchedule daySchedule, LocalTime time, int duration, Status status) {
        this.id = id;
        this.time = time;
        this.duration = duration;
        this.ticket = ticket;
        this.daySchedule = daySchedule;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public DaySchedule getDaySchedule() {
        return daySchedule;
    }

    public void setDaySchedule(DaySchedule daySchedule) {
        this.daySchedule = daySchedule;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }



}
