package net.thumbtack.school.exceptions;

import net.thumbtack.school.dto.responses.failure.JsonErrorResponse;
import net.thumbtack.school.dto.responses.failure.JsonSimpleErrorResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                               HttpHeaders headers, HttpStatus status,
                                                               WebRequest request) {

        List<JsonSimpleErrorResponse> errors = new ArrayList<>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(new JsonSimpleErrorResponse(HttpStatus.BAD_REQUEST, error.getField(), error.getDefaultMessage()));
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(new JsonSimpleErrorResponse(HttpStatus.BAD_REQUEST, error.getObjectName(), error.getDefaultMessage()));
        }
        JsonErrorResponse response = new JsonErrorResponse(errors);
        return handleExceptionInternal(
                ex, response, headers, HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(BaseException.class)
    public ResponseEntity<JsonErrorResponse> handleBaseError(BaseException e){
        JsonErrorResponse response= new JsonErrorResponse();
        for (ErrorItem ex: e.getErrors()) {
            response.addJson(new JsonSimpleErrorResponse(ex));
        }
        return new ResponseEntity<>(response,  HttpStatus.BAD_REQUEST);
    }
}
