package net.thumbtack.school.exceptions;

import java.util.Arrays;
import java.util.List;


public class ErrorItem {
private ErrorCode errorCode;
private List<String> fields;

    public ErrorItem(ErrorCode errorCode, List<String> fields) {
        this.errorCode = errorCode;
        this.fields = fields;
    }

    public ErrorItem(ErrorCode errorCode, String ... fields) {
        this(errorCode, Arrays.asList(fields));
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public void addField(String field) {
        this.fields.add(field);
    }
    public void addFields(List<String> fields) {
        this.fields.addAll(fields);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ErrorItem)) return false;

        ErrorItem errorItem = (ErrorItem) o;

        if (getErrorCode() != errorItem.getErrorCode()) return false;
        return getFields() != null ? getFields().equals(errorItem.getFields()) : errorItem.getFields() == null;
    }

    @Override
    public int hashCode() {
        int result = getErrorCode() != null ? getErrorCode().hashCode() : 0;
        result = 31 * result + (getFields() != null ? getFields().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ErrorItem{" +
                "errorCode=" + errorCode +
                ", fields=" + fields +
                '}';
    }
}
