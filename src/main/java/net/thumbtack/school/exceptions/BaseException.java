package net.thumbtack.school.exceptions;

import java.util.*;

public class BaseException extends Exception {

    private List<ErrorItem> errors;

    public BaseException(List<ErrorItem> errors) {
        super();
        this.errors = errors;
    }
    public BaseException(ErrorItem ... errors) {
        super();
        this.errors = Arrays.asList(errors);
    }
    public BaseException() {
        super();
        errors = new ArrayList<>();
    }

    public List<ErrorItem> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorItem> errors) {
        this.errors = errors;
    }
    public void addError(ErrorItem exception) {
        this.errors.add(exception);
    }
    public void addErrors(List<ErrorItem> errors) {
        this.errors.addAll(errors);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BaseException)) return false;
        BaseException that = (BaseException) o;
        return Objects.equals(getErrors(), that.getErrors());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getErrors());
    }

    @Override
    public String toString() {
        return "BaseException{" +
                "errors=" + errors +
                '}';
    }
}
