package net.thumbtack.school.exceptions;

public enum ErrorCode {
    SUCCESS("", ""),

    INVALID_FIRSTNAME("firstName", "You entered the wrong first name"),
    INVALID_LASTNAME("lastName", "You entered the wrong last name"),
    INVALID_PATRONYMIC("patronymic", "You entered the wrong patronymic"),
    INVALID_PASSWORD("password", "You entered the wrong password"),
    INVALID_LOGIN("login", "You entered the wrong login"),

    LOGIN_IS_EXIST("login", "Login %s is exist"),
    NAME_IS_EXIST("name", "Name %s is exist"),
    LOGIN_DOES_NOT_EXIST("login", "Login %s does not exist"),

    DOCTORS_NOT_FOUND("speciality","Doctors with speciality $s are not found"),
    DOCTOR_NOT_EXIST("doctor","Doctor are not exist"),
    DOCTORS_NOT_EXIST("doctors","Doctors are not exist"),
    USER_IS_NOT_FOUND("userId", "User with id = &s is not found"),
    COOKIE_IS_NOT_FOUND("cookie", "Session with cookie = %s is not found"),
    SESSION_IS_NOT_FOUND("userId", "Session with userId = %s is not found"),
    SPECIALITY_IS_NOT_FOUND("name", "Speciality with name = %s is not found"),
    ROOM_IS_NOT_FOUND("name", "Room with name = %s is not found"),
    ROOM_IS_BUSY("name", "Room with name = %s is not found"),
    ACCESS_ERROR("userType","Access is denied to a user with the type = &s"),
    RECEPTION_IS_BUSY("ids","One of the receptions is busy"),
    COMMISSION_NOT_CREATE("doctorIds","Not all doctors are free"),
    IMPOSSIBLE_CREATE_TICKET("time","Impossible create ticket with time = %s"),
    IMPOSSIBLE_CREATE_TICKET_WITH_DATE("date","Impossible create ticket with date = %s"),
    IMPOSSIBLE_CREATE_TICKET_WITH_DOCTOR("doctorId","Impossible create ticket with doctor = %s");

    private String field;
    private String message;
    ErrorCode(String field, String message) {
        this.field = field;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public String getMessage() {
        return message;
    }
}
