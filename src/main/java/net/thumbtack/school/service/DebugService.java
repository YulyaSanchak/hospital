package net.thumbtack.school.service;

import net.thumbtack.school.dao.CommonDao;
import net.thumbtack.school.daoImpl.CommonDaoImpl;
import net.thumbtack.school.exceptions.BaseException;
import org.springframework.stereotype.Service;

@Service
public class DebugService {
    private CommonDao dao = new CommonDaoImpl();

    public void clear() throws BaseException {
        dao.clear();
        dao.addDefaultAdmin();
    }
}



