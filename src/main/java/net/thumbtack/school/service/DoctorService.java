package net.thumbtack.school.service;

import net.thumbtack.school.dao.*;
import net.thumbtack.school.daoImpl.*;
import net.thumbtack.school.dto.requests.register_update_doctor.DoctorScheduleUpdateRequest;
import net.thumbtack.school.dto.filters.Filter;
import net.thumbtack.school.dto.requests.register_update_doctor.DayScheduleRequest;
import net.thumbtack.school.dto.requests.register_update_doctor.DoctorRegisterRequest;
import net.thumbtack.school.dto.responses.*;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.exceptions.ErrorCode;
import net.thumbtack.school.exceptions.ErrorItem;
import net.thumbtack.school.model.*;
import net.thumbtack.school.model.users.Doctor;
import net.thumbtack.school.model.users.Patient;
import net.thumbtack.school.model.users.User;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.TextStyle;
import java.util.*;

@Service
public class DoctorService {
    private DoctorDao doctorDao = new DoctorDaoImpl();
    private SpecialityDao specialityDao = new SpecialityDaoImpl();
    private RoomDao roomDao = new RoomDaoImpl();
    private UserDao userDao = new UserDaoImpl();
    private PatientDao patientDao = new PatientDaoImpl();
    private ReceptionDao receptionDao = new ReceptionDaoImpl();

    public DoctorRegisterResponse insert(String cookie, DoctorRegisterRequest request) throws BaseException {
        User user = userDao.getByCookie(cookie);
        if (user.getUserType() != UserType.ADMIN){
            throw new BaseException(new ErrorItem(ErrorCode.ACCESS_ERROR,user.getUserType().name()));
        }
        Speciality speciality = specialityDao.getByName(request.getSpeciality());
        if(speciality == null){
            throw new BaseException(new ErrorItem(ErrorCode.SPECIALITY_IS_NOT_FOUND,request.getSpeciality()));
        }
        Room room = roomDao.getByName(request.getRoom());
        if (room == null){
            throw new BaseException(new ErrorItem(ErrorCode.ROOM_IS_NOT_FOUND,request.getRoom()));
        }
        Room roomIsBusy = roomDao.getBusyRoomByName(request.getRoom());
        if(roomIsBusy != null){
            throw new BaseException(new ErrorItem(ErrorCode.RECEPTION_IS_BUSY,request.getRoom()));
        }
        Doctor doctor = new Doctor(0,0,request.getFirstName(),request.getLastName(),request.getPatronymic()
                    ,request.getLogin(),request.getPassword(),speciality,room);

        List<DaySchedule> schedule;
        if(request.getWeekSchedule() == null){
            schedule = getScheduleWithDifferentTimeReception(doctor,request);
        }else{
            schedule = getScheduleWithSameTimeReception(doctor,request);
        }
        doctor.setDaySchedules(schedule);
        Doctor doctorFromDataBase = doctorDao.insert(doctor);
        return getDoctorResponse(doctorFromDataBase,null);
    }

    public EmptySuccessResponse deleteDoctor(String cookie, int doctorId) throws BaseException {
        User user = userDao.getByCookie(cookie);
        if (user.getUserType()!=UserType.ADMIN){
            throw new BaseException(new ErrorItem(ErrorCode.ACCESS_ERROR,user.getUserType().name()));
        }
        Doctor doctor = doctorDao.getInfo(doctorId,LocalDate.now(),null);
        List<Reception> cancelReceptions = new ArrayList<>();
        for (DaySchedule daySchedule : doctor.getDaySchedules()){
            for (Reception reception : daySchedule.getReceptions()){
                if (reception.getTicket()!=null){
                    cancelReceptions.add(reception);
                }
            }
        }
        receptionDao.updateReceptions(cancelReceptions,null,Status.FREE);
        return new EmptySuccessResponse();
    }

    public List<DoctorRegisterResponse> getInfoAboutDoctors(String cookie, Filter filter,LocalDate startDate,
                                                            LocalDate endDate, String speciality) throws BaseException {
        User user = userDao.getByCookie(cookie);
        Patient patient = patientDao.getByUserId(user.getUserId());

        Speciality spec = specialityDao.getByName(speciality);
        if(spec == null){
            throw new BaseException(new ErrorItem(ErrorCode.SPECIALITY_IS_NOT_FOUND,speciality));
        }
        List<DoctorRegisterResponse> doctorResponse =  new ArrayList<>();
        if (filter == Filter.YES){
            if(startDate == null){
                startDate = LocalDate.now();
            }
            if (endDate == null){
                endDate = LocalDate.now().plusMonths(2);
            }
            List<Doctor> doctors = doctorDao.getDoctorsInfo(spec,startDate,endDate);
            for(Doctor doctor : doctors) {
                doctorResponse.add(getDoctorResponse(doctor,patient));
            }
        }else{
            List<Doctor> doctors = doctorDao.getAll();
            for(Doctor doctor: doctors){
                doctorResponse.add(new DoctorRegisterResponse(doctor.getId(),doctor.getFirstName(),doctor.getLastName(),
                        doctor.getPatronymic(),doctor.getSpeciality().getName(),doctor.getRoom().getName(),null));
            }
        }
        return doctorResponse;
    }

    public DoctorRegisterResponse getDoctorInfo(String cookie, Filter filter, LocalDate startDate, LocalDate endDate,
                                            int doctorId) throws BaseException {
        User user = userDao.getByCookie(cookie);
        Patient patient = patientDao.getByUserId(user.getUserId());

        if (filter == Filter.YES){
            if(startDate == null){
                startDate = LocalDate.now();
            }
            if (endDate == null){
                endDate = LocalDate.now().plusMonths(2);
            }
            Doctor doctor = doctorDao.getInfo(doctorId,startDate,endDate);
            return getDoctorResponse(doctor,patient);
        }else{
            Doctor doctor = doctorDao.getById(doctorId);
            return new DoctorRegisterResponse(doctor.getId(),doctor.getFirstName(),doctor.getLastName(),
                    doctor.getPatronymic(),doctor.getSpeciality().getName(),doctor.getRoom().getName(),null);
        }
    }

    public DoctorRegisterResponse updateDoctorSchedule(String cookie, int doctorId, DoctorScheduleUpdateRequest request) throws BaseException {
        User user = userDao.getByCookie(cookie);
        if (user.getUserType()!=UserType.ADMIN){
            throw new BaseException(new ErrorItem(ErrorCode.ACCESS_ERROR,user.getUserType().name()));
        }
        LocalDate startDate = LocalDate.parse(request.getDateStart());
        LocalDate endDate = LocalDate.parse(request.getDateEnd());
        DoctorRegisterResponse doctorInfo = getDoctorInfo(cookie,Filter.YES,startDate,endDate,doctorId);
        if (mayUpdate(doctorInfo.getScheduleResponse())){
            Doctor doctor = new Doctor(doctorInfo.getId(),0,doctorInfo.getFirstName(), doctorInfo.getLastName(),
                    doctorInfo.getPatronymic(),null,null,null,null);
            DoctorRegisterRequest doctorRequest = new DoctorRegisterRequest(doctorInfo.getFirstName(),
                    doctorInfo.getLastName(), doctorInfo.getPatronymic(),doctorInfo.getRoom(),doctorInfo.getSpeciality(),
                    "******","******",request.getDateStart(),request.getDateEnd(),request.getWeekDaysSchedule(),request.getWeekSchedule(),
                    request.getDuration());
            List<DaySchedule> schedule;
            if(request.getWeekSchedule() == null){
                schedule = getScheduleWithDifferentTimeReception(doctor,doctorRequest);

            }else{
                schedule = getScheduleWithSameTimeReception(doctor,doctorRequest);
            }
            doctor.setDaySchedules(schedule);
            boolean createSchedule = false;
            //Расписание на данный период времени нет
            if(doctorInfo.getScheduleResponse().isEmpty()){
                createSchedule = true;
            }
            Doctor updateDoctor = doctorDao.update(doctor,startDate,endDate,createSchedule);
            Doctor response = doctorDao.getById(updateDoctor.getId());

            return getDoctorResponse(response,null);
        }
        return null;
    }

    //Детальное расписание для врача. Время приема одинаковое.
    private static List<DaySchedule> getScheduleWithSameTimeReception(Doctor doctor, DoctorRegisterRequest request){
        List<DaySchedule> result = new ArrayList<>();
        LocalDate startDate =  LocalDate.parse(request.getDateStart());
        LocalDate endDate =  LocalDate.parse(request.getDateEnd());
        List<LocalDate> receptionDates = getDatesReception(request.getWeekSchedule().getWeekDays(),
                startDate,endDate);
        int duration = request.getDuration();
        for (LocalDate date: receptionDates){
            DaySchedule daySchedule = new DaySchedule(0,doctor,date);
            List<Reception> receptions = new ArrayList<>();
            LocalTime endTime = LocalTime.parse(request.getWeekSchedule().getTimeEnd());
            for (LocalTime time = LocalTime.parse(request.getWeekSchedule().getTimeStart());!time.equals(endTime);
                 time = time.plusMinutes(duration)){
                Reception reception = new Reception(0,null,daySchedule,time,duration,Status.FREE);
                receptions.add(reception);
            }
            daySchedule.setReceptions(receptions);
            result.add(daySchedule);
        }
        return result;
    }

    //Детальное расписание для врача. Время приема в разные дни разное.
    private static List<DaySchedule> getScheduleWithDifferentTimeReception(Doctor doctor,DoctorRegisterRequest request){
        List<DaySchedule> result = new ArrayList<>();
        List<String> weekdays = new ArrayList<>();
        for(DayScheduleRequest day: request.getWeekDaysSchedule()){
            weekdays.add(day.getWeekDay());
        }
        for (LocalDate date = LocalDate.parse(request.getDateStart());!date.equals(LocalDate.parse(request.getDateEnd()));
                date = date.plusDays(1)){
            String weekday = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if(weekdays.contains(weekday)){
                DaySchedule daySchedule = new DaySchedule(0, doctor, date);
                List<Reception> receptions = new ArrayList<>();
                for (LocalTime time = request.getTimeStart(weekday);!time.equals(request.getTimeEnd(weekday));
                     time = time.plusMinutes(request.getDuration())){
                    Reception reception = new Reception(0, null, daySchedule, time, request.getDuration(),Status.FREE);
                    receptions.add(reception);
                }
                daySchedule.setReceptions(receptions);
                result.add(daySchedule);
            }
        }
        return result;
    }

    private static DoctorRegisterResponse getDoctorResponse(Doctor doctor,Patient patient){
        if (doctor.getDaySchedules() != null) {
            List<ScheduleResponse> schedule =  new ArrayList<>();
            for (DaySchedule day : doctor.getDaySchedules()){
                List<DayScheduleResponse> daySchedule = new ArrayList<>();
                for(Reception reception : day.getReceptions()){
                    DayScheduleResponse dayResponse;
                    if (reception.getTicket() == null){
                        dayResponse = new DayScheduleResponse(reception.getTime().toString(),null);
                    }else{
                        PatientRegisterUpdateResponse patientResponse = new PatientRegisterUpdateResponse();
                        if (reception.getTicket().getPatient().getId() == patient.getId()) {
                            patientResponse = new PatientRegisterUpdateResponse(patient.getId(), patient.getFirstName(),
                                    patient.getLastName(), patient.getPatronymic(), patient.getEmail(), patient.getAddress(),
                                    patient.getPhone());
                        }
                        dayResponse = new DayScheduleResponse(reception.getTime().toString(),patientResponse);
                    }
                    daySchedule.add(dayResponse);
                }
                schedule.add(new ScheduleResponse(day.getDate().toString(),daySchedule));
            }
            return new DoctorRegisterResponse(doctor.getId(),doctor.getFirstName(),doctor.getLastName(),doctor.getPatronymic(),
                    doctor.getSpeciality().getName(),doctor.getRoom().getName(),schedule);
        }else{
            return new DoctorRegisterResponse(doctor.getId(),doctor.getFirstName(),doctor.getLastName(),doctor.getPatronymic(),
                    doctor.getSpeciality().getName(),doctor.getRoom().getName(),null);
        }
    }

    private static List<LocalDate> getDatesReception(List<String> weekDays,LocalDate startDate, LocalDate endDate){
        List<LocalDate> datesReceptions = new ArrayList<>();
        if(weekDays == null || weekDays.isEmpty()){
            weekDays = new ArrayList<>(5);
            Collections.addAll(weekDays,"Mon", "Tue", "Wed", "Thu", "Fri");
        }
        for(LocalDate date = LocalDate.parse(startDate.toString());!date.equals(endDate);date = date.plusDays(1)){
            String weekday = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if(weekDays.contains(weekday)) {
                datesReceptions.add(date);
            }
        }
        return datesReceptions;
    }

    private boolean mayUpdate(List<ScheduleResponse> scheduleResponses){
        if(scheduleResponses != null){
            for (ScheduleResponse scheduleResponse : scheduleResponses){
                for (DayScheduleResponse day : scheduleResponse.getDaySchedule()){
                    if (day.getPatient()!=null){
                        return false;
                    }
                }
            }
        }
        return true;
    }

}
