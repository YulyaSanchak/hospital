package net.thumbtack.school.service;

import com.fasterxml.jackson.databind.ser.Serializers;
import net.thumbtack.school.containers.statistics.DoctorStatistic;
import net.thumbtack.school.containers.statistics.PatientStatistic;
import net.thumbtack.school.containers.statistics.Statistic;
import net.thumbtack.school.dao.SpecialityDao;
import net.thumbtack.school.dao.StatisticsDao;
import net.thumbtack.school.dao.UserDao;
import net.thumbtack.school.daoImpl.SpecialityDaoImpl;
import net.thumbtack.school.daoImpl.StatisticsDaoImpl;
import net.thumbtack.school.daoImpl.UserDaoImpl;
import net.thumbtack.school.dto.filters.StatisticTypeFilter;
import net.thumbtack.school.dto.responses.statistics.*;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.exceptions.ErrorCode;
import net.thumbtack.school.exceptions.ErrorItem;
import net.thumbtack.school.model.Speciality;
import net.thumbtack.school.model.Status;
import net.thumbtack.school.model.TicketType;
import net.thumbtack.school.model.UserType;
import net.thumbtack.school.model.users.User;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;

@Service
public class StatisticsService {
    private SpecialityDao specialityDao = new SpecialityDaoImpl();
    private StatisticsDao statisticsDao = new StatisticsDaoImpl();
    private UserDao userDao = new UserDaoImpl();

    public StatisticResponse getStatistics(String cookie, String filter,String specialityName, String startDate,
                                                      String endDate) throws BaseException {
        User user = userDao.getByCookie(cookie);
        if(user.getUserType()!= UserType.ADMIN){
            throw new BaseException(new ErrorItem(ErrorCode.ACCESS_ERROR,user.getUserType().name()));
        }

        LocalDate dateStart = LocalDate.parse(startDate);
        LocalDate dateEnd = LocalDate.parse(endDate);
        StatisticTypeFilter  statisticFilter = StatisticTypeFilter.valueOf(filter);
        switch (statisticFilter){
            case ALL:{
                return getAllStatistics(dateStart,dateEnd);
            }
            case DOCTORS:{
                Speciality speciality = null;
                if(specialityName!=null){
                    speciality = specialityDao.getByName(specialityName);
                }
                return getDoctorStatistics(dateStart,dateEnd,speciality);
            }
            case PATIENTS:{
                return getPatientStatistics(dateStart,dateEnd);
            }
        }
        return  null;
    }

    private StatisticResponse getAllStatistics(LocalDate startDate, LocalDate endDate){
        int quantityPatients = statisticsDao.getAllPatientsQuantity(startDate,endDate);
        int quantityReceptions = statisticsDao.getAllReceptionsQuantity(startDate, endDate);
        List<Statistic> receptionsStatus = statisticsDao.getAllReceptionsWithStatusQuantity(startDate,endDate);
        List<QuantityStatusResponse> receptionsStatusQuantity = new ArrayList<>();
        for(Statistic statistic: receptionsStatus){
            receptionsStatusQuantity.add(new QuantityStatusResponse(statistic.getStatus(), (statistic.getQuantity())));
        }
        int quantityCommissions = statisticsDao.getAllCommissionsQuantity(startDate,endDate);
        int quantityDoctorsInCommissions = statisticsDao.getDoctorsInCommissionsQuantity(startDate, endDate);
        int quantityDoctorsInReceptions = statisticsDao.getDoctorsInReceptions(startDate, endDate);
        return new StatisticResponse(startDate.toString(),endDate.toString(),null,quantityPatients,
                quantityReceptions,receptionsStatusQuantity,quantityCommissions,quantityDoctorsInCommissions,quantityDoctorsInReceptions);
    }

    private StatisticResponse getDoctorStatistics(LocalDate startDate, LocalDate endDate, Speciality speciality){
        Map<Integer, DoctorStatisticsResponse> resultMap = new HashMap<>();
        List<DoctorStatisticsResponse> result = new ArrayList<>();
        Integer specialityId;
        String specialityName;
        if (speciality == null){
            specialityId = null;
            specialityName = null;
        }else {
            specialityId = speciality.getId();
            specialityName = speciality.getName();
        }
        List<DoctorStatistic> quantityPatients = statisticsDao.getPatientsQuantity(specialityId, startDate, endDate);
        for (DoctorStatistic statistic: quantityPatients){
            resultMap.put(statistic.getDoctorId(),new DoctorStatisticsResponse(statistic.getDoctorId(),statistic.getFirstName(),
                    statistic.getLastName(),statistic.getPatronymic(),statistic.getSpecialityName(),statistic.getQuantity(),0,0,0, new ArrayList<>()));
        }
        List<DoctorStatistic> quantityWorkingDays = statisticsDao.getWorkingDaysQuantity(specialityId,startDate,endDate);
        List<DoctorStatistic> quantityReceptions = statisticsDao.getReceptionsQuantity(specialityId, startDate, endDate);
        List<DoctorStatistic> quantityCommissions = statisticsDao.getCommissionsQuantity(specialityId,startDate,endDate);
        List<DoctorStatistic> quantityFreeReceptions = statisticsDao.getReceptionsQuantityByStatus(specialityId,startDate,endDate, Status.FREE);
        List<DoctorStatistic> quantityTicketReceptions = statisticsDao.getReceptionsQuantityByStatus(specialityId,startDate,endDate, Status.TICKET);
        List<DoctorStatistic> quantityBusyReceptions = statisticsDao.getReceptionsQuantityByStatus(specialityId,startDate,endDate,Status.BUSY);
        for(int i = 0; i<quantityPatients.size();i++){
            resultMap.get(quantityWorkingDays.get(i).getDoctorId())
                    .setQuantityWorkingDays(quantityWorkingDays.get(i).getQuantity());
            resultMap.get(quantityReceptions.get(i).getDoctorId())
                    .setQuantityReceptions(quantityReceptions.get(i).getQuantity());
            resultMap.get(quantityFreeReceptions.get(i).getDoctorId())
                    .addQuantityStatusReceptions(new QuantityStatusResponse(Status.FREE.toString(),quantityFreeReceptions.get(i).getQuantity()));
            resultMap.get(quantityTicketReceptions.get(i).getDoctorId())
                    .addQuantityStatusReceptions(new QuantityStatusResponse(Status.TICKET.toString(),quantityTicketReceptions.get(i).getQuantity()));
            resultMap.get(quantityBusyReceptions.get(i).getDoctorId())
                    .addQuantityStatusReceptions(new QuantityStatusResponse(Status.BUSY.toString(),quantityBusyReceptions.get(i).getQuantity()));
            resultMap.get(quantityCommissions.get(i).getDoctorId())
                    .setQuantityCommissions(quantityCommissions.get(i).getQuantity());

        }
        result.addAll(resultMap.values());

        return new StatisticResponse(startDate.toString(),endDate.toString(),specialityName,result);

    }

    private StatisticResponse getPatientStatistics(LocalDate startDate, LocalDate endDate){
        Map<Integer, PatientStatisticsResponse> resultMap = new HashMap<>();
        List<PatientStatisticsResponse> result = new ArrayList<>();
        List<PatientStatistic> quantityTickets = statisticsDao.getTicketPatientsQuantity(startDate,endDate);
        for (PatientStatistic statistic: quantityTickets){
            resultMap.put(statistic.getPatientId(),new PatientStatisticsResponse(statistic.getPatientId(),statistic.getFirstName(),
                    statistic.getLastName(),statistic.getPatronymic(),statistic.getQuantity(),new ArrayList<QuantityTicketTypeResponse>()));
        }
        List<PatientStatistic> receptionsTickets = statisticsDao.getTicketPatientsQuantityByType( startDate, endDate, TicketType.RECEPTION);
        List<PatientStatistic> commissionsTickets = statisticsDao.getTicketPatientsQuantityByType( startDate, endDate, TicketType.COMMISSION);
        for(int i = 0; i<quantityTickets.size();i++) {
            resultMap.get(receptionsTickets.get(i).getPatientId()).addQuantityTicketType(new QuantityTicketTypeResponse(TicketType.RECEPTION.toString(), receptionsTickets.get(i).getQuantity()));
            resultMap.get(commissionsTickets.get(i).getPatientId()).addQuantityTicketType(new QuantityTicketTypeResponse(TicketType.COMMISSION.toString(), commissionsTickets.get(i).getQuantity()));
        }
        result.addAll(resultMap.values());

        return new StatisticResponse(startDate.toString(),endDate.toString(),result);

    }
}
