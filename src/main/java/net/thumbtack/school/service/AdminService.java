package net.thumbtack.school.service;

import net.thumbtack.school.dao.AdminDao;
import net.thumbtack.school.dao.UserDao;
import net.thumbtack.school.daoImpl.AdminDaoImpl;
import net.thumbtack.school.daoImpl.UserDaoImpl;
import net.thumbtack.school.dto.requests.register_update_admin.AdminRegisterRequest;
import net.thumbtack.school.dto.requests.register_update_admin.AdminUpdateRequest;
import net.thumbtack.school.dto.responses.AdminRegisterResponse;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.exceptions.ErrorCode;
import net.thumbtack.school.exceptions.ErrorItem;
import net.thumbtack.school.model.UserType;
import net.thumbtack.school.model.users.Admin;
import net.thumbtack.school.model.users.User;
import org.springframework.stereotype.Service;

@Service
public class AdminService {
    private AdminDao adminDao = new AdminDaoImpl();
    private UserDao userDao = new UserDaoImpl();

    public AdminRegisterResponse insert(String cookie,AdminRegisterRequest request) throws BaseException {
        User user = userDao.getByCookie(cookie);
        if(user.getUserType() != UserType.ADMIN){
            throw new BaseException(new ErrorItem(ErrorCode.ACCESS_ERROR,user.getUserType().name()));
        }
        Admin admin = new Admin(0,0,request.getFirstName(),request.getLastName(),request.getPatronymic()
                ,request.getLogin(),request.getPassword(),request.getPosition());
        adminDao.insert(admin);
        return new AdminRegisterResponse(admin.getId(),admin.getFirstName(),admin.getLastName(),admin.getPatronymic()
                ,admin.getPosition());
    }

    public AdminRegisterResponse update(String cookie, AdminUpdateRequest request) throws BaseException {
        BaseException base = new BaseException();
        User user =  userDao.getByCookie(cookie);
        if(user.getUserType()!=UserType.ADMIN){
            throw new BaseException(new ErrorItem(ErrorCode.ACCESS_ERROR,user.getUserType().name()));
        }
        Admin admin = adminDao.getByCookie(cookie);
        if(!user.getLogin().equals(admin.getLogin())){
            base.addError(new ErrorItem(ErrorCode.INVALID_LOGIN,user.getLogin()));
        }
        if(!admin.getPassword().equals(request.getOldPassword())) {
            base.addError(new ErrorItem(ErrorCode.INVALID_PASSWORD,request.getOldPassword()));
        }
        if (base.getErrors().size() != 0) {
            throw base;
        }
        admin.setFirstName(request.getFirstName());
        admin.setLastName(request.getLastName());
        admin.setPatronymic(request.getPatronymic());
        admin.setPosition(request.getPosition());
        admin.setPassword(request.getNewPassword());

        Admin updatedAdmin =  adminDao.update(admin);
        return new AdminRegisterResponse(updatedAdmin.getId(),updatedAdmin .getFirstName(), updatedAdmin.getLastName(),
                updatedAdmin.getPatronymic(),updatedAdmin.getPosition());
    }
}
