package net.thumbtack.school.service;

import net.thumbtack.school.dao.*;
import net.thumbtack.school.daoImpl.*;
import net.thumbtack.school.dto.requests.LoginDtoRequest;
import net.thumbtack.school.dto.responses.*;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.exceptions.ErrorCode;
import net.thumbtack.school.exceptions.ErrorItem;
import net.thumbtack.school.model.DaySchedule;
import net.thumbtack.school.model.Reception;
import net.thumbtack.school.model.Session;
import net.thumbtack.school.model.users.Admin;
import net.thumbtack.school.model.users.Doctor;
import net.thumbtack.school.model.users.Patient;
import net.thumbtack.school.model.users.User;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class SessionService {
    private SessionDao sessionDao =  new SessionDaoImpl();
    private UserDao userDao = new UserDaoImpl();
    private AdminDao adminDao =  new AdminDaoImpl();
    private DoctorDao doctorDao =  new DoctorDaoImpl();
    private PatientDao patientDao = new PatientDaoImpl();

    public ImmutablePair<LoginDtoResponse, Session> login(LoginDtoRequest request) throws BaseException {

        BaseException base = new BaseException();
        User user = userDao.getByLogin(request.getLogin());

        if(!user.getPassword().equals(request.getPassword())) {
            throw new BaseException(new ErrorItem(ErrorCode.INVALID_PASSWORD,request.getPassword()));
        }
        Session session = sessionDao.insert(user);
        LoginDtoResponse response = null;
        switch (user.getUserType()){
            case ADMIN: {
                response = createAdminInfoDtoResponse(user.getUserId(), base);
                break;
            }
            case DOCTOR:{
                response = createDoctorInfoDtoResponse(user.getUserId(), base);
                break;
            }
            case PATIENT:{
                response = createPatientInfoDtoResponse(user.getUserId(), base);
                break;
            }
        }
        return new ImmutablePair<>(response, session);
    }

    public EmptySuccessResponse logout(String cookie) throws BaseException {
        sessionDao.delete(cookie);
        return new EmptySuccessResponse();
    }

    public ImmutablePair<LoginDtoResponse, Session> account(String cookie) throws BaseException {
        BaseException base = new BaseException();
        User user = userDao.getByCookie(cookie);

        Session session = null;
        try {
            session = sessionDao.getByUserId(user.getUserId());
        } catch (BaseException e) {
            base.addErrors(e.getErrors());
        }
        LoginDtoResponse response = null;
        switch (user.getUserType()){
            case ADMIN : {
                response = createAdminInfoDtoResponse(user.getUserId(), base);
                break;
            }
            case DOCTOR:{
                response = createDoctorInfoDtoResponse(user.getUserId(), base);
                break;
            }
            case PATIENT:{
                response = createPatientInfoDtoResponse(user.getUserId(), base);
                break;
            }
        }
        return new ImmutablePair<>(response, session);

    }

    private LoginDtoResponse createAdminInfoDtoResponse(int userId, BaseException base) throws BaseException {
        Admin admin;
        try {
            admin = adminDao.getByUserId(userId);
        } catch (BaseException e) {
            base.addErrors(e.getErrors());
            throw base;
        }
        return new LoginDtoResponse(admin.getFirstName(), admin.getLastName(), admin.getPatronymic(), admin.getPosition());
    }

    private LoginDtoResponse createDoctorInfoDtoResponse(int userId, BaseException base) throws BaseException {
        Doctor doctor = null;
        try {
            doctor = doctorDao.getByUserId(userId);
        } catch (BaseException e) {
            base.addErrors(e.getErrors());
            throw base;
        }
        List<DaySchedule> schedule = doctor.getDaySchedules();
        List<ScheduleResponse> scheduleResponse = new ArrayList<>();
        for(DaySchedule day : schedule){
            List<Reception> receptions = day.getReceptions();

            List<DayScheduleResponse> daySchedule =  new ArrayList<>();
            for(Reception reception : receptions){
                DayScheduleResponse dayScheduleResponse;
                if (reception.getTicket()==null){
                    dayScheduleResponse = new DayScheduleResponse(reception.getTime().toString(),new PatientRegisterUpdateResponse());
                }
                else {
                    Patient patient = reception.getTicket().getPatient();
                    PatientRegisterUpdateResponse patientRegisterUpdateResponse = new PatientRegisterUpdateResponse(
                            patient.getId(),patient.getFirstName(),patient.getLastName(), patient.getPatronymic(),
                            patient.getEmail(),patient.getAddress(),patient.getPhone());
                    dayScheduleResponse = new DayScheduleResponse(reception.getTime().toString(),patientRegisterUpdateResponse );
                }
                daySchedule.add(dayScheduleResponse);
            }
            scheduleResponse.add(new ScheduleResponse(day.getDate().toString(),daySchedule));
        }
        return new LoginDtoResponse(doctor.getFirstName(), doctor.getLastName(), doctor.getPatronymic(),
                doctor.getSpeciality().getName(),doctor.getRoom().getName(),scheduleResponse);
    }

    private LoginDtoResponse createPatientInfoDtoResponse(int userId, BaseException base) throws BaseException {
        Patient patient = null;
        try {
            patient = patientDao.getByUserId(userId);
        } catch (BaseException e) {
            base.addErrors(e.getErrors());
            throw base;
        }
        return new LoginDtoResponse(patient.getFirstName(),patient.getLastName(),patient.getPatronymic()
                ,patient.getEmail(),patient.getAddress(),patient.getPhone());
    }

}
