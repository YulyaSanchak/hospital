package net.thumbtack.school.service;

import net.thumbtack.school.dao.UserDao;
import net.thumbtack.school.daoImpl.UserDaoImpl;
import net.thumbtack.school.dto.responses.settings.AdminGetSettingsResponse;
import net.thumbtack.school.dto.responses.settings.DoctorGetSettingsResponse;
import net.thumbtack.school.dto.responses.settings.GetSettingsResponse;
import net.thumbtack.school.dto.responses.settings.PatientGetSettingsResponse;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.model.users.User;
import net.thumbtack.school.utils.ConfigUtils;
import org.springframework.stereotype.Service;

@Service
public class SettingService {
    private UserDao userDao = new UserDaoImpl();
    public GetSettingsResponse getSettings(String cookie) throws BaseException {
        if (cookie == null){
            return new GetSettingsResponse(ConfigUtils.getMaxNameLength(), ConfigUtils.getMinPasswordLength());
        }
        User user = userDao.getByCookie(cookie);
        switch (user.getUserType()){
            case PATIENT:{
                return new PatientGetSettingsResponse(ConfigUtils.getMaxNameLength(),ConfigUtils.getMinPasswordLength());
            }
            case ADMIN:{
                return new AdminGetSettingsResponse(ConfigUtils.getMaxNameLength(),ConfigUtils.getMinPasswordLength());
            }
            case DOCTOR:{
                return new DoctorGetSettingsResponse(ConfigUtils.getMaxNameLength(),ConfigUtils.getMinPasswordLength());
            }
        }
        return new GetSettingsResponse(ConfigUtils.getMaxNameLength(), ConfigUtils.getMinPasswordLength());
    }
}
