package net.thumbtack.school.service;

import net.thumbtack.school.dao.PatientDao;
import net.thumbtack.school.dao.SessionDao;
import net.thumbtack.school.dao.UserDao;
import net.thumbtack.school.daoImpl.PatientDaoImpl;
import net.thumbtack.school.daoImpl.SessionDaoImpl;
import net.thumbtack.school.daoImpl.UserDaoImpl;
import net.thumbtack.school.dto.requests.register_update_patient.PatientRegisterRequest;
import net.thumbtack.school.dto.requests.register_update_patient.PatientUpdateRequest;
import net.thumbtack.school.dto.responses.PatientRegisterUpdateResponse;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.exceptions.ErrorCode;
import net.thumbtack.school.exceptions.ErrorItem;
import net.thumbtack.school.model.Session;
import net.thumbtack.school.model.UserType;
import net.thumbtack.school.model.users.Patient;
import net.thumbtack.school.model.users.User;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.stereotype.Service;

@Service
public class PatientService {
    private PatientDao patientDao = new PatientDaoImpl();
    private UserDao userDao = new UserDaoImpl();
    private SessionDao sessionDao = new SessionDaoImpl();

    public ImmutablePair<PatientRegisterUpdateResponse, Session> insert(PatientRegisterRequest request) throws BaseException {
        Patient patient = new Patient(request.getFirstName(), request.getLastName(), request.getPatronymic(), request.getEmail(),request.getAddress(),request.getPhone().replace("-", ""),
                request.getLogin().toLowerCase(), request.getPassword() );
        ImmutablePair<Patient, Session> pair = patientDao.insert(patient);
        patient = pair.getLeft();
        return new ImmutablePair<>(new PatientRegisterUpdateResponse(patient.getId(),patient.getFirstName()
                , patient.getLastName(), patient.getPatronymic(),patient.getEmail(),patient.getAddress(),patient.getPhone()),
                pair.getRight());
    }

    public ImmutablePair<PatientRegisterUpdateResponse, Session> getById(String cookie,int patientId) throws BaseException {
        User user = userDao.getByCookie(cookie);
        if(user.getUserType() != UserType.ADMIN && user.getUserType() != UserType.DOCTOR){
            throw new BaseException(new ErrorItem(ErrorCode.ACCESS_ERROR,user.getUserType().name()));
        }
        Patient patient = patientDao.getById(patientId);
        Session session = sessionDao.getByUserId(patient.getUserId());
        return new ImmutablePair<>(new PatientRegisterUpdateResponse(patient.getId(),patient.getFirstName()
                , patient.getLastName(), patient.getPatronymic(),patient.getEmail(),patient.getAddress(),patient.getPhone()),
                session);
    }

    public ImmutablePair<PatientRegisterUpdateResponse, Session> update(String cookie, PatientUpdateRequest request) throws BaseException {
        BaseException base = new BaseException();
        User user = userDao.getByCookie(cookie);
        if(user.getUserType()!=UserType.PATIENT){
            base.addError(new ErrorItem(ErrorCode.ACCESS_ERROR,user.getUserType().name()));
            throw base;
        }
        Patient patient = patientDao.getByUserId(user.getUserId());
        if(!patient.getLogin().equals(user.getLogin())){
            base.addError(new ErrorItem(ErrorCode.INVALID_LOGIN,user.getLogin()));
        }
        if (!(patient.getPassword().equals(user.getPassword()))){
            base.addError(new ErrorItem(ErrorCode.INVALID_PASSWORD,user.getPassword()));
        }
        if(base.getErrors().size()!=0){
            throw base;
        }
        patient.setFirstName(request.getFirstName());
        patient.setLastName(request.getLastName());
        patient.setPatronymic(request.getPatronymic());
        patient.setEmail(request.getEmail());
        patient.setAddress(request.getAddress());
        patient.setPhone(request.getPhone());
        patient.setPassword(request.getNewPassword());
        Session session = sessionDao.getByUserId(user.getUserId());
        Patient updatedPatient = patientDao.update(patient);
        return new ImmutablePair<>(new PatientRegisterUpdateResponse(updatedPatient.getId(),updatedPatient .getFirstName(), updatedPatient.getLastName(),
                updatedPatient.getPatronymic(),updatedPatient.getEmail(),updatedPatient.getAddress(),updatedPatient.getPhone()),session);
    }
}
