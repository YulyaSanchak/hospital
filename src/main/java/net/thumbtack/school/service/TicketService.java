package net.thumbtack.school.service;

import net.thumbtack.school.containers.DurationReception;
import net.thumbtack.school.dao.*;
import net.thumbtack.school.daoImpl.*;
import net.thumbtack.school.dto.requests.CommissionRequest;
import net.thumbtack.school.dto.requests.CreateTicketRequest;
import net.thumbtack.school.dto.responses.*;
import net.thumbtack.school.dto.responses.tickets.AllTicketsResponse;
import net.thumbtack.school.dto.responses.tickets.CommissionResponse;
import net.thumbtack.school.dto.responses.tickets.CommissionTicketsResponse;
import net.thumbtack.school.dto.responses.tickets.ReceptionTicketResponse;
import net.thumbtack.school.exceptions.BaseException;
import net.thumbtack.school.exceptions.ErrorCode;
import net.thumbtack.school.exceptions.ErrorItem;
import net.thumbtack.school.model.*;
import net.thumbtack.school.model.users.Doctor;
import net.thumbtack.school.model.users.Patient;
import net.thumbtack.school.model.users.User;
import org.springframework.stereotype.Service;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class TicketService {
    private UserDao userDao = new UserDaoImpl();
    private DoctorDao doctorDao = new DoctorDaoImpl();
    private PatientDao patientDao = new PatientDaoImpl();
    private TicketDao ticketDao = new TicketDaoImpl();
    private ReceptionDao receptionDao = new ReceptionDaoImpl();
    private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("ddMMYYYYHHmm");
    private SpecialityDao specialityDao = new SpecialityDaoImpl();
    private CommissionDao commissionDao = new CommissionDaoImpl();

    public ReceptionTicketResponse createTicketReception(String cookie, CreateTicketRequest request) throws BaseException {
        User user = userDao.getByCookie(cookie);
        if(user.getUserType()!= UserType.PATIENT){
            throw new BaseException(new ErrorItem(ErrorCode.ACCESS_ERROR,user.getUserType().name()));
        }
        Patient patient = patientDao.getByUserId(user.getUserId());
        LocalDate date = LocalDate.parse(request.getDate());
        LocalTime time = LocalTime.parse(request.getTime());
        if(date.getDayOfWeek().equals(DayOfWeek.SATURDAY) || date.getDayOfWeek().equals(DayOfWeek.SUNDAY)||
                Period.between(LocalDate.now(),date).getMonths()>2){
            throw new BaseException(new ErrorItem(ErrorCode.IMPOSSIBLE_CREATE_TICKET_WITH_DATE,date.toString()));
        }
        Doctor doctor = getDoctorForReception(request,date,time);
        List<Reception> patientsReceptions = receptionDao.getTicketReceptionsPatientByDate(patient.getId(),date,null);
        if(!patientsReceptions.isEmpty()){
            int duration = receptionDao.getDurationReceptionsByDate(doctor.getId(),date);
            isPossiblyCreateTicket(patientsReceptions,time,duration);
        }
        List<Reception> receptionsWithDoctor = receptionDao.getTicketReceptionsPatientByDate(patient.getId(),date,doctor.getId());
        if(!receptionsWithDoctor.isEmpty()){
            throw new BaseException(new ErrorItem(ErrorCode.IMPOSSIBLE_CREATE_TICKET_WITH_DOCTOR,String.valueOf(doctor.getId())));
        }
        receptionDao.updateForTicketReception(doctor.getDaySchedules().get(0).getReceptions().get(0).getId(),null,Status.TICKET);
        String dateTime = LocalDateTime.of(date.getYear(),date.getMonth(),date.getDayOfMonth(),
                time.getHour(),time.getMinute()).format(dtf);
        String ticketNumber="D<"+doctor.getId()+">"+dateTime;
        Ticket ticket = new Ticket(0,ticketNumber,patient, TicketType.RECEPTION);
        ticketDao.insert(ticket,doctor.getDaySchedules().get(0).getReceptions().get(0));
        return new ReceptionTicketResponse(ticketNumber,doctor.getId(),doctor.getFirstName(),doctor.getLastName(),
                doctor.getPatronymic(),doctor.getSpeciality().getName(),doctor.getRoom().getName(),request.getDate(),request.getTime());
    }

    public EmptySuccessResponse cancelTicket(String cookie, String number) throws BaseException {
        User user = userDao.getByCookie(cookie);
        if(user.getUserType()!= UserType.PATIENT &&
                user.getUserType()!= UserType.DOCTOR &&
                user.getUserType()!= UserType.ADMIN){
            throw new BaseException(new ErrorItem(ErrorCode.ACCESS_ERROR,user.getUserType().name()));
        }
        Ticket ticket = ticketDao.getByNumber(number);
        Reception reception = receptionDao.getByTicket(ticket);
        receptionDao.update(reception.getId(),ticket.getId(),Status.FREE);
        return new EmptySuccessResponse();
    }

    public CommissionResponse createTicketCommission(String cookie, CommissionRequest request) throws BaseException {
        User user = userDao.getByCookie(cookie);
        if(user.getUserType()!= UserType.DOCTOR){
            throw new BaseException(new ErrorItem(ErrorCode.ACCESS_ERROR,user.getUserType().name()));
        }
        //TODO взять врача по userId и поместить ид этого врача в request.doctorIds
        LocalDate date = LocalDate.parse(request.getDate());
        LocalTime time = LocalTime.parse(request.getTime());
        if(date.getDayOfWeek().equals(DayOfWeek.SATURDAY) || date.getDayOfWeek().equals(DayOfWeek.SUNDAY)||
                Period.between(LocalDate.now(),date).getMonths()>2){
            throw new BaseException(new ErrorItem(ErrorCode.IMPOSSIBLE_CREATE_TICKET_WITH_DATE,date.toString()));
        }
        Patient patient = patientDao.getById(request.getPatientId());
        List<Reception> patientsReceptions = receptionDao.getTicketReceptionsPatientByDate(patient.getId(),date,null);
        if(!patientsReceptions.isEmpty()){
            isPossiblyCreateTicket(patientsReceptions,time,request.getDuration());
        }
        LocalTime endTime = LocalTime.parse(request.getTime()).plusMinutes(request.getDuration());
        List<Doctor> doctors = doctorDao.getDoctorsForCommission(request.getDoctorIds(),date);
        if(doctors.size() != request.getDoctorIds().size()){
            throw new BaseException(new ErrorItem(ErrorCode.DOCTORS_NOT_FOUND));
        }
        Map<Doctor,List<Reception>> doctorsIdAndReceptions = getReceptionsForCommission(doctors,request);
        List<Integer> idsForCommission = getReceptionsIds(doctorsIdAndReceptions);
        receptionDao.updateForCommission(idsForCommission);

        String ticketNumber = getCommissionTicketNumber(request.getDoctorIds(),date,time);
        Ticket ticket = new  Ticket(0,ticketNumber,patient,TicketType.COMMISSION);
        ticketDao.insert(ticket);

        Commission commission = new Commission(0,LocalDateTime.of(date.getYear(),date.getMonth(),date.getDayOfMonth(),
                time.getHour(),time.getMinute()),request.getDuration(),request.getRoom(),doctors,ticket);
        commissionDao.insert(commission);

        receptionDao.deleteByIds(idsForCommission);

        List<Reception> newReceptions = createReceptionsWithCommission(doctorsIdAndReceptions,time,endTime,ticket);
        receptionDao.batchInsert(newReceptions);
        return new CommissionResponse(ticketNumber,patient.getId(),request.getDoctorIds(),request.getRoom(),
                request.getDate(),request.getTime(),request.getDuration());

    }

    public EmptySuccessResponse cancelCommission(String cookie, String number) throws BaseException {
        User user = userDao.getByCookie(cookie);
        if(user.getUserType()!= UserType.PATIENT){
            throw new BaseException(new ErrorItem(ErrorCode.ACCESS_ERROR,user.getUserType().name()));
        }
        Commission commission = commissionDao.getByTicketNumber(number);

        receptionDao.updateState(commission.getTicket().getId(),Status.BUSY);

        List<Doctor> doctorsAndCommissionReceptions =
                doctorDao.getDoctorsForDeleteCommission(commission.getTime().toLocalDate(),
                        commission.getTicket().getId());

        List<Integer> doctorIds = getDoctorIds(commission.getDoctors());
        List<DurationReception> durationReceptions = receptionDao.getDurationReceptions(doctorIds,commission.getTime().toLocalDate());

        List<Reception> restoredReceptions = getRestoredReceptions(doctorsAndCommissionReceptions,durationReceptions);

        receptionDao.deleteByIds(getReceptionsIds(doctorsAndCommissionReceptions));

        receptionDao.batchInsert(restoredReceptions);

        return new EmptySuccessResponse();

    }

    public List<AllTicketsResponse> getAllTickets(String cookie) throws BaseException {
        User user = userDao.getByCookie(cookie);
        if(user.getUserType()!= UserType.PATIENT){
            throw new BaseException(new ErrorItem(ErrorCode.ACCESS_ERROR,user.getUserType().name()));
        }
        Patient patient = patientDao.getByUserId(user.getUserId());
        List<Ticket> tickets = ticketDao.getByPatient(patient);
        List<Integer> ticketsIds = getTicketIds(tickets);
        List<Reception> receptions = receptionDao.getReceptions(ticketsIds);
        List<Commission> commissions = commissionDao.getCommissions(ticketsIds);
        return getAllTicketResult(receptions,commissions);
    }

    private static List<Reception> createReceptionsWithCommission(Map<Doctor,List<Reception>> receptions,
                                                           LocalTime startCommission,
                                                           LocalTime endCommission,
                                                           Ticket ticket){
        List<Reception> result = new ArrayList<>();
        for (Doctor doctor : receptions.keySet()){
            LocalTime startReceptions = receptions.get(doctor).get(0).getTime();
            LocalTime endReceptions = receptions.get(doctor).get(0).getTime();
            for(int i = 0; i<receptions.get(doctor).size();i++){
                endReceptions =endReceptions.plusMinutes(receptions.get(doctor).get(i).getDuration());
            }
            long duration;
            if(startCommission.compareTo(startReceptions)==0){
                if(endCommission.compareTo(endReceptions)==0){
                    duration = Duration.between(startReceptions,endReceptions).toMinutes();
                    result.add(new Reception(0,ticket,doctor.getDaySchedules().get(0),startReceptions, (int) duration,Status.BUSY));
                }else{
                    duration = Duration.between(startReceptions,endCommission).toMinutes();
                    result.add(new Reception(0,ticket,doctor.getDaySchedules().get(0),startReceptions, (int) duration,Status.BUSY));
                    duration = Duration.between(endCommission,endReceptions).toMinutes();
                    result.add(new Reception(0,ticket,doctor.getDaySchedules().get(0),endCommission, (int) duration,Status.FREE));
                }
            }else{
                if(endCommission.compareTo(endReceptions)==0){
                    duration = Duration.between(startReceptions,startCommission).toMinutes();
                    result.add(new Reception(0,ticket,doctor.getDaySchedules().get(0),startReceptions, (int) duration,Status.FREE));
                    duration = Duration.between(startCommission,endReceptions).toMinutes();
                    result.add(new Reception(0,ticket,doctor.getDaySchedules().get(0),startCommission, (int) duration,Status.BUSY));
                }else{
                    duration = Duration.between(startReceptions,startCommission).toMinutes();
                    result.add(new Reception(0,ticket,doctor.getDaySchedules().get(0),startReceptions, (int) duration,Status.FREE));
                    duration = Duration.between(startCommission,endCommission).toMinutes();
                    result.add(new Reception(0,ticket,doctor.getDaySchedules().get(0),startCommission, (int) duration,Status.BUSY));
                    duration = Duration.between(endCommission,endReceptions).toMinutes();
                    result.add(new Reception(0,ticket,doctor.getDaySchedules().get(0),endCommission, (int) duration,Status.FREE));
                }
            }
        }
        return result;
    }

    private static List<Integer> getDoctorIds(List<Doctor> doctors) {
        List<Integer> doctorIds = new ArrayList<>();
        for(Doctor doctor : doctors){
            doctorIds.add(doctor.getId());
        }
        return doctorIds;
    }

    private static String getCommissionTicketNumber (List<Integer> doctorIds,LocalDate date, LocalTime time){
        StringBuilder result = new StringBuilder("CD");
        String dateTime = LocalDateTime.of(date.getYear(),date.getMonth(),date.getDayOfMonth(),
                time.getHour(),time.getMinute()).format(dtf);
        for (Integer id : doctorIds){
            result.append("<");
            result.append(id);
            result.append(">");
        }
        result.append(dateTime);
        return result.toString();
    }

   private static Map<Doctor,List<Reception>> getReceptionsForCommission(List<Doctor> doctors, CommissionRequest request) throws BaseException {

        LocalTime startTime = LocalTime.parse(request.getTime());
        LocalTime endTime = LocalTime.parse(request.getTime()).plusMinutes(request.getDuration());
        int commissionDuration = request.getDuration();
        Map<Doctor,List<Reception>> result = new HashMap<>();
        for(Doctor doctor: doctors) {
            List<Reception> receptionsForCommission = new ArrayList<>();
            if (doctor.getDaySchedules() != null && !doctor.getDaySchedules().isEmpty()) {
                List<Reception> receptions = doctor.getDaySchedules().get(0).getReceptions();
                long durationFromReception = 0;
                long currentDuration = 0;
                for(int i = 0; i< receptions.size(); i++){
                    if (((receptions.get(i).getTime().compareTo(startTime) <= 0 &&
                            receptions.get(i).getTime().plusMinutes(receptions.get(i).getDuration()).compareTo(startTime) > 0) ||
                            (receptions.get(i).getTime().plusMinutes(commissionDuration).compareTo(endTime) >= 0
                                    && receptions.get(i).getTime().compareTo(endTime) < 0)) && receptions.get(i).getStatus() == Status.FREE) {
                        receptionsForCommission.add(receptions.get(i));
                        if (receptions.get(i).getTime().compareTo(startTime) <= 0 && receptions.get(i).getTime().plusMinutes(receptions.get(i).getDuration()).compareTo(endTime) >= 0) {
                            durationFromReception = Duration.between(startTime, endTime).toMinutes();
                        }
                        if (receptions.get(i).getTime().compareTo(endTime) < 0
                                && receptions.get(i).getTime().plusMinutes(receptions.get(i).getDuration()).compareTo(endTime) >= 0
                                && receptions.get(i).getTime().compareTo(startTime) > 0) {
                            durationFromReception = Duration.between(receptions.get(i).getTime(), endTime).toMinutes();
                        }
                        if (receptions.get(i).getTime().plusMinutes(receptions.get(i).getDuration()).compareTo(endTime) < 0
                                && receptions.get(i).getTime().compareTo(startTime) > 0) {
                            durationFromReception = receptions.get(i).getDuration();
                        }

                        if (receptions.get(i).getTime().compareTo(startTime) <= 0
                                && receptions.get(i).getTime().plusMinutes(receptions.get(i).getDuration()).compareTo(startTime) > 0
                                && receptions.get(i).getTime().plusMinutes(receptions.get(i).getDuration()).compareTo(endTime) < 0) {
                            durationFromReception = Duration.between(startTime, receptions.get(i).getTime().plusMinutes(receptions.get(i).getDuration())).toMinutes();
                        }

                        if (durationFromReception < commissionDuration) {
                            currentDuration += durationFromReception;
                        }
                        if (durationFromReception == commissionDuration) {
                            currentDuration = durationFromReception;
                        }
                    }
                }
                if (currentDuration != commissionDuration) {
                    throw new BaseException(new ErrorItem(ErrorCode.COMMISSION_NOT_CREATE));
                }
                result.put(doctor, receptionsForCommission);
            }
        }
        return result;
    }

   private static List<Integer> getReceptionsIds(List<Doctor> doctors){
        List<Integer> result = new ArrayList<>();
        for(Doctor doctor: doctors){

            for(Reception reception:doctor.getDaySchedules().get(0).getReceptions()){
                result.add(reception.getId());
            }
        }
        return result;
   }

   private static List<Integer> getReceptionsIds(Map<Doctor,List<Reception>> receptions){
        List<Integer> receptionsIds = new ArrayList<>();
        for (Map.Entry<Doctor,List<Reception>> entry : receptions.entrySet()){
            for(Reception reception : entry.getValue()){
                receptionsIds.add(reception.getId());
            }
        }

        return receptionsIds;
   }

   private Doctor getDoctorForReception(CreateTicketRequest request,LocalDate date,LocalTime time) throws BaseException {
        Doctor doctor;
        if (request.getDoctorId()!=null){
            doctor = doctorDao.getDoctorForTicket(request.getDoctorId(),date,time);

        }else{
            Speciality speciality = specialityDao.getByName(request.getSpeciality());
            List<Doctor> doctors = doctorDao.getDoctorsForTicket(speciality,date,time);
            Random random = new Random();
            int index = random.nextInt(doctors.size());
            doctor = doctors.get(index);
        }
        return doctor;
   }

   private static List<Reception> getRestoredReceptions(List<Doctor> doctors, List<DurationReception> durations){
        List<Reception> result = new ArrayList<>();
        Map<Integer,Integer> receptionsDuration = getDurations(durations);
        for (Doctor doctor: doctors){
            List<Reception> receptions = doctor.getDaySchedules().get(0).getReceptions();
            LocalTime startReceptions = doctor.getDaySchedules().get(0).getReceptions().get(0).getTime();
            LocalTime endReceptions = doctor.getDaySchedules().get(0).getReceptions().get(0).getTime();
            int durationReception = receptionsDuration.get(doctor.getId());
            for (int i = 0; i<receptions.size();i++){
                endReceptions= endReceptions.plusMinutes(receptions.get(i).getDuration());
            }
            long duration = Duration.between(startReceptions,endReceptions).toMinutes();
            int n = (int)duration/durationReception;
            LocalTime time = startReceptions;
            for(int i = 0;i<n;i++){
                result.add(new Reception(0,null,doctor.getDaySchedules().get(0),time,durationReception,Status.FREE));
                time = time.plusMinutes(durationReception);
            }
            if (time.compareTo(endReceptions)<0){
                result.add(new Reception(0,null,doctor.getDaySchedules().get(0),time,
                        (int)Duration.between(time,endReceptions).toMinutes(),Status.FREE));
            }
        }
    return result;
   }

   private static Map<Integer,Integer> getDurations(List<DurationReception> durations){
        Map<Integer,Integer> result = new HashMap<>();
        for (DurationReception durationReception: durations){
            int averageDuration = (int)Math.floor(durationReception.getDuration().stream().mapToInt(e->e).average().orElse(0));
            int duration = durationReception.getDuration().stream().filter(e->e>=averageDuration).findFirst().orElse(0);
            result.put(durationReception.getDoctorId(),duration);
        }
        return result;
   }

   private static List<Integer> getTicketIds(List<Ticket> tickets){
        List<Integer> ids = new ArrayList<>();
        for (Ticket ticket: tickets){
            ids.add(ticket.getId());
        }
        return  ids;
   }

   private static List<AllTicketsResponse> getAllTicketResult(List<Reception> receptions, List<Commission> commissions){
        List<AllTicketsResponse> result = new ArrayList<>();
        for (Reception reception : receptions){
            result.add(new AllTicketsResponse(reception.getTicket().getTicketNumber(),
                    reception.getDaySchedule().getDoctor().getRoom().getName(),reception.getDaySchedule().getDate().toString(),
                    reception.getTime().toString(),
                    reception.getDaySchedule().getDoctor().getId(),reception.getDaySchedule().getDoctor().getFirstName(),
                    reception.getDaySchedule().getDoctor().getLastName(),reception.getDaySchedule().getDoctor().getPatronymic(),
                    reception.getDaySchedule().getDoctor().getSpeciality().getName()));
        }
        for (Commission commission: commissions){
            List<CommissionTicketsResponse> commissionTicketsResponses = new ArrayList<>();
            for(Doctor doctor:commission.getDoctors()){
                commissionTicketsResponses.add(new CommissionTicketsResponse(doctor.getId(),doctor.getFirstName(),
                        doctor.getLastName(),doctor.getPatronymic(),doctor.getSpeciality().getName()));
            }
            result.add(new AllTicketsResponse(commission.getTicket().getTicketNumber(),
                    commission.getRoomName(),commission.getTime().toLocalDate().toString(),
                    commission.getTime().toLocalTime().toString(),commissionTicketsResponses));
        }
        return result;
   }

   private void isPossiblyCreateTicket(List<Reception> receptions, LocalTime startTime, int duration)
           throws BaseException {
        LocalTime endTime = startTime.plusMinutes(duration);
        for (Reception reception : receptions) {
            if ((reception.getTime().compareTo(startTime) <= 0 &&
                    reception.getTime().plusMinutes(reception.getDuration()).compareTo(startTime) > 0) ||
                    (reception.getTime().plusMinutes(duration).compareTo(endTime) >= 0
                            && reception.getTime().compareTo(endTime) < 0)) {
                throw new BaseException(new ErrorItem(ErrorCode.IMPOSSIBLE_CREATE_TICKET, startTime.toString()));
            }
        }
    }
}
