package net.thumbtack.school.dto.responses.settings;

public class GetSettingsResponse {
   private int maxNameLength;
   private int minPasswordLength;

    public GetSettingsResponse(int maxNameLength, int minPasswordLength) {
        this.maxNameLength = maxNameLength;
        this.minPasswordLength = minPasswordLength;
    }

    public int getMaxNameLength() {
        return maxNameLength;
    }

    public void setMaxNameLength(int maxNameLength) {
        this.maxNameLength = maxNameLength;
    }

    public int getMinPasswordLength() {
        return minPasswordLength;
    }

    public void setMinPasswordLength(int minPasswordLength) {
        this.minPasswordLength = minPasswordLength;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GetSettingsResponse)) return false;

        GetSettingsResponse that = (GetSettingsResponse) o;

        if (getMaxNameLength() != that.getMaxNameLength()) return false;
        return getMinPasswordLength() == that.getMinPasswordLength();
    }

    @Override
    public int hashCode() {
        int result = getMaxNameLength();
        result = 31 * result + getMinPasswordLength();
        return result;
    }
}
