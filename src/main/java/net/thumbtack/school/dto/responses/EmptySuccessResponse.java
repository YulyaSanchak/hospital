package net.thumbtack.school.dto.responses;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NONE)
public class EmptySuccessResponse {

    public EmptySuccessResponse() {
    }

}
