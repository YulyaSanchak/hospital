package net.thumbtack.school.dto.responses;

public class DayScheduleResponse {
    private String time;
    private PatientRegisterUpdateResponse patient;

    public DayScheduleResponse() {
    }

    public DayScheduleResponse(String time, PatientRegisterUpdateResponse patient) {
        this.time = time;
        this.patient = patient;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public PatientRegisterUpdateResponse getPatient() {
        return patient;
    }

    public void setPatient(PatientRegisterUpdateResponse patient) {
        this.patient = patient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DayScheduleResponse)) return false;

        DayScheduleResponse that = (DayScheduleResponse) o;

        if (getTime() != null ? !getTime().equals(that.getTime()) : that.getTime() != null) return false;
        return getPatient() != null ? getPatient().equals(that.getPatient()) : that.getPatient() == null;
    }

    @Override
    public int hashCode() {
        int result = getTime() != null ? getTime().hashCode() : 0;
        result = 31 * result + (getPatient() != null ? getPatient().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DayScheduleResponse{" +
                "time='" + time + '\'' +
                ", patient=" + patient +
                '}';
    }
}
