package net.thumbtack.school.dto.responses;

import java.util.List;

public class DoctorRegisterResponse {
    private int id;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String speciality;
    private String room;
    private List<ScheduleResponse> scheduleResponse;

    public DoctorRegisterResponse() {
    }

    public DoctorRegisterResponse(int id, String firstName, String lastName, String patronymic, String speciality
            , String room, List<ScheduleResponse> scheduleResponse) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.speciality = speciality;
        this.room = room;
        this.scheduleResponse = scheduleResponse;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public List<ScheduleResponse> getScheduleResponse() {
        return scheduleResponse;
    }

    public void setScheduleResponse(List<ScheduleResponse> scheduleResponse) {
        this.scheduleResponse = scheduleResponse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DoctorRegisterResponse)) return false;

        DoctorRegisterResponse that = (DoctorRegisterResponse) o;

        if (getId() != that.getId()) return false;
        if (getFirstName() != null ? !getFirstName().equals(that.getFirstName()) : that.getFirstName() != null)
            return false;
        if (getLastName() != null ? !getLastName().equals(that.getLastName()) : that.getLastName() != null)
            return false;
        if (getPatronymic() != null ? !getPatronymic().equals(that.getPatronymic()) : that.getPatronymic() != null)
            return false;
        if (getSpeciality() != null ? !getSpeciality().equals(that.getSpeciality()) : that.getSpeciality() != null)
            return false;
        if (getRoom() != null ? !getRoom().equals(that.getRoom()) : that.getRoom() != null) return false;
        return getScheduleResponse() != null ? getScheduleResponse().equals(that.getScheduleResponse()) : that.getScheduleResponse() == null;
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getFirstName() != null ? getFirstName().hashCode() : 0);
        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
        result = 31 * result + (getPatronymic() != null ? getPatronymic().hashCode() : 0);
        result = 31 * result + (getSpeciality() != null ? getSpeciality().hashCode() : 0);
        result = 31 * result + (getRoom() != null ? getRoom().hashCode() : 0);
        result = 31 * result + (getScheduleResponse() != null ? getScheduleResponse().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DoctorRegisterResponse{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", speciality='" + speciality + '\'' +
                ", room='" + room + '\'' +
                ", scheduleResponse=" + scheduleResponse +
                '}';
    }
}
