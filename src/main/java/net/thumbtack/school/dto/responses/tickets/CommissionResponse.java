package net.thumbtack.school.dto.responses.tickets;

import java.util.List;

public class CommissionResponse {
    private String ticket;
    private int patientId;
    private List<Integer> doctorIds;
    private String room;
    private String date;
    private String time;
    private int duration;

    public CommissionResponse(String ticket, int patientId, List<Integer> doctorIds, String room, String date,
                              String time, int duration) {

        this.ticket = ticket;
        this.patientId = patientId;
        this.doctorIds = doctorIds;
        this.room = room;
        this.date = date;
        this.time = time;
        this.duration = duration;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public List<Integer> getDoctorIds() {
        return doctorIds;
    }

    public void setDoctorIds(List<Integer> doctorIds) {
        this.doctorIds = doctorIds;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CommissionResponse)) return false;

        CommissionResponse that = (CommissionResponse) o;

        if (getPatientId() != that.getPatientId()) return false;
        if (getDuration() != that.getDuration()) return false;
        if (getTicket() != null ? !getTicket().equals(that.getTicket()) : that.getTicket() != null) return false;
        if (getDoctorIds() != null ? !getDoctorIds().equals(that.getDoctorIds()) : that.getDoctorIds() != null)
            return false;
        if (getRoom() != null ? !getRoom().equals(that.getRoom()) : that.getRoom() != null) return false;
        if (getDate() != null ? !getDate().equals(that.getDate()) : that.getDate() != null) return false;
        return getTime() != null ? getTime().equals(that.getTime()) : that.getTime() == null;
    }

    @Override
    public int hashCode() {
        int result = getTicket() != null ? getTicket().hashCode() : 0;
        result = 31 * result + getPatientId();
        result = 31 * result + (getDoctorIds() != null ? getDoctorIds().hashCode() : 0);
        result = 31 * result + (getRoom() != null ? getRoom().hashCode() : 0);
        result = 31 * result + (getDate() != null ? getDate().hashCode() : 0);
        result = 31 * result + (getTime() != null ? getTime().hashCode() : 0);
        result = 31 * result + getDuration();
        return result;
    }

    @Override
    public String toString() {
        return "CommissionResponse{" +
                "ticket='" + ticket + '\'' +
                ", patientId=" + patientId +
                ", doctorIds=" + doctorIds +
                ", room='" + room + '\'' +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", duration=" + duration +
                '}';
    }
}
