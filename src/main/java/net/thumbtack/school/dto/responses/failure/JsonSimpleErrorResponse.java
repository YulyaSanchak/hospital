package net.thumbtack.school.dto.responses.failure;


import net.thumbtack.school.exceptions.ErrorItem;
import org.springframework.http.HttpStatus;

public class JsonSimpleErrorResponse {
    private String code;
    private String field;
    private String message;

    public JsonSimpleErrorResponse() {
    }

    public JsonSimpleErrorResponse(ErrorItem e) {
        this.code = e.getErrorCode().name();
        this.field = e.getErrorCode().getField();
        this.message = String.format(e.getErrorCode().getMessage(), e.getFields());
    }

    public JsonSimpleErrorResponse(String field, String message) {
        this.field = field;
        this.message = message;
    }

    public JsonSimpleErrorResponse(String code, String field, String message) {
        this.code = code;
        this.field = field;
        this.message = message;
    }

    public JsonSimpleErrorResponse(HttpStatus code, String field, String message) {
        this.code = code.toString();
        this.field = field;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "JsonSimpleErrorResponse{" +
                "code=" + code +
                ", field='" + field + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
