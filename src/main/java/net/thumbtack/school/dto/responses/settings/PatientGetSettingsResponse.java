package net.thumbtack.school.dto.responses.settings;

import net.thumbtack.school.dto.responses.settings.GetSettingsResponse;

public class PatientGetSettingsResponse extends GetSettingsResponse {

    public PatientGetSettingsResponse(int maxNameLength, int minPasswordLength) {
        super(maxNameLength, minPasswordLength);
    }

}
