package net.thumbtack.school.dto.responses.statistics;

import java.util.List;

public class PatientStatisticsResponse {
    private int patientId;
    private String firstName;
    private String lastName;
    private String patronymic;
    private int quantityTickets;
    private List<QuantityTicketTypeResponse> quantityTicketWithType;

    public PatientStatisticsResponse(int patientId, String firstName, String lastName, String patronymic, int quantityTickets,
                                     List<QuantityTicketTypeResponse> quantityTicketWithType) {
        this.patientId = patientId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.quantityTickets = quantityTickets;
        this.quantityTicketWithType = quantityTicketWithType;
    }

    public PatientStatisticsResponse(int patientId, String firstName, String lastName, String patronymic, int quantityTickets) {
        this.patientId = patientId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.quantityTickets = quantityTickets;
        this.quantityTicketWithType = null;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public int getQuantityTickets() {
        return quantityTickets;
    }

    public void setQuantityTickets(int quantityTickets) {
        this.quantityTickets = quantityTickets;
    }

    public List<QuantityTicketTypeResponse> getQuantityTicketWithType() {
        return quantityTicketWithType;
    }

    public void setQuantityTicketWithType(List<QuantityTicketTypeResponse> quantityTicketWithType) {
        this.quantityTicketWithType = quantityTicketWithType;
    }

    public void addQuantityTicketType(QuantityTicketTypeResponse quantityTicketTypeResponse){
        quantityTicketWithType.add(quantityTicketTypeResponse);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PatientStatisticsResponse)) return false;

        PatientStatisticsResponse that = (PatientStatisticsResponse) o;

        if (getPatientId() != that.getPatientId()) return false;
        if (getQuantityTickets() != that.getQuantityTickets()) return false;
        if (getFirstName() != null ? !getFirstName().equals(that.getFirstName()) : that.getFirstName() != null)
            return false;
        if (getLastName() != null ? !getLastName().equals(that.getLastName()) : that.getLastName() != null)
            return false;
        if (getPatronymic() != null ? !getPatronymic().equals(that.getPatronymic()) : that.getPatronymic() != null)
            return false;
        return getQuantityTicketWithType() != null ? getQuantityTicketWithType().equals(that.getQuantityTicketWithType()) : that.getQuantityTicketWithType() == null;
    }

    @Override
    public int hashCode() {
        int result = getPatientId();
        result = 31 * result + (getFirstName() != null ? getFirstName().hashCode() : 0);
        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
        result = 31 * result + (getPatronymic() != null ? getPatronymic().hashCode() : 0);
        result = 31 * result + getQuantityTickets();
        result = 31 * result + (getQuantityTicketWithType() != null ? getQuantityTicketWithType().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PatientStatisticsResponse{" +
                "patientId=" + patientId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", quantityTickets=" + quantityTickets +
                ", quantityTicketWithType=" + quantityTicketWithType +
                '}';
    }
}
