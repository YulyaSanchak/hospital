package net.thumbtack.school.dto.responses.settings;

public class AdminGetSettingsResponse extends GetSettingsResponse {

    public AdminGetSettingsResponse(int maxNameLength, int minPasswordLength) {
        super(maxNameLength,minPasswordLength);
    }

}
