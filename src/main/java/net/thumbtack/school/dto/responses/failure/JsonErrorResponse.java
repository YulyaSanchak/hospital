package net.thumbtack.school.dto.responses.failure;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class JsonErrorResponse {
    private List<JsonSimpleErrorResponse> jsons;

    public JsonErrorResponse(List<JsonSimpleErrorResponse> jsons) {
        this.jsons = jsons;
    }

    public JsonErrorResponse() {
        jsons = new ArrayList<>();
    }

    public List<JsonSimpleErrorResponse> getJsons() {
        return jsons;
    }

    public void setJsons(List<JsonSimpleErrorResponse> jsons) {
        this.jsons = jsons;
    }

    public void addJson(JsonSimpleErrorResponse json) {
        this.jsons.add(json);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JsonErrorResponse)) return false;
        JsonErrorResponse that = (JsonErrorResponse) o;
        return Objects.equals(getJsons(), that.getJsons());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getJsons());
    }

    @Override
    public String toString() {
        return "JsonErrorResponse{" +
                "jsons=" + jsons +
                '}';
    }
}
