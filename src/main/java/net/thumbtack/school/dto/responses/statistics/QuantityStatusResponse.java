package net.thumbtack.school.dto.responses.statistics;

public class QuantityStatusResponse {
    private String status;
    private int quantity;

    public QuantityStatusResponse(String status, int quantity) {
        this.status = status;
        this.quantity = quantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QuantityStatusResponse)) return false;

        QuantityStatusResponse that = (QuantityStatusResponse) o;

        if (getQuantity() != that.getQuantity()) return false;
        return getStatus() != null ? getStatus().equals(that.getStatus()) : that.getStatus() == null;
    }

    @Override
    public int hashCode() {
        int result = getStatus() != null ? getStatus().hashCode() : 0;
        result = 31 * result + getQuantity();
        return result;
    }

    @Override
    public String toString() {
        return "QuantityStatusResponse{" +
                "status='" + status + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
