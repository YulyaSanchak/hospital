package net.thumbtack.school.dto.responses.tickets;

import java.util.List;

public class AllTicketsResponse {
    private String ticket;
    private String room;
    private String date;
    private String time;

    private int doctorId;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String speciality;

    private List<CommissionTicketsResponse> commissionsDoctors;

    public AllTicketsResponse(String ticket, String room, String date, String time,
                              int doctorId, String firstName, String lastName, String patronymic, String speciality) {
        this.ticket = ticket;
        this.room = room;
        this.date = date;
        this.time = time;
        this.doctorId = doctorId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.speciality = speciality;
        this.commissionsDoctors = null;
    }

    public AllTicketsResponse(String ticket, String room, String date, String time,  List<CommissionTicketsResponse> commissionsDoctors) {
        this.ticket = ticket;
        this.room = room;
        this.date = date;
        this.time = time;
        this.commissionsDoctors = commissionsDoctors;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public List<CommissionTicketsResponse> getCommissionsDoctors() {
        return commissionsDoctors;
    }

    public void setCommissionsDoctors(List<CommissionTicketsResponse> commissionsDoctors) {
        this.commissionsDoctors = commissionsDoctors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AllTicketsResponse)) return false;

        AllTicketsResponse that = (AllTicketsResponse) o;

        if (getDoctorId() != that.getDoctorId()) return false;
        if (getTicket() != null ? !getTicket().equals(that.getTicket()) : that.getTicket() != null) return false;
        if (getRoom() != null ? !getRoom().equals(that.getRoom()) : that.getRoom() != null) return false;
        if (getDate() != null ? !getDate().equals(that.getDate()) : that.getDate() != null) return false;
        if (getTime() != null ? !getTime().equals(that.getTime()) : that.getTime() != null) return false;
        if (getFirstName() != null ? !getFirstName().equals(that.getFirstName()) : that.getFirstName() != null)
            return false;
        if (getLastName() != null ? !getLastName().equals(that.getLastName()) : that.getLastName() != null)
            return false;
        if (getPatronymic() != null ? !getPatronymic().equals(that.getPatronymic()) : that.getPatronymic() != null)
            return false;
        if (getSpeciality() != null ? !getSpeciality().equals(that.getSpeciality()) : that.getSpeciality() != null)
            return false;
        return getCommissionsDoctors() != null ? getCommissionsDoctors().equals(that.getCommissionsDoctors()) : that.getCommissionsDoctors() == null;
    }

    @Override
    public int hashCode() {
        int result = getTicket() != null ? getTicket().hashCode() : 0;
        result = 31 * result + (getRoom() != null ? getRoom().hashCode() : 0);
        result = 31 * result + (getDate() != null ? getDate().hashCode() : 0);
        result = 31 * result + (getTime() != null ? getTime().hashCode() : 0);
        result = 31 * result + getDoctorId();
        result = 31 * result + (getFirstName() != null ? getFirstName().hashCode() : 0);
        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
        result = 31 * result + (getPatronymic() != null ? getPatronymic().hashCode() : 0);
        result = 31 * result + (getSpeciality() != null ? getSpeciality().hashCode() : 0);
        result = 31 * result + (getCommissionsDoctors() != null ? getCommissionsDoctors().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AllTicketsResponse{" +
                "ticket='" + ticket + '\'' +
                ", room='" + room + '\'' +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", doctorId=" + doctorId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", speciality='" + speciality + '\'' +
                ", commissionsDoctors=" + commissionsDoctors +
                '}';
    }
}
