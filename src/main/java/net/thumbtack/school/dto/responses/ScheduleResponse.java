package net.thumbtack.school.dto.responses;

import java.util.List;

public class ScheduleResponse {
    private String date;
    private List<DayScheduleResponse> daySchedule;

    public ScheduleResponse() {
    }

    public ScheduleResponse(String date, List<DayScheduleResponse> daySchedule) {
        this.date = date;
        this.daySchedule = daySchedule;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<DayScheduleResponse> getDaySchedule() {
        return daySchedule;
    }

    public void setDaySchedule(List<DayScheduleResponse> daySchedule) {
        this.daySchedule = daySchedule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ScheduleResponse)) return false;

        ScheduleResponse that = (ScheduleResponse) o;

        if (getDate() != null ? !getDate().equals(that.getDate()) : that.getDate() != null) return false;
        return getDaySchedule() != null ? getDaySchedule().equals(that.getDaySchedule()) : that.getDaySchedule() == null;
    }

    @Override
    public int hashCode() {
        int result = getDate() != null ? getDate().hashCode() : 0;
        result = 31 * result + (getDaySchedule() != null ? getDaySchedule().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ScheduleResponse{" +
                "date='" + date + '\'' +
                ", daySchedule=" + daySchedule +
                '}';
    }
}
