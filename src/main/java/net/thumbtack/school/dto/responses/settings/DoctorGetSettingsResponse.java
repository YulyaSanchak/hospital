package net.thumbtack.school.dto.responses.settings;

public class DoctorGetSettingsResponse extends GetSettingsResponse {

    public DoctorGetSettingsResponse(int maxNameLength, int minPasswordLength) {
       super(maxNameLength, minPasswordLength);
    }

}
