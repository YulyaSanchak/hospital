package net.thumbtack.school.dto.responses.statistics;

import java.util.List;

public class StatisticResponse {
    private String startDate;
    private String endDate;
    private String specialityName;
    private Integer quantityPatients;
    private Integer quantityReceptions;
    private List<QuantityStatusResponse> quantityReceptionsStatus;
    private Integer quantityCommissions;
    private Integer quantityDoctorsInCommissions;
    private Integer quantityDoctorsInReceptions;
    private List<DoctorStatisticsResponse> doctorStatistics;
    private List<PatientStatisticsResponse> patientStatistics;

    public StatisticResponse(String startDate, String endDate, String specialityName, int quantityPatients,
                             int quantityReceptions, List<QuantityStatusResponse> quantityReceptionsStatus,
                             int quantityCommissions, int quantityDoctorsInCommissions, int quantityDoctorsInReceptions) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.specialityName = specialityName;
        this.quantityPatients = quantityPatients;
        this.quantityReceptions = quantityReceptions;
        this.quantityReceptionsStatus = quantityReceptionsStatus;
        this.quantityCommissions = quantityCommissions;
        this.quantityDoctorsInCommissions = quantityDoctorsInCommissions;
        this.quantityDoctorsInReceptions = quantityDoctorsInReceptions;
        doctorStatistics = null;
        patientStatistics = null;
    }

    public StatisticResponse(String startDate, String endDate, String specialityName, List<DoctorStatisticsResponse> doctorStatistics) {

        this.startDate = startDate;
        this.endDate = endDate;
        this.specialityName = specialityName;
        this.quantityPatients = null;
        this.quantityReceptions = null;
        this.quantityReceptionsStatus = null;
        this.quantityCommissions = null;
        this.quantityDoctorsInCommissions =  null;
        this.quantityDoctorsInReceptions = null;
        this.doctorStatistics = doctorStatistics;
        this.patientStatistics = null;
    }

    public StatisticResponse(String startDate, String endDate, List<PatientStatisticsResponse> patientStatistics) {

        this.startDate = startDate;
        this.endDate = endDate;
        this.specialityName = null;
        this.quantityPatients = null;
        this.quantityReceptions = null;
        this.quantityReceptionsStatus = null;
        this.quantityCommissions = null;
        this.quantityDoctorsInCommissions =  null;
        this.quantityDoctorsInReceptions = null;
        this.doctorStatistics = null;
        this.patientStatistics = patientStatistics;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String  startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getSpecialityName() {
        return specialityName;
    }

    public void setSpecialityName(String specialityName) {
        this.specialityName = specialityName;
    }

    public Integer getQuantityPatients() {
        return quantityPatients;
    }

    public void setQuantityPatients(Integer quantityPatients) {
        this.quantityPatients = quantityPatients;
    }

    public Integer getQuantityReceptions() {
        return quantityReceptions;
    }

    public void setQuantityReceptions(Integer quantityReceptions) {
        this.quantityReceptions = quantityReceptions;
    }

    public List<QuantityStatusResponse> getQuantityReceptionsStatus() {
        return quantityReceptionsStatus;
    }

    public void setQuantityReceptionsStatus(List<QuantityStatusResponse> quantityReceptionsStatus) {
        this.quantityReceptionsStatus = quantityReceptionsStatus;
    }

    public Integer getQuantityCommissions() {
        return quantityCommissions;
    }

    public void setQuantityCommissions(Integer quantityCommissions) {
        this.quantityCommissions = quantityCommissions;
    }

    public Integer getQuantityDoctorsInCommissions() {
        return quantityDoctorsInCommissions;
    }

    public void setQuantityDoctorsInCommissions(Integer quantityDoctorsInCommissions) {
        this.quantityDoctorsInCommissions = quantityDoctorsInCommissions;
    }

    public Integer getQuantityDoctorsInReceptions() {
        return quantityDoctorsInReceptions;
    }

    public void setQuantityDoctorsInReceptions(Integer quantityDoctorsInReceptions) {
        this.quantityDoctorsInReceptions = quantityDoctorsInReceptions;
    }

    public List<DoctorStatisticsResponse> getDoctorStatistics() {
        return doctorStatistics;
    }

    public void setDoctorStatistics(List<DoctorStatisticsResponse> doctorStatistics) {
        this.doctorStatistics = doctorStatistics;
    }

    public List<PatientStatisticsResponse> getPatientStatistics() {
        return patientStatistics;
    }

    public void setPatientStatistics(List<PatientStatisticsResponse> patientStatistics) {
        this.patientStatistics = patientStatistics;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StatisticResponse)) return false;

        StatisticResponse that = (StatisticResponse) o;

        if (getStartDate() != null ? !getStartDate().equals(that.getStartDate()) : that.getStartDate() != null)
            return false;
        if (getEndDate() != null ? !getEndDate().equals(that.getEndDate()) : that.getEndDate() != null) return false;
        if (getSpecialityName() != null ? !getSpecialityName().equals(that.getSpecialityName()) : that.getSpecialityName() != null)
            return false;
        if (getQuantityPatients() != null ? !getQuantityPatients().equals(that.getQuantityPatients()) : that.getQuantityPatients() != null)
            return false;
        if (getQuantityReceptions() != null ? !getQuantityReceptions().equals(that.getQuantityReceptions()) : that.getQuantityReceptions() != null)
            return false;
        if (getQuantityReceptionsStatus() != null ? !getQuantityReceptionsStatus().equals(that.getQuantityReceptionsStatus()) : that.getQuantityReceptionsStatus() != null)
            return false;
        if (getQuantityCommissions() != null ? !getQuantityCommissions().equals(that.getQuantityCommissions()) : that.getQuantityCommissions() != null)
            return false;
        if (getQuantityDoctorsInCommissions() != null ? !getQuantityDoctorsInCommissions().equals(that.getQuantityDoctorsInCommissions()) : that.getQuantityDoctorsInCommissions() != null)
            return false;
        if (getQuantityDoctorsInReceptions() != null ? !getQuantityDoctorsInReceptions().equals(that.getQuantityDoctorsInReceptions()) : that.getQuantityDoctorsInReceptions() != null)
            return false;
        if (getDoctorStatistics() != null ? !getDoctorStatistics().equals(that.getDoctorStatistics()) : that.getDoctorStatistics() != null)
            return false;
        return getPatientStatistics() != null ? getPatientStatistics().equals(that.getPatientStatistics()) : that.getPatientStatistics() == null;
    }

    @Override
    public int hashCode() {
        int result = getStartDate() != null ? getStartDate().hashCode() : 0;
        result = 31 * result + (getEndDate() != null ? getEndDate().hashCode() : 0);
        result = 31 * result + (getSpecialityName() != null ? getSpecialityName().hashCode() : 0);
        result = 31 * result + (getQuantityPatients() != null ? getQuantityPatients().hashCode() : 0);
        result = 31 * result + (getQuantityReceptions() != null ? getQuantityReceptions().hashCode() : 0);
        result = 31 * result + (getQuantityReceptionsStatus() != null ? getQuantityReceptionsStatus().hashCode() : 0);
        result = 31 * result + (getQuantityCommissions() != null ? getQuantityCommissions().hashCode() : 0);
        result = 31 * result + (getQuantityDoctorsInCommissions() != null ? getQuantityDoctorsInCommissions().hashCode() : 0);
        result = 31 * result + (getQuantityDoctorsInReceptions() != null ? getQuantityDoctorsInReceptions().hashCode() : 0);
        result = 31 * result + (getDoctorStatistics() != null ? getDoctorStatistics().hashCode() : 0);
        result = 31 * result + (getPatientStatistics() != null ? getPatientStatistics().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "StatisticResponse{" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                ", specialityName='" + specialityName + '\'' +
                ", quantityPatients=" + quantityPatients +
                ", quantityReceptions=" + quantityReceptions +
                ", quantityReceptionsStatus=" + quantityReceptionsStatus +
                ", quantityCommissions=" + quantityCommissions +
                ", quantityDoctorsInCommissions=" + quantityDoctorsInCommissions +
                ", quantityDoctorsInReceptions=" + quantityDoctorsInReceptions +
                ", doctorStatistics=" + doctorStatistics +
                ", patientStatistics=" + patientStatistics +
                '}';
    }
}
