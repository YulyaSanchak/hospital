package net.thumbtack.school.dto.responses.statistics;

import java.util.List;

public class DoctorStatisticsResponse {
    private int doctorId;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String speciality;
    private int quantityPatients;
    private int quantityWorkingDays;
    private int quantityReceptions;
    private int quantityCommissions;
    private List<QuantityStatusResponse> quantityStatusReceptions;

    public DoctorStatisticsResponse(int doctorId, String firstName, String lastName, String patronymic, String speciality,
                                    int quantityPatients, int quantityWorkingDays, int quantityReceptions,
                                    int quantityCommissions, List<QuantityStatusResponse> quantityStatusReceptions) {
        this.doctorId = doctorId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.speciality = speciality;
        this.quantityPatients = quantityPatients;
        this.quantityWorkingDays = quantityWorkingDays;
        this.quantityReceptions = quantityReceptions;
        this.quantityCommissions = quantityCommissions;
        this.quantityStatusReceptions = quantityStatusReceptions;
    }

    public DoctorStatisticsResponse(int doctorId, String firstName, String lastName, String patronymic, String speciality,
                                    int quantityPatients, int quantityWorkingDays, int quantityReceptions,
                                    int quantityCommissions) {
        this.doctorId = doctorId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.speciality = speciality;
        this.quantityPatients = quantityPatients;
        this.quantityWorkingDays = quantityWorkingDays;
        this.quantityReceptions = quantityReceptions;
        this.quantityCommissions = quantityCommissions;
        this.quantityStatusReceptions = quantityStatusReceptions;
        this.quantityStatusReceptions = null;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public int getQuantityPatients() {
        return quantityPatients;
    }

    public void setQuantityPatients(int quantityPatients) {
        this.quantityPatients = quantityPatients;
    }

    public int getQuantityWorkingDays() {
        return quantityWorkingDays;
    }

    public void setQuantityWorkingDays(int quantityWorkingDays) {
        this.quantityWorkingDays = quantityWorkingDays;
    }

    public int getQuantityReceptions() {
        return quantityReceptions;
    }

    public void setQuantityReceptions(int quantityReceptions) {
        this.quantityReceptions = quantityReceptions;
    }

    public int getQuantityCommissions() {
        return quantityCommissions;
    }

    public void setQuantityCommissions(int quantityCommissions) {
        this.quantityCommissions = quantityCommissions;
    }

    public List<QuantityStatusResponse> getQuantityStatusReceptions() {
        return quantityStatusReceptions;
    }

    public void setQuantityStatusReceptions(List<QuantityStatusResponse> quantityStatusReceptions) {
        this.quantityStatusReceptions = quantityStatusReceptions;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public void addQuantityStatusReceptions(QuantityStatusResponse quantityStatusResponse){
        quantityStatusReceptions.add(quantityStatusResponse);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DoctorStatisticsResponse)) return false;

        DoctorStatisticsResponse that = (DoctorStatisticsResponse) o;

        if (getDoctorId() != that.getDoctorId()) return false;
        if (getQuantityPatients() != that.getQuantityPatients()) return false;
        if (getQuantityWorkingDays() != that.getQuantityWorkingDays()) return false;
        if (getQuantityReceptions() != that.getQuantityReceptions()) return false;
        if (getQuantityCommissions() != that.getQuantityCommissions()) return false;
        if (getFirstName() != null ? !getFirstName().equals(that.getFirstName()) : that.getFirstName() != null)
            return false;
        if (getLastName() != null ? !getLastName().equals(that.getLastName()) : that.getLastName() != null)
            return false;
        if (getPatronymic() != null ? !getPatronymic().equals(that.getPatronymic()) : that.getPatronymic() != null)
            return false;
        if (getSpeciality() != null ? !getSpeciality().equals(that.getSpeciality()) : that.getSpeciality() != null)
            return false;
        return getQuantityStatusReceptions() != null ? getQuantityStatusReceptions().equals(that.getQuantityStatusReceptions()) : that.getQuantityStatusReceptions() == null;
    }

    @Override
    public int hashCode() {
        int result = getDoctorId();
        result = 31 * result + (getFirstName() != null ? getFirstName().hashCode() : 0);
        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
        result = 31 * result + (getPatronymic() != null ? getPatronymic().hashCode() : 0);
        result = 31 * result + (getSpeciality() != null ? getSpeciality().hashCode() : 0);
        result = 31 * result + getQuantityPatients();
        result = 31 * result + getQuantityWorkingDays();
        result = 31 * result + getQuantityReceptions();
        result = 31 * result + getQuantityCommissions();
        result = 31 * result + (getQuantityStatusReceptions() != null ? getQuantityStatusReceptions().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DoctorStatisticsResponse{" +
                "doctorId=" + doctorId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", speciality='" + speciality + '\'' +
                ", quantityPatients=" + quantityPatients +
                ", quantityWorkingDays=" + quantityWorkingDays +
                ", quantityReceptions=" + quantityReceptions +
                ", quantityCommissions=" + quantityCommissions +
                ", quantityStatusReceptions=" + quantityStatusReceptions +
                '}';
    }
}
