package net.thumbtack.school.dto.responses.statistics;

public class QuantityTicketTypeResponse {
    private String ticketType;
    private int quantity;

    public QuantityTicketTypeResponse(String ticketType, int quantity) {
        this.ticketType = ticketType;
        this.quantity = quantity;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QuantityTicketTypeResponse)) return false;

        QuantityTicketTypeResponse that = (QuantityTicketTypeResponse) o;

        if (getQuantity() != that.getQuantity()) return false;
        return getTicketType() != null ? getTicketType().equals(that.getTicketType()) : that.getTicketType() == null;
    }

    @Override
    public int hashCode() {
        int result = getTicketType() != null ? getTicketType().hashCode() : 0;
        result = 31 * result + getQuantity();
        return result;
    }

    @Override
    public String toString() {
        return "QuantityTicketTypeResponse{" +
                "ticketType='" + ticketType + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
