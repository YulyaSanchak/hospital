package net.thumbtack.school.dto.filters;

public enum Filter {
    YES,
    NO;

    public static Filter filterFromString(String filterString) {
        return filterString == null ? NO : valueOf(filterString);
    }


}
