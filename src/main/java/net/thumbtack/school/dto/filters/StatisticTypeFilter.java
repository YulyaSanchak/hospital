package net.thumbtack.school.dto.filters;

public enum  StatisticTypeFilter {
    ALL, DOCTORS, PATIENTS
}
