package net.thumbtack.school.dto.requests;

import net.thumbtack.school.validators.NameConstraint;
import net.thumbtack.school.validators.PasswordConstraint;

import javax.validation.constraints.NotNull;

public class LoginDtoRequest {
    @NotNull(message = StringConstants.NULL_LOGIN)
    @NameConstraint(message = StringConstants.INVALID_LOGIN)
    private String login;

    @NotNull(message = StringConstants.NULL_PASSWORD)
    @PasswordConstraint(message = StringConstants.INVALID_PASSWORD)
    private String password;

    public LoginDtoRequest() {
    }

    public LoginDtoRequest(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LoginDtoRequest)) return false;

        LoginDtoRequest that = (LoginDtoRequest) o;

        if (getLogin() != null ? !getLogin().equals(that.getLogin()) : that.getLogin() != null) return false;
        return getPassword() != null ? getPassword().equals(that.getPassword()) : that.getPassword() == null;
    }

    @Override
    public int hashCode() {
        int result = getLogin() != null ? getLogin().hashCode() : 0;
        result = 31 * result + (getPassword() != null ? getPassword().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LoginDtoRequest{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
