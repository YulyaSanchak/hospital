package net.thumbtack.school.dto.requests;


public class StringConstants {
    public static final String NULL_FIRSTNAME = "firstName can't be null";
    public static final String INVALID_FIRSTNAME = "invalid firstName";
    public static final String NULL_LASTNAME = "lastName can't be null";
    public static final String INVALID_LASTNAME = "invalid lastName";
    public static final String INVALID_PATRONYMIC = "invalid patronymic";

    public static final String NULL_POSITION="position can't be null";

    public static final String NULL_PASSWORD="password can't be null";
    public static final String INVALID_PASSWORD="invalid password";



    public static final String NULL_EMAIL = "email can't be null";
    public static final String INVALID_EMAIL = "invalid email";

    public static final String NULL_ADDRESS = "address can't be null";
    public static final String INVALID_ADDRESS = "invalid address";


    public static final String NULL_PHONE = "phone can't be null";
    public static final String INVALID_PHONE = "invalid phone";


    public static final String NULL_LOGIN = "login can't be null";
    public static final String INVALID_LOGIN = "invalid login";

}
