package net.thumbtack.school.dto.requests.register_update_doctor;

import net.thumbtack.school.validators.DateConstraint;

import javax.validation.constraints.NotNull;
import java.util.List;

public class DoctorScheduleUpdateRequest {

    @NotNull(message = "dateStart can't be null")
    @DateConstraint(message = "invalid date")
    private String dateStart;

    @NotNull(message = "dateEnd can't be null")
    @DateConstraint(message = "invalid date")
    private String dateEnd;

    private List<DayScheduleRequest> weekDaysSchedule;
    private WeekSchedule weekSchedule;
    private int duration;

    public DoctorScheduleUpdateRequest() {
    }

    public DoctorScheduleUpdateRequest(String dateStart, String dateEnd,
                                       List<DayScheduleRequest> weekDaysSchedule,
                                       WeekSchedule weekSchedule,
                                       int duration) {
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.weekDaysSchedule = weekDaysSchedule;
        this.weekSchedule = weekSchedule;
        this.duration = duration;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public List<DayScheduleRequest> getWeekDaysSchedule() {
        return weekDaysSchedule;
    }

    public void setWeekDaysSchedule(List<DayScheduleRequest> weekDaysSchedule) {
        this.weekDaysSchedule = weekDaysSchedule;
    }

    public WeekSchedule getWeekSchedule() {
        return weekSchedule;
    }

    public void setWeekSchedule(WeekSchedule weekSchedule) {
        this.weekSchedule = weekSchedule;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DoctorScheduleUpdateRequest)) return false;

        DoctorScheduleUpdateRequest that = (DoctorScheduleUpdateRequest) o;

        if (getDuration() != that.getDuration()) return false;
        if (getDateStart() != null ? !getDateStart().equals(that.getDateStart()) : that.getDateStart() != null)
            return false;
        if (getDateEnd() != null ? !getDateEnd().equals(that.getDateEnd()) : that.getDateEnd() != null) return false;
        if (getWeekDaysSchedule() != null ? !getWeekDaysSchedule().equals(that.getWeekDaysSchedule()) : that.getWeekDaysSchedule() != null)
            return false;
        return getWeekSchedule() != null ? getWeekSchedule().equals(that.getWeekSchedule()) : that.getWeekSchedule() == null;
    }

    @Override
    public int hashCode() {
        int result = getDateStart() != null ? getDateStart().hashCode() : 0;
        result = 31 * result + (getDateEnd() != null ? getDateEnd().hashCode() : 0);
        result = 31 * result + (getWeekDaysSchedule() != null ? getWeekDaysSchedule().hashCode() : 0);
        result = 31 * result + (getWeekSchedule() != null ? getWeekSchedule().hashCode() : 0);
        result = 31 * result + getDuration();
        return result;
    }

    @Override
    public String toString() {
        return "DoctorScheduleUpdateRequest{" +
                "dateStart='" + dateStart + '\'' +
                ", dateEnd='" + dateEnd + '\'' +
                ", weekDaysSchedule=" + weekDaysSchedule +
                ", weekSchedule=" + weekSchedule +
                ", duration=" + duration +
                '}';
    }
}
