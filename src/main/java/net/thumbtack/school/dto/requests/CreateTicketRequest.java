package net.thumbtack.school.dto.requests;


import net.thumbtack.school.validators.DateConstraint;
import net.thumbtack.school.validators.TimeConstraint;

import javax.validation.constraints.NotNull;

public class CreateTicketRequest {
   private Integer doctorId;
   private String speciality;

   @NotNull(message = "date can't be null")
   @DateConstraint(message = "date invalid")
   private String date;

   @NotNull(message = "time can't be null")
   @TimeConstraint(message = "time invalid")
   private String time;

    public CreateTicketRequest() {
    }

    public CreateTicketRequest(Integer doctorId, String date, String time) {
        this.doctorId = doctorId;
        this.speciality = null;
        this.date = date;
        this.time = time;
    }

    public CreateTicketRequest(String speciality, String date, String time) {
        this.doctorId = null;
        this.speciality = speciality;
        this.date = date;
        this.time = time;
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CreateTicketRequest)) return false;

        CreateTicketRequest that = (CreateTicketRequest) o;

        if (getDoctorId() != null ? !getDoctorId().equals(that.getDoctorId()) : that.getDoctorId() != null)
            return false;
        if (getSpeciality() != null ? !getSpeciality().equals(that.getSpeciality()) : that.getSpeciality() != null)
            return false;
        if (getDate() != null ? !getDate().equals(that.getDate()) : that.getDate() != null) return false;
        return getTime() != null ? getTime().equals(that.getTime()) : that.getTime() == null;
    }

    @Override
    public int hashCode() {
        int result = getDoctorId() != null ? getDoctorId().hashCode() : 0;
        result = 31 * result + (getSpeciality() != null ? getSpeciality().hashCode() : 0);
        result = 31 * result + (getDate() != null ? getDate().hashCode() : 0);
        result = 31 * result + (getTime() != null ? getTime().hashCode() : 0);
        return result;
    }
}
