package net.thumbtack.school.dto.requests.register_update_patient;

import net.thumbtack.school.dto.requests.StringConstants;
import net.thumbtack.school.validators.*;

import javax.validation.constraints.NotNull;

public class PatientRegisterRequest {
    @NotNull(message = StringConstants.NULL_FIRSTNAME)
    @UserPersonalDataValidConstraint(message = StringConstants.INVALID_FIRSTNAME)
    private String firstName;

    @NotNull(message = StringConstants.NULL_LASTNAME)
    @UserPersonalDataValidConstraint(message = StringConstants.INVALID_LASTNAME)
    private String lastName;

    @UserPersonalDataValidConstraint(message = StringConstants.INVALID_PATRONYMIC)
    private String patronymic;

    @NotNull(message = StringConstants.NULL_EMAIL)
    @EmailConstraint(message = StringConstants.INVALID_EMAIL)
    private String email;

    @NotNull(message = StringConstants.NULL_ADDRESS)
    @AddressConstraint(message = StringConstants.INVALID_ADDRESS)
    private String address;

    @NotNull(message = StringConstants.NULL_PHONE)
    @PhoneConstraint(message = StringConstants.INVALID_PHONE)
    private String phone;

    @NotNull(message = StringConstants.NULL_LOGIN)
    @NameConstraint(message = StringConstants.INVALID_LOGIN)
    private String login;

    @NotNull(message = StringConstants.NULL_PASSWORD)
    @PasswordConstraint(message = StringConstants.INVALID_PASSWORD)
    private String password;

    public PatientRegisterRequest() {
    }

    public PatientRegisterRequest(String firstName, String lastName, String patronymic, String email,
                                  String address, String phone, String login, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.login = login;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PatientRegisterRequest)) return false;

        PatientRegisterRequest that = (PatientRegisterRequest) o;

        if (getFirstName() != null ? !getFirstName().equals(that.getFirstName()) : that.getFirstName() != null)
            return false;
        if (getLastName() != null ? !getLastName().equals(that.getLastName()) : that.getLastName() != null)
            return false;
        if (getPatronymic() != null ? !getPatronymic().equals(that.getPatronymic()) : that.getPatronymic() != null)
            return false;
        if (getEmail() != null ? !getEmail().equals(that.getEmail()) : that.getEmail() != null) return false;
        if (getAddress() != null ? !getAddress().equals(that.getAddress()) : that.getAddress() != null) return false;
        if (getPhone() != null ? !getPhone().equals(that.getPhone()) : that.getPhone() != null) return false;
        if (getLogin() != null ? !getLogin().equals(that.getLogin()) : that.getLogin() != null) return false;
        return getPassword() != null ? getPassword().equals(that.getPassword()) : that.getPassword() == null;
    }

    @Override
    public int hashCode() {
        int result = getFirstName() != null ? getFirstName().hashCode() : 0;
        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
        result = 31 * result + (getPatronymic() != null ? getPatronymic().hashCode() : 0);
        result = 31 * result + (getEmail() != null ? getEmail().hashCode() : 0);
        result = 31 * result + (getAddress() != null ? getAddress().hashCode() : 0);
        result = 31 * result + (getPhone() != null ? getPhone().hashCode() : 0);
        result = 31 * result + (getLogin() != null ? getLogin().hashCode() : 0);
        result = 31 * result + (getPassword() != null ? getPassword().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PatientRegisterRequest{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
