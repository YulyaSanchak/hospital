package net.thumbtack.school.dto.requests.register_update_doctor;

import net.thumbtack.school.dto.requests.StringConstants;
import net.thumbtack.school.validators.DateConstraint;
import net.thumbtack.school.validators.NameConstraint;
import net.thumbtack.school.validators.PasswordConstraint;
import net.thumbtack.school.validators.UserPersonalDataValidConstraint;

import javax.validation.constraints.NotNull;
import java.time.LocalTime;
import java.util.List;

public class DoctorRegisterRequest {

    @NotNull(message = StringConstants.NULL_FIRSTNAME)
    @UserPersonalDataValidConstraint(message = StringConstants.INVALID_FIRSTNAME)
    private String firstName;

    @NotNull(message = StringConstants.NULL_LASTNAME)
    @UserPersonalDataValidConstraint(message = StringConstants.INVALID_LASTNAME)
    private String lastName;

    @UserPersonalDataValidConstraint(message = StringConstants.INVALID_PATRONYMIC)
    private String patronymic;

    @NotNull(message = "speciality can't be null")
    private String speciality;

    @NotNull(message = "room can't be null")
    private String room;

    @NotNull(message = StringConstants.NULL_LOGIN)
    @NameConstraint(message = StringConstants.INVALID_LOGIN)
    private String login;

    @NotNull(message = StringConstants.NULL_PASSWORD)
    @PasswordConstraint(message = StringConstants.INVALID_PASSWORD)
    private String password;

    @NotNull(message = "dateStart can't be null")
    @DateConstraint(message = "date invalid")
    private String dateStart;

    @NotNull(message = "dateEnd can't be null")
    @DateConstraint(message = "date invalid")
    private String dateEnd;

    private List<DayScheduleRequest> weekDaysSchedule;
    private WeekSchedule weekSchedule;
    private int duration;

    public DoctorRegisterRequest() {
    }

    public DoctorRegisterRequest(String firstName, String lastName, String patronymic, String speciality, String room
            , String login, String password, String dateStart, String dateEnd, List<DayScheduleRequest> weekDaysSchedule
            , WeekSchedule weekSchedule, int duration) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.speciality = speciality;
        this.room = room;
        this.login = login;
        this.password = password;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.weekDaysSchedule = weekDaysSchedule;
        this.weekSchedule = weekSchedule;
        this.duration = duration;
    }

    public LocalTime getTimeStart(String weekday){
        for (DayScheduleRequest day: weekDaysSchedule){
            if (day.getWeekDay().equals(weekday)){
                return LocalTime.parse(day.getTimeStart());
            }
        }
        return null;
    }

    public LocalTime getTimeEnd(String weekday){
        for (DayScheduleRequest day: weekDaysSchedule){
            if (day.getWeekDay().equals(weekday)){
                return LocalTime.parse(day.getTimeEnd());
            }
        }
        return null;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public List<DayScheduleRequest> getWeekDaysSchedule() {
        return weekDaysSchedule;
    }

    public void setWeekDaysSchedule(List<DayScheduleRequest> weekDaysSchedule) {
        this.weekDaysSchedule = weekDaysSchedule;
    }

    public WeekSchedule getWeekSchedule() {
        return weekSchedule;
    }

    public void setWeekSchedule(WeekSchedule weekSchedule) {
        this.weekSchedule = weekSchedule;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DoctorRegisterRequest)) return false;

        DoctorRegisterRequest that = (DoctorRegisterRequest) o;

        if (getDuration() != that.getDuration()) return false;
        if (getFirstName() != null ? !getFirstName().equals(that.getFirstName()) : that.getFirstName() != null)
            return false;
        if (getLastName() != null ? !getLastName().equals(that.getLastName()) : that.getLastName() != null)
            return false;
        if (getPatronymic() != null ? !getPatronymic().equals(that.getPatronymic()) : that.getPatronymic() != null)
            return false;
        if (getSpeciality() != null ? !getSpeciality().equals(that.getSpeciality()) : that.getSpeciality() != null)
            return false;
        if (getRoom() != null ? !getRoom().equals(that.getRoom()) : that.getRoom() != null) return false;
        if (getLogin() != null ? !getLogin().equals(that.getLogin()) : that.getLogin() != null) return false;
        if (getPassword() != null ? !getPassword().equals(that.getPassword()) : that.getPassword() != null)
            return false;
        if (getDateStart() != null ? !getDateStart().equals(that.getDateStart()) : that.getDateStart() != null)
            return false;
        if (getDateEnd() != null ? !getDateEnd().equals(that.getDateEnd()) : that.getDateEnd() != null) return false;
        if (getWeekDaysSchedule() != null ? !getWeekDaysSchedule().equals(that.getWeekDaysSchedule()) : that.getWeekDaysSchedule() != null)
            return false;
        return getWeekSchedule() != null ? getWeekSchedule().equals(that.getWeekSchedule()) : that.getWeekSchedule() == null;
    }

    @Override
    public int hashCode() {
        int result = getFirstName() != null ? getFirstName().hashCode() : 0;
        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
        result = 31 * result + (getPatronymic() != null ? getPatronymic().hashCode() : 0);
        result = 31 * result + (getSpeciality() != null ? getSpeciality().hashCode() : 0);
        result = 31 * result + (getRoom() != null ? getRoom().hashCode() : 0);
        result = 31 * result + (getLogin() != null ? getLogin().hashCode() : 0);
        result = 31 * result + (getPassword() != null ? getPassword().hashCode() : 0);
        result = 31 * result + (getDateStart() != null ? getDateStart().hashCode() : 0);
        result = 31 * result + (getDateEnd() != null ? getDateEnd().hashCode() : 0);
        result = 31 * result + (getWeekDaysSchedule() != null ? getWeekDaysSchedule().hashCode() : 0);
        result = 31 * result + (getWeekSchedule() != null ? getWeekSchedule().hashCode() : 0);
        result = 31 * result + getDuration();
        return result;
    }
}
