package net.thumbtack.school.dto.requests;

import net.thumbtack.school.validators.DateConstraint;
import net.thumbtack.school.validators.TimeConstraint;

import javax.validation.constraints.NotNull;
import java.util.List;

public class CommissionRequest {
   private int patientId;
   private List<Integer> doctorIds;
   private String room;

   @NotNull(message = "date can't be null")
   @DateConstraint(message = "date invalid")
   private String date;

   @NotNull(message = "time can't be null")
   @TimeConstraint(message = "time invalid")
   private String time;

   private int duration;

    public CommissionRequest(int patientId, List<Integer> doctorIds, String room, String date, String time, int duration) {
        this.patientId = patientId;
        this.doctorIds = doctorIds;
        this.room = room;
        this.date = date;
        this.time = time;
        this.duration = duration;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public List<Integer> getDoctorIds() {
        return doctorIds;
    }

    public void setDoctorIds(List<Integer> doctorIds) {
        this.doctorIds = doctorIds;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CommissionRequest)) return false;

        CommissionRequest that = (CommissionRequest) o;

        if (getPatientId() != that.getPatientId()) return false;
        if (getDuration() != that.getDuration()) return false;
        if (getDoctorIds() != null ? !getDoctorIds().equals(that.getDoctorIds()) : that.getDoctorIds() != null)
            return false;
        if (getRoom() != null ? !getRoom().equals(that.getRoom()) : that.getRoom() != null) return false;
        if (getDate() != null ? !getDate().equals(that.getDate()) : that.getDate() != null) return false;
        return getTime() != null ? getTime().equals(that.getTime()) : that.getTime() == null;
    }

    @Override
    public int hashCode() {
        int result = getPatientId();
        result = 31 * result + (getDoctorIds() != null ? getDoctorIds().hashCode() : 0);
        result = 31 * result + (getRoom() != null ? getRoom().hashCode() : 0);
        result = 31 * result + (getDate() != null ? getDate().hashCode() : 0);
        result = 31 * result + (getTime() != null ? getTime().hashCode() : 0);
        result = 31 * result + getDuration();
        return result;
    }

    @Override
    public String toString() {
        return "CommissionRequest{" +
                "patientId=" + patientId +
                ", doctorIds=" + doctorIds +
                ", room='" + room + '\'' +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", duration=" + duration +
                '}';
    }
}
