package net.thumbtack.school.dto.requests.register_update_doctor;


import net.thumbtack.school.validators.TimeConstraint;

import javax.validation.constraints.NotNull;

public class DayScheduleRequest {
    private String weekDay;

    @NotNull(message = "timeStart can't be null")
    @TimeConstraint(message = "time invalid")
    private String timeStart;
    @NotNull(message = "timeEnd can't be null")
    @TimeConstraint(message = "time invalid")
    private String timeEnd;

    public DayScheduleRequest() {
    }

    public DayScheduleRequest(String weekDay, String timeStart, String timeEnd) {
        this.weekDay = weekDay;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
    }

    public String getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(String weekDay) {
        this.weekDay = weekDay;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DayScheduleRequest)) return false;

        DayScheduleRequest that = (DayScheduleRequest) o;

        if (getWeekDay() != null ? !getWeekDay().equals(that.getWeekDay()) : that.getWeekDay() != null) return false;
        if (getTimeStart() != null ? !getTimeStart().equals(that.getTimeStart()) : that.getTimeStart() != null)
            return false;
        return getTimeEnd() != null ? getTimeEnd().equals(that.getTimeEnd()) : that.getTimeEnd() == null;
    }

    @Override
    public int hashCode() {
        int result = getWeekDay() != null ? getWeekDay().hashCode() : 0;
        result = 31 * result + (getTimeStart() != null ? getTimeStart().hashCode() : 0);
        result = 31 * result + (getTimeEnd() != null ? getTimeEnd().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DayScheduleRequest{" +
                "weekDay='" + weekDay + '\'' +
                ", timeStart=" + timeStart +
                ", timeEnd=" + timeEnd +
                '}';
    }
}
