package net.thumbtack.school.dto.requests.register_update_admin;

import net.thumbtack.school.dto.requests.StringConstants;
import net.thumbtack.school.validators.NameConstraint;
import net.thumbtack.school.validators.PasswordConstraint;
import net.thumbtack.school.validators.UserPersonalDataValidConstraint;

import javax.validation.constraints.NotNull;

public class AdminUpdateRequest {
    @NotNull(message = StringConstants.NULL_FIRSTNAME)
    @UserPersonalDataValidConstraint(message = StringConstants.INVALID_FIRSTNAME)
    private String firstName;

    @NotNull(message = StringConstants.NULL_LASTNAME)
    @UserPersonalDataValidConstraint(message = StringConstants.INVALID_LASTNAME)
    private String lastName;

    @UserPersonalDataValidConstraint(message = StringConstants.INVALID_PATRONYMIC)
    private String patronymic;

    @NotNull(message = StringConstants.NULL_POSITION)
    private String position;

    @NotNull(message = StringConstants.NULL_PASSWORD)
    @PasswordConstraint(message = StringConstants.INVALID_PASSWORD)
    private String oldPassword;

    @NotNull(message = StringConstants.NULL_PASSWORD)
    @PasswordConstraint(message = StringConstants.INVALID_PASSWORD)
    private String newPassword;

    public AdminUpdateRequest( String firstName, String lastName, String patronymic, String position,
                               String oldPassword, String newPassword) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.position = position;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }


    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AdminUpdateRequest)) return false;

        AdminUpdateRequest that = (AdminUpdateRequest) o;

        if (getFirstName() != null ? !getFirstName().equals(that.getFirstName()) : that.getFirstName() != null)
            return false;
        if (getLastName() != null ? !getLastName().equals(that.getLastName()) : that.getLastName() != null)
            return false;
        if (getPatronymic() != null ? !getPatronymic().equals(that.getPatronymic()) : that.getPatronymic() != null)
            return false;
        if (getPosition() != null ? !getPosition().equals(that.getPosition()) : that.getPosition() != null)
            return false;
        if (getOldPassword() != null ? !getOldPassword().equals(that.getOldPassword()) : that.getOldPassword() != null)
            return false;
        return getNewPassword() != null ? getNewPassword().equals(that.getNewPassword()) : that.getNewPassword() == null;
    }

    @Override
    public int hashCode() {
        int result = getFirstName() != null ? getFirstName().hashCode() : 0;
        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
        result = 31 * result + (getPatronymic() != null ? getPatronymic().hashCode() : 0);
        result = 31 * result + (getPosition() != null ? getPosition().hashCode() : 0);
        result = 31 * result + (getOldPassword() != null ? getOldPassword().hashCode() : 0);
        result = 31 * result + (getNewPassword() != null ? getNewPassword().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AdminUpdateRequest{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", position='" + position + '\'' +
                ", oldPassword='" + oldPassword + '\'' +
                ", newPassword='" + newPassword + '\'' +
                '}';
    }
}
