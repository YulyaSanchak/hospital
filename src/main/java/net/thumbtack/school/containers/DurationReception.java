package net.thumbtack.school.containers;

import java.time.LocalDate;
import java.util.List;

public class DurationReception {
    private int doctorId;
    private LocalDate date;
    private List<Integer> duration;

    public DurationReception() {
    }

    public DurationReception(int doctorId, LocalDate date, List<Integer> duration) {
        this.doctorId = doctorId;
        this.date = date;
        this.duration = duration;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public List<Integer> getDuration() {
        return duration;
    }

    public void setDuration(List<Integer> duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "DurationReception{" +
                "doctorId=" + doctorId +
                ", date=" + date +
                ", duration=" + duration +
                ", size=" + duration.size() +
                '}';
    }
}
