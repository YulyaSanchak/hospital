package net.thumbtack.school.containers.statistics;



public class Statistic {
    private String status;
    private int quantity;

    public Statistic(String status, int quantity) {
        this.status = status;
        this.quantity = quantity;
    }
    public Statistic(String status, long quantity) {
        this.status = status;
        this.quantity = (int)quantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
