package net.thumbtack.school.containers.statistics;

import net.thumbtack.school.model.Status;
import net.thumbtack.school.model.TicketType;

import java.util.List;

public class PatientStatistic {
    private int patientId;
    private String firstName;
    private String lastName;
    private String patronymic;
    private int quantity;

    public PatientStatistic() {
    }

    public PatientStatistic(int patientId, String firstName, String lastName, String patronymic, int quantity) {
        this.patientId = patientId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.quantity = quantity;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
