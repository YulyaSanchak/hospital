package net.thumbtack.school.containers.statistics;

import net.thumbtack.school.model.Status;

import java.util.List;

public class DoctorStatistic {
    private int doctorId;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String specialityName;
    private int quantity;

    public DoctorStatistic() {
    }

    public DoctorStatistic(int doctorId, String firstName, String lastName, String patronymic,String specialityName, int quantity) {
            this.doctorId = doctorId;
            this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.quantity = quantity;

        this.specialityName = specialityName;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getSpecialityName() {
        return specialityName;
    }

    public void setSpecialityName(String specialityName) {
        this.specialityName = specialityName;
    }
}
