package net.thumbtack.school.mappers;


import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import net.thumbtack.school.model.Speciality;
import org.apache.ibatis.annotations.*;

public interface SpecialityMapper {

    @Insert("INSERT INTO speciality (name) VALUES (#{name}) ")
    @Options(useGeneratedKeys = true)
    Integer insert(Speciality speciality);

    @Select("SELECT speciality.id as id, speciality.name as name FROM speciality WHERE speciality.id = #{id}")
    Speciality getById(@Param("id") int id);

    @Select("SELECT speciality.id as id, speciality.name as name FROM speciality WHERE speciality.name = #{name}")
    Speciality getByName(@Param("name") String name);

    @Delete("DELETE FROM speciality WHERE id = #{speciality.id}")
    void delete(@Param("speciality") Speciality speciality);

    @Delete("DELETE FROM speciality")
    void deleteAll();
}
