package net.thumbtack.school.mappers;


import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import net.thumbtack.school.model.users.Patient;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;
public interface PatientMapper {
    @Insert("INSERT INTO patient ( userId, email, address, phone) VALUES (#{userId}, #{email}, #{address}, #{phone})")
    @Options(useGeneratedKeys = true)
    Integer insert(Patient patient);

    @Select("SELECT patient.id  as id , patient.userId as userId, patient.email as email, patient.address as address," +
            "patient.phone as phone, user.firstName as firstName, user.lastName as lastName, user.patronymic as patronymic," +
            "user.login as login, user.password as password, user.userType as userType" +
            " FROM patient JOIN user ON user.id = patient.userId  WHERE patient.id = #{id}")
    Patient getById(@Param("id") int id);

    @Select("SELECT patient.id  as id , patient.userId as userId, patient.email as email, patient.address as address," +
            "patient.phone as phone, user.firstName as firstName, user.lastName as lastName, user.patronymic as patronymic," +
            "user.login as login, user.password as password, user.userType as userType" +
            " FROM patient JOIN user ON user.id = patient.userId  WHERE patient.userId = #{userId}")
    Patient getByUserId(@Param("userId") int userId);

    @Delete("DELETE FROM patient WHERE id = #{patient.id}")
    void delete(@Param("patient") Patient patient);

    @Delete("DELETE FROM patient")
    void deleteAll();

    @Update("UPDATE patient SET email = #{patient.email} ,address = #{patient.address}, phone = #{patient.phone}" +
            "WHERE id = #{patient.id}")
    void update(@Param("patient") Patient patient);
}
