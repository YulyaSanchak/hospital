package net.thumbtack.school.mappers;

import net.thumbtack.school.model.*;
import net.thumbtack.school.model.users.Doctor;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;


public interface CommissionMapper {

    @Insert("INSERT INTO commission ( ticketId, date, duration, roomName) VALUES (#{ticket.id}, #{time}, #{duration}, #{roomName}) ")
    @Options(useGeneratedKeys = true)
    Integer insert(Commission commission);

    @Insert({"<script>",
            "INSERT INTO commission_doctors ( id, commissionId, doctorId ) VALUES",
            "<foreach item='item' collection='list' separator=','>",
            "(0, #{commissionId} , #{item.id} )",
            "</foreach>",
            "</script>"})
    void batchInsert(@Param("commissionId")int commissionId, @Param("list") List<Doctor> doctors);

    @Insert("INSERT INTO commission_doctors ( commissionId, doctorId ) VALUES ( #{commissionId} , #{doctor.id} )")
    @Options(useGeneratedKeys = true)
    void insertDoctor(@Param("commissionId")int commissionId, @Param("doctor") Doctor doctor);

    @Select("SELECT commission.id as id, commission.date as date, commission.duration as duration," +
            " commission.ticketId as ticketId, commission.roomName as roomName " +
            " FROM commission " +
            " WHERE commission.id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result (property = "ticket" ,column = "ticketId" , javaType = Ticket.class,
                    one = @One(select = "net.thumbtack.school.mappers.TicketMapper.getById" , fetchType = FetchType.LAZY)),
            @Result(property = "doctors", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.mappers.CommissionMapper.getCommissionDoctors" , fetchType = FetchType.LAZY)),
            @Result(property = "time", column = "date"),
            @Result(property = "duration",column ="duration")
    })
    Commission getById(@Param("id") int id);

    @Select("SELECT commission.id as id, commission.date as date, commission.duration as duration," +
            " commission.ticketId as ticketId FROM commission WHERE commission.id " +
            " IN (SELECT commission_doctors.commissionId" +
            " FROM commission_doctors WHERE commission_doctors.doctorId = #{doctor.id})")
    List<Commission> getByDoctor(@Param("doctor") Doctor doctor);


    @Select("SELECT doctor.id  as id , doctor.userId as userId, doctor.specialityId as specialityId, " +
            "doctor.roomId as roomId, user.firstName as firstName, user.lastName as lastName" +
            ", user.patronymic as patronymic, user.login as login, user.password as password" +
            " FROM doctor JOIN user ON user.id = doctor.userId WHERE doctor.id " +
            " IN (SELECT commission_doctors.doctorId as doctorId FROM commission_doctors " +
            " WHERE commission_doctors.commissionId = #{commissionId})")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result (property = "room" ,column = "roomId" , javaType = Room.class,
                    many = @Many(select = "net.thumbtack.school.mappers.RoomMapper.getById" , fetchType = FetchType.LAZY)),
            @Result (property = "speciality" ,column = "specialityId" , javaType = Speciality.class,
                    many = @Many(select = "net.thumbtack.school.mappers.SpecialityMapper.getById" , fetchType = FetchType.LAZY))
    })
    List<Doctor> getCommissionDoctors(@Param("commissionId") int commissionId);

    @Delete("DELETE FROM commission WHERE id = #{commission.id}")
    void delete(@Param("commission") Commission commission);

    @Delete("DELETE FROM commission_doctors WHERE commission_doctors.commissionId = #{commission.id} ")
    void deleteDoctorsFromCommissions(@Param("commission") Commission commission);

    @Delete("DELETE FROM commission ")
    void deleteAll();

    @Delete("DELETE FROM commission_doctors ")
    void deleteAllDoctorsFromCommissions();

    @Select("SELECT commission.id as id, ticketId, date, duration FROM commission JOIN ticket " +
            "ON commission.ticketId = ticket.id AND ticket.ticketNumber = #{number}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result (property = "time" ,column = "date"),
            @Result (property = "ticket" ,column = "ticketId" , javaType = Ticket.class,
                    many = @Many(select = "net.thumbtack.school.mappers.TicketMapper.getById" , fetchType = FetchType.LAZY)),
            @Result(property = "doctors", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.mappers.CommissionMapper.getCommissionDoctors" , fetchType = FetchType.LAZY))
    })
    Commission getByTicketNumber(@Param("number")String ticketNumber);

    @Update("UPDATE commission SET commission.ticketId = #{ticketId} WHERE commission.id = #{id} ")
    void updateTicket(@Param("id") int id,@Param("ticketId") Integer ticketId);
}
