package net.thumbtack.school.mappers;

import net.thumbtack.school.model.DaySchedule;
import net.thumbtack.school.model.Reception;
import net.thumbtack.school.model.Status;
import net.thumbtack.school.model.Ticket;
import net.thumbtack.school.model.users.Doctor;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.time.LocalDate;
import java.util.List;
public interface ReceptionMapper {

    @Insert("INSERT INTO reception ( ticketId, day_scheduleId, time, duration,status) VALUES (#{ticket.id}, #{daySchedule.id}, #{time}," +
            " #{duration}, #{status}) ")
    @Options(useGeneratedKeys = true)
    Integer insert(Reception reception);

    @Insert({"<script>",
            "INSERT INTO reception (ticketId, day_scheduleId, time, duration, status) VALUES",
            "<foreach item='item' collection='list' separator=','>",
            "(#{item.ticket.id}, #{item.daySchedule.id}, #{item.time}, #{item.duration}, #{item.status})",
            "</foreach>",
            "</script>"})
    @Options(useGeneratedKeys = true)
    void batchInsert(@Param("list") List<Reception> receptions);


    @Select("SELECT reception.id as id, reception.day_scheduleId as dayScheduleId, reception.ticketId as ticketId, reception.time as time, " +
            "reception.duration as duration, reception.status as status FROM reception WHERE reception.id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result (property = "ticket" ,column = "ticketId" , javaType = Ticket.class,
                    one = @One(select = "net.thumbtack.school.mappers.TicketMapper.getById" , fetchType = FetchType.LAZY))
    })
    Reception getById(@Param("id") int id);

    @Select("SELECT reception.id as id, reception.day_scheduleId as dayScheduleId, reception.ticketId as ticketId, reception.time as time, " +
            "reception.duration as duration, reception.status as status FROM reception WHERE reception.day_scheduleId = #{scheduleDay.id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result (property = "ticket" ,column = "ticketId" , javaType = Ticket.class,
                    one = @One(select = "net.thumbtack.school.mappers.TicketMapper.getById" , fetchType = FetchType.LAZY))
    })
    List<Reception> getByDaySchedule(@Param("scheduleDay") DaySchedule daySchedule);

    @Select("SELECT reception.id as id, reception.day_scheduleId as dayScheduleId, reception.ticketId as ticketId, " +
            "reception.time as time, reception.duration as duration,  reception.status as status " +
            " FROM reception JOIN day_schedule ON reception.day_scheduleId = day_schedule.id " +
            " WHERE day_schedule.doctorId = #{doctor.id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result (property = "ticket" ,column = "ticketId" , javaType = Ticket.class,
                    one = @One(select = "net.thumbtack.school.mappers.TicketMapper.getById" , fetchType = FetchType.LAZY))
    })
    List<Reception> getByDoctor(@Param("doctor") Doctor doctor);

    @Select("SELECT reception.id as id, reception.day_scheduleId as dayScheduleId, reception.ticketId as ticketId, " +
            "reception.time as time, reception.duration as duration, reception.status as status " +
            " FROM reception WHERE reception.ticketId = #{ticketId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result (property = "ticket" ,column = "ticketId" , javaType = Ticket.class,
                    one = @One(select = "net.thumbtack.school.mappers.TicketMapper.getById" , fetchType = FetchType.LAZY))
    })
    Reception getByTicket(@Param("ticketId") int ticketId);

    @Delete("DELETE FROM reception WHERE id = #{reception.id}")
    void delete(@Param("reception") Reception reception);

    @Delete("<script>" +
            "DELETE FROM reception WHERE id IN " +
            "( <foreach item='item' collection='list' separator=',' >" +
            " #{item} " +
            " </foreach> )" +
            "</script>"
    )
    void deleteByIds(@Param("list") List<Integer> ids);

    @Select("SELECT reception.id as id FROM reception JOIN day_schedule ON reception.day_scheduleId = day_schedule.id " +
            " AND day_schedule.doctorId = #{doctorId} AND (day_schedule.date BETWEEN #{startDate} AND #{endDate})")
    List<Integer> getIdByDoctorAndDate(@Param("doctorId") int doctorId,
                               @Param("startDate") LocalDate startDate,
                               @Param("endDate")LocalDate endDate);

    @Delete("DELETE FROM reception ")
    void deleteAll();

    @Update("UPDATE reception SET reception.ticketId = #{ticketId}, reception.status = #{status} WHERE reception.id = #{id} ")
    void update(@Param("id") int id, @Param("ticketId") Integer ticketId, @Param("status")Status status);

    @Update("UPDATE reception SET reception.ticketId = #{ticketId}, reception.status = #{status} " +
            "WHERE reception.id = #{id} AND reception.status = 'FREE' ")
    Integer updateForTicket(@Param("id") int id, @Param("ticketId") Integer ticketId, @Param("status")Status status);

    @Update("<script>" +
            "UPDATE reception SET reception.status = 'BUSY' WHERE id IN " +
            "( <foreach item='item' collection='list' separator=',' >" +
            " #{item} " +
            " </foreach> )" +
            "</script>"
    )
    Integer updateForCommission(@Param("list") List<Integer> ids);

    @Update("UPDATE reception SET reception.status = #{status} WHERE reception.ticketId = #{ticketId} ")
    void updateState(@Param("ticketId") Integer ticketId, @Param("status")Status status);

    @Select("SELECT duration FROM reception " +
            "JOIN day_schedule " +
            "ON reception.day_scheduleId = day_schedule.id AND day_schedule.doctorId = #{doctorId} AND day_schedule.date = #{date}" +
            "WHERE (reception.status = 'FREE' AND reception.ticketId) OR reception.status ='TICKET'")
    Integer getDurationReception(@Param("doctorId") int doctorId,@Param("date")LocalDate date);


}
