package net.thumbtack.school.mappers;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import net.thumbtack.school.model.Room;
import org.apache.ibatis.annotations.*;

public interface RoomMapper {

    @Insert("INSERT INTO room (name) VALUES (#{name}) ")
    @Options(useGeneratedKeys = true)
    Integer insert(Room room);

    @Select("SELECT * FROM room WHERE room.id = #{id}")
    Room getById(@Param("id") int id);

    @Select("SELECT room.id as id, room.name as name FROM room JOIN doctor ON doctor.roomId = room.id AND room.name = #{name}")
    Room getBusyRoomByName(@Param("name") String roomName);

    @Select("SELECT * FROM room WHERE room.name LIKE #{name}")
    Room getByName(@Param("name") String name);

    @Update("UPDATE room SET name = #{room.name} WHERE id = #{room.id}")
    void update(@Param("room") Room room);

    @Delete("DELETE FROM room WHERE id = #{room.id}")
    void delete(@Param("room") Room room);

    @Delete("DELETE FROM room")
    void deleteAll();



}
