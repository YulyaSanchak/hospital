package net.thumbtack.school.mappers;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import net.thumbtack.school.model.users.Admin;
import org.apache.ibatis.annotations.*;

public interface AdminMapper {
    @Insert("INSERT INTO admin ( userId, position) VALUES (#{userId}, #{position})")
    @Options(useGeneratedKeys = true)
    Integer insert(Admin admin) ;

    @Select("SELECT admin.id  as id , admin.userId as userId, admin.position as position, user.firstName as firstName, " +
            "user.lastName as lastName, user.patronymic as patronymic, user.login as login, user.password as password," +
            " user.userType as userType" +
            " FROM admin JOIN user ON user.id = admin.userId  WHERE admin.id = #{id}")
    Admin getById(@Param("id") int id);

    @Select("SELECT admin.id  as id , admin.userId as userId, admin.position as position, user.firstName as firstName, " +
            "user.lastName as lastName, user.patronymic as patronymic, user.login as login, user.password as password," +
            " user.userType as userType" +
            " FROM admin JOIN user ON user.id = admin.userId  WHERE admin.userId = #{userId}")
    Admin getByUserId(@Param("userId") int userId);

    @Select("SELECT admin.id  as id , admin.userId as userId, admin.position as position, user.firstName as firstName, " +
            "user.lastName as lastName, user.patronymic as patronymic, user.login as login, user.password as password," +
            " user.userType as userType" +
            " FROM admin JOIN user ON user.id = admin.userId" +
            " JOIN session ON session.userId  = admin.userId AND session.cookie=#{cookie}")
    Admin getByCookie(@Param("cookie") String cookie);

    @Delete("DELETE FROM admin WHERE id = #{admin.id}")
    void delete(@Param("admin") Admin admin);

    @Update("UPDATE admin SET userId = #{admin.userId}, position = #{admin.position} WHERE id = #{admin.id} ")
    void update(@Param("admin") Admin admin);

    @Delete("DELETE FROM admin")
    void deleteAll();
}
