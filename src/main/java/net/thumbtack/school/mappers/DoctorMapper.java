package net.thumbtack.school.mappers;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import net.thumbtack.school.model.Room;
import net.thumbtack.school.model.Speciality;
import net.thumbtack.school.model.users.Doctor;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;


public interface DoctorMapper {

    @Insert("INSERT INTO doctor ( userId, specialityId, roomId) VALUES (#{userId}, #{speciality.id}, " +
            "#{room.id})")
    @Options(useGeneratedKeys = true)
    Integer insert(Doctor doctor);

    @Select("SELECT doctor.id  as id , doctor.userId as userId, doctor.specialityId as specialityId, " +
            "doctor.roomId as roomId, user.firstName as firstName, user.lastName as lastName" +
            ", user.patronymic as patronymic, user.login as login, user.password as password" +
            " FROM doctor JOIN user ON user.id = doctor.userId WHERE doctor.id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result (property = "room" ,column = "roomId" , javaType = Room.class,
                    one = @One(select = "net.thumbtack.school.mappers.RoomMapper.getById" , fetchType = FetchType.LAZY)),
            @Result (property = "speciality" ,column = "specialityId" , javaType = Speciality.class,
                    one = @One(select = "net.thumbtack.school.mappers.SpecialityMapper.getById" , fetchType = FetchType.LAZY)),
            @Result (property = "daySchedules" ,column = "id" , javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.mappers.DayScheduleMapper.getByDoctor" , fetchType = FetchType.LAZY))
    })
    Doctor getById(@Param("id") int id);

    @Select("SELECT doctor.id  as id , doctor.userId as userId, doctor.specialityId as specialityId, " +
            "doctor.roomId as roomId, user.firstName as firstName, user.lastName as lastName" +
            ", user.patronymic as patronymic, user.login as login, user.password as password" +
            " FROM doctor JOIN user ON user.id = doctor.userId WHERE doctor.userId = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result (property = "room" ,column = "roomId" , javaType = Room.class,
                    one = @One(select = "net.thumbtack.school.mappers.RoomMapper.getById" , fetchType = FetchType.LAZY)),
            @Result (property = "speciality" ,column = "specialityId" , javaType = Speciality.class,
                    one = @One(select = "net.thumbtack.school.mappers.SpecialityMapper.getById" , fetchType = FetchType.LAZY)),
            @Result (property = "daySchedules" ,column = "id" , javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.mappers.DayScheduleMapper.getByDoctor" , fetchType = FetchType.LAZY))
    })
    Doctor getByUserId(@Param("userId") int userId);

    @Select("SELECT doctor.id  as id , doctor.userId as userId, doctor.specialityId as specialityId, " +
            "doctor.roomId as roomId, user.firstName as firstName, user.lastName as lastName" +
            ", user.patronymic as patronymic, user.login as login, user.password as password" +
            " FROM doctor JOIN user ON user.id = doctor.userId WHERE doctor.specialityId = #{speciality.id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result (property = "room" ,column = "roomId" , javaType = Room.class,
                    one = @One(select = "net.thumbtack.school.mappers.RoomMapper.getById" , fetchType = FetchType.LAZY)),
            @Result (property = "speciality" ,column = "specialityId" , javaType = Speciality.class,
                    one = @One(select = "net.thumbtack.school.mappers.SpecialityMapper.getById" , fetchType = FetchType.LAZY))
    })
    List<Doctor> getBySpeciality(@Param("speciality")Speciality speciality);

    @Select("SELECT doctor.id as id , doctor.userId as userId, doctor.specialityId as specialityId, " +
            "doctor.roomId as roomId, user.firstName as firstName, user.lastName as lastName" +
            ", user.patronymic as patronymic, user.login as login, user.password as password" +
            " FROM doctor JOIN user ON user.id = doctor.userId")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result (property = "room" ,column = "roomId" , javaType = Room.class,
                    one = @One(select = "net.thumbtack.school.mappers.RoomMapper.getById" , fetchType = FetchType.LAZY)),
            @Result (property = "speciality" ,column = "specialityId" , javaType = Speciality.class,
                    one = @One(select = "net.thumbtack.school.mappers.SpecialityMapper.getById" , fetchType = FetchType.LAZY))
    })
    List<Doctor> getAll();


    @Update("UPDATE doctor SET specialityId = #{doctor.speciality.id}, roomId = #{doctor.room.id} " +
            " WHERE id = #{doctor.id}")
    void update(@Param("doctor") Doctor doctor);

    @Delete("DELETE FROM doctor WHERE id = #{doctor.id}")
    void delete(@Param("doctor") Doctor doctor);

    @Delete("DELETE FROM doctor")
    void deleteAll();
}
