package net.thumbtack.school.mappers;

import net.thumbtack.school.model.DaySchedule;
import net.thumbtack.school.model.Reception;
import net.thumbtack.school.model.Ticket;
import net.thumbtack.school.model.users.Doctor;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.time.LocalDate;
import java.util.List;

public interface DayScheduleMapper {

    @Insert("INSERT INTO day_schedule ( doctorId, date) VALUES ( #{doctor.id}, #{date}) ")
    @Options(useGeneratedKeys = true)
    Integer insert(DaySchedule daySchedule);

    @Insert({"<script>",
            "INSERT INTO day_schedule (doctorId, date) VALUES",
            "<foreach item='item' collection='list' separator=','>",
            "(#{item.doctor.id}, #{item.date})",
            "</foreach>",
            "</script>"})
    @Options(useGeneratedKeys = true)
    void batchInsert(@Param("list") List<DaySchedule> daySchedule);

    @Select("SELECT day_schedule.id as id, day_schedule.doctorId as doctorId, day_schedule.date as date " +
            " FROM day_schedule WHERE day_schedule.id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "doctor", column = "doctorId", javaType = Doctor.class,
                    one = @One(select = "net.thumbtack.school.mappers.DoctorMapper.getById" , fetchType = FetchType.LAZY)),
            @Result(property = "receptions", column = "id", javaType = List.class,
                    many =@Many(select = "net.thumbtack.school.mappers.ReceptionMapper.getByDaySchedule", fetchType = FetchType.LAZY))
    })
    DaySchedule getById(@Param("id") int id);

    @Select("SELECT day_schedule.id as id, day_schedule.doctorId as doctorId, day_schedule.date as date " +
            " FROM day_schedule WHERE day_schedule.date = #{date} AND day_schedule.doctorId = #{doctor.id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "doctor", column = "doctorId", javaType = Doctor.class,
                    one = @One(select = "net.thumbtack.school.mappers.DoctorMapper.getById" , fetchType = FetchType.LAZY)),
            @Result(property = "receptions", column = "id", javaType = List.class,
                    many =@Many(select = "net.thumbtack.school.mappers.ReceptionMapper.getByDaySchedule", fetchType = FetchType.LAZY))
    })
    DaySchedule getByDateAndDoctor(@Param("date")LocalDate date, @Param("doctor") Doctor doctor);

    @Select("SELECT day_schedule.id as id, day_schedule.doctorId as doctorId, day_schedule.date as date " +
            " FROM day_schedule WHERE day_schedule.doctorId = #{doctor.id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "receptions", column = "id", javaType = List.class,
                    many =@Many(select = "net.thumbtack.school.mappers.ReceptionMapper.getByDaySchedule", fetchType = FetchType.LAZY))
    })
    List<DaySchedule> getByDoctor(@Param("doctor") Doctor doctor);

    @Delete("DELETE FROM day_schedule WHERE id = #{scheduleDay.id}")
    void delete(@Param("scheduleDay") DaySchedule scheduleDay);

    @Delete("DELETE FROM day_schedule WHERE day_schedule.doctorId = #{doctorId} AND " +
            "(day_schedule.date BETWEEN #{startDate} AND #{endDate})")
    void deleteByDoctorAndDate(@Param("doctorId") int doctorId,
                               @Param("startDate")LocalDate startDate,
                               @Param("endDate")LocalDate endDate);

    @Delete("DELETE FROM day_schedule ")
    void deleteAll();

    @Update("UPDATE day_schedule SET doctorId = #{daySchedule.doctor.id}, date =#{daySchedule.date} WHERE day_schedule.id = #{id}) ")
    void update(@Param("daySchedule") DaySchedule daySchedule);

}
