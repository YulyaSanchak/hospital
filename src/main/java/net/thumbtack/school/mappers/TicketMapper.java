package net.thumbtack.school.mappers;

import net.thumbtack.school.model.Ticket;
import net.thumbtack.school.model.users.Patient;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

public interface TicketMapper {

    @Insert("INSERT INTO ticket ( patientId, ticketNumber, ticketType) VALUES (#{patient.id}, #{ticketNumber}, " +
            " #{ticketType})")
    @Options(useGeneratedKeys = true)
    Integer insert(Ticket ticket);

    @Select("SELECT * FROM ticket WHERE id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result (property = "patient" ,column = "patientId" , javaType = Patient.class,
                    one = @One(select = "net.thumbtack.school.mappers.PatientMapper.getById" , fetchType = FetchType.LAZY))
    })
    Ticket getById(@Param("id") int id);

    @Select("SELECT * FROM ticket WHERE ticketNumber = #{number}")
    Ticket getByNumber(@Param("number") String number);

    @Select("SELECT * FROM ticket WHERE patientId = #{patient.id}")
    List<Ticket> getByPatient(@Param("patient") Patient patient);

    @Delete("DELETE FROM ticket WHERE id = #{ticket.id}")
    void delete(@Param("ticket") Ticket ticket);

    @Delete("DELETE FROM ticket ")
    void deleteAll();
}
