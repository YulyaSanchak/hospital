package net.thumbtack.school.mappers;


import net.thumbtack.school.model.Session;
import net.thumbtack.school.model.users.User;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

public interface SessionMapper {
    @Insert("INSERT INTO session (userId, cookie) " +
            "VALUES ( #{user.userId}, #{session.cookie}) ON DUPLICATE KEY UPDATE cookie=#{session.cookie}")
    @Options(useGeneratedKeys = true, keyProperty = "session.id")
    void insert(@Param("session") Session session, @Param("user") User user);

    @Select("SELECT * FROM session JOIN user ON session.userId = user.id WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id")

    })
    Session getById(int id);

    @Select("SELECT session.id as id, session.cookie as cookie, session.userId as userId FROM session WHERE  cookie = #{cookie}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result (property = "user" ,column = "userId" , javaType = User.class,
                    one = @One(select = "net.thumbtack.school.mappers.UserMapper.getById",fetchType = FetchType.LAZY))
    })
    Session getByCookie(@Param("cookie") String cookie);

    @Delete("DELETE FROM session WHERE cookie = #{cookie}")
    void delete(@Param("cookie") String cookie);

    @Delete("DELETE FROM session")
    void deleteAll();

    @Select("SELECT * FROM session WHERE userId = #{userId}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "user", column = "userId", javaType = User.class,
                    one = @One(select = "net.thumbtack.school.mappers.UserMapper.getById"))

    })
    Session getByUserId(int userId);

}
