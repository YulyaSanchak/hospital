package net.thumbtack.school.mappers;

import net.thumbtack.school.containers.statistics.Statistic;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDate;
import java.util.List;

public interface StatisticsMapper {

    //Кол-во пациентов,пришедших на приемы в указанный период времени по всей клинике
    @Select("SELECT count(distinct patient.id) as patientsQuantity " +
            "FROM reception " +
            "LEFT JOIN " +
            "day_schedule " +
            "ON " +
            "day_schedule.id = reception.day_scheduleId AND (day_schedule.date BETWEEN #{dateStart}  AND #{dateEnd}) " +
            "JOIN " +
            "ticket " +
            "ON " +
            "ticket.id = reception.ticketId " +
            "JOIN " +
            "patient " +
            "ON " +
            "patient.id = ticket.patientId")
    Integer getAllPatientsQuantity(@Param("dateStart")LocalDate startDate, @Param("dateEnd") LocalDate endDate);

    //Общее количество приемов по всей клинике
    @Select("SELECT count(reception.id) as receptionsQuantity " +
            "FROM day_schedule " +
            "JOIN reception " +
            "ON " +
            "day_schedule.id = reception.day_scheduleId AND (day_schedule.date BETWEEN #{dateStart}  AND #{dateEnd})")
    Integer getAllReceptionsQuantity(@Param("dateStart")LocalDate startDate, @Param("dateEnd") LocalDate endDate);

    //Количество приемов по статусам по всей клинике
    @Select("SELECT reception.status as status ,count(reception.id) as quantity " +
            "FROM " +
            "day_schedule " +
            "LEFT JOIN " +
            "reception " +
            "ON " +
            "day_schedule.id = reception.day_scheduleId AND (day_schedule.date BETWEEN #{dateStart}  AND #{dateEnd}) " +
            "group by reception.status")
    List<Statistic> getAllReceptionsWithStatusQuantity(@Param("dateStart")LocalDate startDate, @Param("dateEnd") LocalDate endDate);

    //<!--Общее количество комиссий по всей клинике-->
    @Select("SELECT count(distinct commission.id) " +
            "FROM commission_doctors " +
            "JOIN commission " +
            "ON commission.id= commission_doctors.commissionId " +
            "LEFT JOIN day_schedule " +
            "ON " +
            "day_schedule.doctorId = commission_doctors.doctorId AND (day_schedule.date BETWEEN #{dateStart}  AND #{dateEnd})")
    Integer getAllCommissionsQuantity(@Param("dateStart")LocalDate startDate, @Param("dateEnd") LocalDate endDate);

    //Количество врачей, учавствующих в комиссиях за указанный промежуток времени
    @Select("SELECT count(distinct commission_doctors.doctorId) " +
            "FROM commission_doctors " +
            "JOIN commission "+
            "ON commission.id= commission_doctors.commissionId "+
            "LEFT JOIN day_schedule "+
            "ON "+
            "day_schedule.doctorId = commission_doctors.doctorId AND (day_schedule.date BETWEEN #{dateStart}  AND #{dateEnd})")
    Integer getDoctorsInCommissionsQuantity(@Param("dateStart")LocalDate startDate, @Param("dateEnd") LocalDate endDate);


    @Select("SELECT count(doctorId) as doctors " +
            "FROM day_schedule " +
            "JOIN reception " +
            "ON " +
            "day_schedule.id = reception.day_scheduleId AND (day_schedule.date BETWEEN #{dateStart}  AND #{dateEnd}) " +
            "AND reception.status = 'TICKET'")
    Integer getDoctorsInReceptions(@Param("dateStart")LocalDate startDate, @Param("dateEnd") LocalDate endDate);

}
