package net.thumbtack.school.mappers;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import net.thumbtack.school.model.UserType;
import net.thumbtack.school.model.users.User;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

public interface UserMapper {

    @Insert("INSERT INTO user ( login, password, firstName, lastName, patronymic, userType)"
            +"VALUES (#{user.login}, #{user.password}, #{user.firstName}, #{user.lastName}, #{user.patronymic} ,#{userType})")
    @Options(useGeneratedKeys = true, keyProperty = "user.userId", keyColumn = "id")
    Integer insert(@Param("user") User user, @Param("userType") UserType userType)throws MySQLIntegrityConstraintViolationException;

    @Select("SELECT login, password, firstName, lastName, patronymic, userType " +
            "FROM user WHERE id = #{id}")
    @Results({
            @Result(property = "userId", column = "id")
    })
    User getById(@Param("id") int id);

    @Select("SELECT * FROM user WHERE login = #{login}")
    @Results({
            @Result(property = "userId", column = "id")
    })
    User getByLogin(String login);

    @Select("SELECT user.id as id, user.login as login, user.password as password, user.firstName as firstName," +
            " user.lastName as lastName, user.patronymic as patronymic, user.userType as userType FROM session JOIN user" +
            " ON session.userId = user.id WHERE  session.cookie = #{cookie}")
    @Results({
            @Result(property = "userId", column = "id")
    })
    User getByCookie(@Param("cookie") String cookie);

    @Delete("DELETE FROM user WHERE id = #{user.userId}")
    void delete(@Param("user") User user);

    @Delete("DELETE FROM user WHERE userId = #{userId}")
    void deleteById(@Param("userId") int userId);

    @Delete("DELETE FROM user")
    void deleteAll();

    @Delete("DELETE FROM user WHERE user.userType = #{userType}")
    void deleteAllByType(@Param("userType")UserType userType);


    @Update("UPDATE user SET login = #{user.login}, password = #{user.password}, firstName = #{user.firstName}" +
            ", lastName = #{user.lastName}, patronymic = #{user.patronymic} " +
            "WHERE id = #{user.userId} ")
    void update(@Param("user") User user);
}
