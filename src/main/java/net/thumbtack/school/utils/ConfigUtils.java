package net.thumbtack.school.utils;


import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigUtils {
    private static int restHttpPort; //Порт, на котором работает REST-сервер
    private static int maxNameLength; //Максимальная длина имени, логина и пароля
    private static int minPasswordLength; //Минимальная длина пароля

    public ConfigUtils(String propFileName) {
        Properties prop = new Properties();
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName)) {
            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }
            restHttpPort = Integer.parseInt(prop.getProperty("rest_http_port"));
            maxNameLength = Integer.parseInt(prop.getProperty("max_name_length"));
            minPasswordLength = Integer.parseInt(prop.getProperty("min_password_length"));
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
    }

    public static int getRestHttpPort() {
        return restHttpPort;
    }

    public static int getMaxNameLength() {
        return maxNameLength;
    }

    public static int getMinPasswordLength() {
        return minPasswordLength;
    }

}
